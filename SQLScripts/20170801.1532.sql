

/************ Update: Tables ***************/

/******************** Add Table: dbo.LAFile ************************/

/* Build Table Structure */
CREATE TABLE dbo.LAFile
(
	GuidFile UniqueIdentifier NOT NULL,
	FileName VARCHAR(1000) NULL,
	FileSize BIGINT NULL,
	FileType VARCHAR(255) NULL,
	FileData VARBINARY(MAX) NULL,
	FileStorage VARCHAR(500) NULL,
	ContainerStorage VARCHAR(255) NULL
);

/* Add Primary Key */
ALTER TABLE dbo.LAFile ADD CONSTRAINT pkLAFile
	PRIMARY KEY (GuidFile);


	
/************ Update: Tables ***************/

/******************** Update Table: ProfileBlob ************************/

ALTER TABLE dbo.ProfileBlob ADD GuidFile UniqueIdentifier NULL;





/************ Add Foreign Keys ***************/

/* Add Foreign Key: fk_ProfileBlob_LAFile */
ALTER TABLE dbo.ProfileBlob ADD CONSTRAINT fk_ProfileBlob_LAFile
	FOREIGN KEY (GuidFile) REFERENCES dbo.LAFile (GuidFile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;