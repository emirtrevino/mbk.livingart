

/************ Remove Foreign Keys ***************/

ALTER TABLE dbo.ProjectContractTerms DROP CONSTRAINT FK_ProjectContractTerms_ProjectContract;



/************ Update: Tables ***************/

/******************** Update Table: ProjectContractTerm ************************/

/* Rename: dbo.ProjectContractTerms */
EXEC sp_rename 'dbo.ProjectContractTerms', 'ProjectContractTerm';





/************ Add Foreign Keys ***************/

/* Add Foreign Key: FK_ProjectContractTerms_ProjectContract */
ALTER TABLE dbo.ProjectContractTerm ADD CONSTRAINT FK_ProjectContractTerms_ProjectContract
	FOREIGN KEY (GuidProjectContract) REFERENCES dbo.ProjectContract (GuidProjectContract)
	ON UPDATE NO ACTION ON DELETE NO ACTION;