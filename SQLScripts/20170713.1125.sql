
/************ Update: Schemas ***************/

--/* Add Schema: test */
--CREATE SCHEMA test;



/************ Update: Tables ***************/

/******************** Add Table: test.testComment ************************/

/* Build Table Structure */
CREATE TABLE test.testComment
(
	GuidComment UniqueIdentifier NOT NULL,
	Content VARCHAR(4000) NULL,
	CreatedByUsername VARCHAR(100) NULL,
	GuidPost UniqueIdentifier NULL
);

/* Add Primary Key */
ALTER TABLE test.testComment ADD CONSTRAINT pktestComment
	PRIMARY KEY (GuidComment);


/******************** Add Table: test.testPost ************************/

/* Build Table Structure */
CREATE TABLE test.testPost
(
	GuidPost UniqueIdentifier NOT NULL,
	FiendInt INTEGER NULL,
	Title VARCHAR(255) NULL,
	Content VARCHAR(MAX) NULL,
	CreatedByUsername VARCHAR(100) NULL
);

/* Add Primary Key */
ALTER TABLE test.testPost ADD CONSTRAINT pktestPost
	PRIMARY KEY (GuidPost);





/************ Add Foreign Keys ***************/

/* Add Foreign Key: fk_testComment_testPost */
ALTER TABLE test.testComment ADD CONSTRAINT fk_testComment_testPost
	FOREIGN KEY (GuidPost) REFERENCES test.testPost (GuidPost)
	ON UPDATE NO ACTION ON DELETE NO ACTION;