

/************ Update: Tables ***************/

/******************** Add Table: dbo.CommunicationProject ************************/

/* Build Table Structure */
CREATE TABLE dbo.CommunicationProject
(
	GuidCommunicationProject UniqueIdentifier NOT NULL,
	GuidProject UniqueIdentifier NOT NULL,
	GuidProfile UniqueIdentifier NOT NULL,
	Description VARCHAR(400) NOT NULL,
	PublishDate DATETIME NOT NULL,
	StatusCommunication INTEGER NOT NULL
);

/* Add Primary Key */
ALTER TABLE dbo.CommunicationProject ADD CONSTRAINT pkCommunicationProject
	PRIMARY KEY (GuidCommunicationProject);


/******************** Add Table: dbo.InterestedPage ************************/

/* Build Table Structure */
CREATE TABLE dbo.InterestedPage
(
	GuidProjectContract UniqueIdentifier NOT NULL,
	GuidProfilePage UniqueIdentifier NOT NULL,
	Accepted BIT NOT NULL
);


/******************** Add Table: dbo.MusicPreference ************************/

/* Build Table Structure */
CREATE TABLE dbo.MusicPreference
(
	GuidMusicPreference UniqueIdentifier NOT NULL,
	Description VARCHAR(150) NULL,
	IsDeleted BIT NOT NULL
);

/* Add Primary Key */
ALTER TABLE dbo.MusicPreference ADD CONSTRAINT pkMusicPreference
	PRIMARY KEY (GuidMusicPreference);


/******************** Add Table: dbo.PageType ************************/

/* Build Table Structure */
CREATE TABLE dbo.PageType
(
	GuidPageType UniqueIdentifier NOT NULL,
	Name VARCHAR(150) NOT NULL,
	ProfileType INTEGER NOT NULL
);

/* Add Primary Key */
ALTER TABLE dbo.PageType ADD CONSTRAINT pkPageType
	PRIMARY KEY (GuidPageType);


/******************** Add Table: dbo.Profile ************************/

/* Build Table Structure */
CREATE TABLE dbo.Profile
(
	GuidProfile UniqueIdentifier NOT NULL,
	Name VARCHAR(150) NOT NULL,
	Alias VARCHAR(150) NULL,
	Biography VARCHAR(150) NULL,
	Birthdate DATE NULL,
	ProfileType INTEGER NOT NULL,
	ProfileStatus INTEGER NOT NULL
);

/* Add Primary Key */
ALTER TABLE dbo.Profile ADD CONSTRAINT pkProfile
	PRIMARY KEY (GuidProfile);


/******************** Add Table: dbo.ProfileBlob ************************/

/* Build Table Structure */
CREATE TABLE dbo.ProfileBlob
(
	GuidProfileBlob UniqueIdentifier NOT NULL,
	GuidProfile UniqueIdentifier NOT NULL,
	BlobPath VARCHAR(250) NOT NULL,
	BlobPathThumbnail VARCHAR(250) NOT NULL,
	BlobType INTEGER NOT NULL
);

/* Add Primary Key */
ALTER TABLE dbo.ProfileBlob ADD CONSTRAINT pkProfileBlob
	PRIMARY KEY (GuidProfileBlob);


/******************** Add Table: dbo.ProfileEvent ************************/

/* Build Table Structure */
CREATE TABLE dbo.ProfileEvent
(
	GuidProfileEvent UniqueIdentifier NOT NULL,
	GuidProfile UniqueIdentifier NOT NULL,
	EventDate DATETIME NOT NULL,
	Description VARCHAR(500) NOT NULL,
	GuidProject UniqueIdentifier NOT NULL
);

/* Add Primary Key */
ALTER TABLE dbo.ProfileEvent ADD CONSTRAINT pkProfileEvent
	PRIMARY KEY (GuidProfileEvent);


/******************** Add Table: dbo.ProfileMusicPreference ************************/

/* Build Table Structure */
CREATE TABLE dbo.ProfileMusicPreference
(
	GuidProfile UniqueIdentifier NULL,
	GuidMusicPreference UniqueIdentifier NULL,
	GuidProfileMusicPreference UniqueIdentifier NOT NULL
);

/* Add Primary Key */
ALTER TABLE dbo.ProfileMusicPreference ADD CONSTRAINT pkProfileMusicPreference
	PRIMARY KEY (GuidProfileMusicPreference);


/******************** Add Table: dbo.ProfilePage ************************/

/* Build Table Structure */
CREATE TABLE dbo.ProfilePage
(
	GuidProfilePage UniqueIdentifier NOT NULL,
	GuidProfile UniqueIdentifier NOT NULL,
	GuidPageType UniqueIdentifier NOT NULL,
	Name VARCHAR(150) NOT NULL,
	Description VARCHAR(4000) NOT NULL,
	Address VARCHAR(250) NOT NULL,
	PhoneNumber VARCHAR(25) NOT NULL
);

/* Add Primary Key */
ALTER TABLE dbo.ProfilePage ADD CONSTRAINT pkProfilePage
	PRIMARY KEY (GuidProfilePage);


/******************** Add Table: dbo.ProfilePageBlob ************************/

/* Build Table Structure */
CREATE TABLE dbo.ProfilePageBlob
(
	GuidProfilePageBlob UniqueIdentifier NOT NULL,
	GuidProfilePage UniqueIdentifier NOT NULL,
	BlobPath VARCHAR(250) NOT NULL,
	BlobPathThumbnail VARCHAR(250) NOT NULL,
	BlobType INTEGER NOT NULL,
	BlobSection INTEGER NOT NULL
);

/* Add Primary Key */
ALTER TABLE dbo.ProfilePageBlob ADD CONSTRAINT pkProfilePageBlob
	PRIMARY KEY (GuidProfilePageBlob);


/******************** Add Table: dbo.ProfilePageStore ************************/

/* Build Table Structure */
CREATE TABLE dbo.ProfilePageStore
(
	GuidProfilePageStore UniqueIdentifier NOT NULL,
	GuidProfilePage UniqueIdentifier NOT NULL,
	Name VARCHAR(150) NOT NULL,
	Address VARCHAR(250) NOT NULL
);

/* Add Primary Key */
ALTER TABLE dbo.ProfilePageStore ADD CONSTRAINT pkProfilePageStore
	PRIMARY KEY (GuidProfilePageStore);


/******************** Add Table: dbo.ProfilePageTermContract ************************/

/* Build Table Structure */
CREATE TABLE dbo.ProfilePageTermContract
(
	GuidProfilePageTermContact UniqueIdentifier NOT NULL,
	GuidProfilePage UniqueIdentifier NOT NULL,
	Description VARCHAR(250) NOT NULL
);

/* Add Primary Key */
ALTER TABLE dbo.ProfilePageTermContract ADD CONSTRAINT pkProfilePageTermContract
	PRIMARY KEY (GuidProfilePageTermContact);


/******************** Add Table: dbo.Project ************************/

/* Build Table Structure */
CREATE TABLE dbo.Project
(
	GuidProject UniqueIdentifier NOT NULL,
	GuidProfile UniqueIdentifier NOT NULL,
	Description VARCHAR(4000) NOT NULL,
	PublishDate DATETIME NOT NULL,
	ProjectDate DATE NOT NULL,
	IsExternal BIT NOT NULL,
	Surface DECIMAL(18, 2) NOT NULL,
	Money DECIMAL(18, 2) NOT NULL,
	StatusProject INTEGER NOT NULL
);

/* Add Primary Key */
ALTER TABLE dbo.Project ADD CONSTRAINT pkProject
	PRIMARY KEY (GuidProject);


/******************** Add Table: dbo.ProjectContract ************************/

/* Build Table Structure */
CREATE TABLE dbo.ProjectContract
(
	GuidProjectContract UniqueIdentifier NOT NULL,
	GuidProject UniqueIdentifier NOT NULL,
	Description VARCHAR(7000) NOT NULL,
	GuidPageType UniqueIdentifier NOT NULL,
	Money DECIMAL(18, 2) NOT NULL,
	StatusProjectContracts INTEGER NOT NULL,
	Payed BIT NOT NULL,
	Canceled BIT NOT NULL
);

/* Add Primary Key */
ALTER TABLE dbo.ProjectContract ADD CONSTRAINT pkProjectContract
	PRIMARY KEY (GuidProjectContract);


/******************** Add Table: dbo.ProjectContractTerms ************************/

/* Build Table Structure */
CREATE TABLE dbo.ProjectContractTerms
(
	ProjectContractTermId UniqueIdentifier NOT NULL,
	GuidProjectContract UniqueIdentifier NOT NULL,
	Description VARCHAR(250) NOT NULL
);

/* Add Primary Key */
ALTER TABLE dbo.ProjectContractTerms ADD CONSTRAINT PK_ProjectContractTerms
	PRIMARY KEY (ProjectContractTermId);


/******************** Add Table: dbo.ProjectIncident ************************/

/* Build Table Structure */
CREATE TABLE dbo.ProjectIncident
(
	GuidProjectIncident UniqueIdentifier NOT NULL,
	GuidProject UniqueIdentifier NOT NULL,
	GuidProfile UniqueIdentifier NOT NULL,
	Subject VARCHAR(150) NOT NULL,
	PublishDate DATETIME NOT NULL,
	StatusIncident INTEGER NOT NULL
);

/* Add Primary Key */
ALTER TABLE dbo.ProjectIncident ADD CONSTRAINT pkProjectIncident
	PRIMARY KEY (GuidProjectIncident);


/******************** Add Table: dbo.ProjectIncidentDetail ************************/

/* Build Table Structure */
CREATE TABLE dbo.ProjectIncidentDetail
(
	GuidProjectIncidentDetail UniqueIdentifier NOT NULL,
	GuidProjectIncident UniqueIdentifier NOT NULL,
	GuidProfile UniqueIdentifier NOT NULL,
	Description VARCHAR(300) NOT NULL,
	PublishDate DATETIME NOT NULL
);

/* Add Primary Key */
ALTER TABLE dbo.ProjectIncidentDetail ADD CONSTRAINT pkProjectIncidentDetail
	PRIMARY KEY (GuidProjectIncidentDetail);


/******************** Add Table: dbo.ProjectProfileRating ************************/

/* Build Table Structure */
CREATE TABLE dbo.ProjectProfileRating
(
	GuidProjectProfileRating UniqueIdentifier NOT NULL,
	GuidProfile UniqueIdentifier NOT NULL,
	GuidProjectContract UniqueIdentifier NOT NULL,
	Comment VARCHAR(500) NOT NULL,
	Calification INTEGER NOT NULL
);

/* Add Primary Key */
ALTER TABLE dbo.ProjectProfileRating ADD CONSTRAINT pkProjectProfileRating
	PRIMARY KEY (GuidProjectProfileRating);


/******************** Add Table: dbo.ProjectsQuestion ************************/

/* Build Table Structure */
CREATE TABLE dbo.ProjectsQuestion
(
	GuidProjectQuestion UniqueIdentifier NOT NULL,
	GuidProject UniqueIdentifier NOT NULL,
	Description VARBINARY(400) NOT NULL,
	PublishDate DATETIME NOT NULL,
	StatusQuestion INTEGER NOT NULL
);

/* Add Primary Key */
ALTER TABLE dbo.ProjectsQuestion ADD CONSTRAINT pkProjectsQuestion
	PRIMARY KEY (GuidProjectQuestion);





/************ Add Foreign Keys ***************/

/* Add Foreign Key: FK_CommunicationProjects_Profile */
ALTER TABLE dbo.CommunicationProject ADD CONSTRAINT FK_CommunicationProjects_Profile
	FOREIGN KEY (GuidProfile) REFERENCES dbo.Profile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_CommunicationProjects_Project */
ALTER TABLE dbo.CommunicationProject ADD CONSTRAINT FK_CommunicationProjects_Project
	FOREIGN KEY (GuidProject) REFERENCES dbo.Project (GuidProject)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_InterestedPages_ProfilePage */
ALTER TABLE dbo.InterestedPage ADD CONSTRAINT FK_InterestedPages_ProfilePage
	FOREIGN KEY (GuidProfilePage) REFERENCES dbo.ProfilePage (GuidProfilePage)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_InterestedPages_ProjectContract */
ALTER TABLE dbo.InterestedPage ADD CONSTRAINT FK_InterestedPages_ProjectContract
	FOREIGN KEY (GuidProjectContract) REFERENCES dbo.ProjectContract (GuidProjectContract)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProfileBlobs_ProfileBlobs_Profile */
ALTER TABLE dbo.ProfileBlob ADD CONSTRAINT FK_ProfileBlobs_ProfileBlobs_Profile
	FOREIGN KEY (GuidProfile) REFERENCES dbo.Profile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProfileEvents_Profile */
ALTER TABLE dbo.ProfileEvent ADD CONSTRAINT FK_ProfileEvents_Profile
	FOREIGN KEY (GuidProfileEvent) REFERENCES dbo.Profile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProfileMusicPreferences_MusicPreferences */
ALTER TABLE dbo.ProfileMusicPreference ADD CONSTRAINT FK_ProfileMusicPreferences_MusicPreferences
	FOREIGN KEY (GuidMusicPreference) REFERENCES dbo.MusicPreference (GuidMusicPreference)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProfileMusicPreferences_Profiles */
ALTER TABLE dbo.ProfileMusicPreference ADD CONSTRAINT FK_ProfileMusicPreferences_Profiles
	FOREIGN KEY (GuidProfile) REFERENCES dbo.Profile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProfilePages_PageTypes */
ALTER TABLE dbo.ProfilePage ADD CONSTRAINT FK_ProfilePages_PageTypes
	FOREIGN KEY (GuidProfilePage) REFERENCES dbo.PageType (GuidPageType)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProfilePages_Profile */
ALTER TABLE dbo.ProfilePage ADD CONSTRAINT FK_ProfilePages_Profile
	FOREIGN KEY (GuidProfilePage) REFERENCES dbo.Profile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProfilePageBlobs_Profile */
ALTER TABLE dbo.ProfilePageBlob ADD CONSTRAINT FK_ProfilePageBlobs_Profile
	FOREIGN KEY (GuidProfilePage) REFERENCES dbo.ProfilePage (GuidProfilePage)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProfilePageStores_ProfilePageS */
ALTER TABLE dbo.ProfilePageStore ADD CONSTRAINT FK_ProfilePageStores_ProfilePageS
	FOREIGN KEY (GuidProfilePage) REFERENCES dbo.ProfilePage (GuidProfilePage)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProfilePageTermContracts_ProfilePages */
ALTER TABLE dbo.ProfilePageTermContract ADD CONSTRAINT FK_ProfilePageTermContracts_ProfilePages
	FOREIGN KEY (GuidProfilePage) REFERENCES dbo.ProfilePage (GuidProfilePage)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_Projects_Profile */
ALTER TABLE dbo.Project ADD CONSTRAINT FK_Projects_Profile
	FOREIGN KEY (GuidProfile) REFERENCES dbo.Profile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProjectContracts_PageTypes */
ALTER TABLE dbo.ProjectContract ADD CONSTRAINT FK_ProjectContracts_PageTypes
	FOREIGN KEY (GuidPageType) REFERENCES dbo.PageType (GuidPageType)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProjectContracts_Project */
ALTER TABLE dbo.ProjectContract ADD CONSTRAINT FK_ProjectContracts_Project
	FOREIGN KEY (GuidProject) REFERENCES dbo.Project (GuidProject)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProjectContractTerms_ProjectContract */
ALTER TABLE dbo.ProjectContractTerms ADD CONSTRAINT FK_ProjectContractTerms_ProjectContract
	FOREIGN KEY (GuidProjectContract) REFERENCES dbo.ProjectContract (GuidProjectContract)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProjectIncidents_Profile */
ALTER TABLE dbo.ProjectIncident ADD CONSTRAINT FK_ProjectIncidents_Profile
	FOREIGN KEY (GuidProfile) REFERENCES dbo.Profile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProjectIncidents_Project */
ALTER TABLE dbo.ProjectIncident ADD CONSTRAINT FK_ProjectIncidents_Project
	FOREIGN KEY (GuidProject) REFERENCES dbo.Project (GuidProject)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProjectIncidentDetails_ProjectIncident */
ALTER TABLE dbo.ProjectIncidentDetail ADD CONSTRAINT FK_ProjectIncidentDetails_ProjectIncident
	FOREIGN KEY (GuidProjectIncident) REFERENCES dbo.ProjectIncident (GuidProjectIncident)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProjectIncidentDetails_ProjectIncidentDetails */
ALTER TABLE dbo.ProjectIncidentDetail ADD CONSTRAINT FK_ProjectIncidentDetails_ProjectIncidentDetails
	FOREIGN KEY (GuidProjectIncidentDetail) REFERENCES dbo.ProjectIncidentDetail (GuidProjectIncidentDetail)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProjectProfileRatings_Profile */
ALTER TABLE dbo.ProjectProfileRating ADD CONSTRAINT FK_ProjectProfileRatings_Profile
	FOREIGN KEY (GuidProfile) REFERENCES dbo.Profile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProjectProfileRatings_ProjectContrac */
ALTER TABLE dbo.ProjectProfileRating ADD CONSTRAINT FK_ProjectProfileRatings_ProjectContrac
	FOREIGN KEY (GuidProjectContract) REFERENCES dbo.ProjectContract (GuidProjectContract)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProjectsQuestions_Projects */
ALTER TABLE dbo.ProjectsQuestion ADD CONSTRAINT FK_ProjectsQuestions_Projects
	FOREIGN KEY (GuidProject) REFERENCES dbo.Project (GuidProject)
	ON UPDATE NO ACTION ON DELETE NO ACTION;