

/************ Remove Foreign Keys ***************/

ALTER TABLE dbo.ProfilePage DROP CONSTRAINT FK_ProfilePages_PageTypes;

ALTER TABLE dbo.ProfilePage DROP CONSTRAINT FK_ProfilePages_Profile;



/************ Add Foreign Keys ***************/

/* Add Foreign Key: fk_ProfilePage_PageType */
ALTER TABLE dbo.ProfilePage ADD CONSTRAINT fk_ProfilePage_PageType
	FOREIGN KEY (GuidPageType) REFERENCES dbo.PageType (GuidPageType)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: fk_ProfilePage_Profile */
ALTER TABLE dbo.ProfilePage ADD CONSTRAINT fk_ProfilePage_Profile
	FOREIGN KEY (GuidProfile) REFERENCES dbo.Profile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;



/************ Remove Foreign Keys ***************/

ALTER TABLE dbo.ProfileEvent DROP CONSTRAINT FK_ProfileEvents_Profile;



/************ Add Foreign Keys ***************/

/* Add Foreign Key: fk_ProfileEvent_Profile */
ALTER TABLE dbo.ProfileEvent ADD CONSTRAINT fk_ProfileEvent_Profile
	FOREIGN KEY (GuidProfile) REFERENCES dbo.Profile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;




/************ Remove Foreign Keys ***************/

ALTER TABLE dbo.ProjectIncidentDetail DROP CONSTRAINT FK_ProjectIncidentDetails_ProjectIncidentDetails;



/************ Add Foreign Keys ***************/

/* Add Foreign Key: fk_ProjectIncidentDetail_Profile */
ALTER TABLE dbo.ProjectIncidentDetail ADD CONSTRAINT fk_ProjectIncidentDetail_Profile
	FOREIGN KEY (GuidProfile) REFERENCES dbo.Profile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;