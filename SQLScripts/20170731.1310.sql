
/************ Remove Foreign Keys ***************/

ALTER TABLE dbo.CommunicationProject DROP CONSTRAINT FK_CommunicationProjects_Profile;

ALTER TABLE dbo.ProfileBlob DROP CONSTRAINT FK_ProfileBlobs_ProfileBlobs_Profile;

ALTER TABLE dbo.ProfileEvent DROP CONSTRAINT fk_ProfileEvent_Profile;

ALTER TABLE dbo.ProfileMusicPreference DROP CONSTRAINT FK_ProfileMusicPreferences_Profiles;

ALTER TABLE dbo.ProfilePage DROP CONSTRAINT fk_ProfilePage_Profile;

ALTER TABLE dbo.Project DROP CONSTRAINT FK_Projects_Profile;

ALTER TABLE dbo.ProjectIncident DROP CONSTRAINT FK_ProjectIncidents_Profile;

ALTER TABLE dbo.ProjectIncidentDetail DROP CONSTRAINT fk_ProjectIncidentDetail_Profile;

ALTER TABLE dbo.ProjectProfileRating DROP CONSTRAINT FK_ProjectProfileRatings_Profile;



/************ Update: Tables ***************/

/******************** Update Table: UserProfile ************************/

/* Rename: dbo.Profile */
EXEC sp_rename 'dbo.Profile', 'UserProfile';





/************ Add Foreign Keys ***************/

/* Add Foreign Key: FK_CommunicationProjects_Profile */
ALTER TABLE dbo.CommunicationProject ADD CONSTRAINT FK_CommunicationProjects_Profile
	FOREIGN KEY (GuidProfile) REFERENCES dbo.UserProfile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProfileBlobs_ProfileBlobs_Profile */
ALTER TABLE dbo.ProfileBlob ADD CONSTRAINT FK_ProfileBlobs_ProfileBlobs_Profile
	FOREIGN KEY (GuidProfile) REFERENCES dbo.UserProfile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: fk_ProfileEvent_Profile */
ALTER TABLE dbo.ProfileEvent ADD CONSTRAINT fk_ProfileEvent_Profile
	FOREIGN KEY (GuidProfile) REFERENCES dbo.UserProfile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProfileMusicPreferences_Profiles */
ALTER TABLE dbo.ProfileMusicPreference ADD CONSTRAINT FK_ProfileMusicPreferences_Profiles
	FOREIGN KEY (GuidProfile) REFERENCES dbo.UserProfile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: fk_ProfilePage_Profile */
ALTER TABLE dbo.ProfilePage ADD CONSTRAINT fk_ProfilePage_Profile
	FOREIGN KEY (GuidProfile) REFERENCES dbo.UserProfile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_Projects_Profile */
ALTER TABLE dbo.Project ADD CONSTRAINT FK_Projects_Profile
	FOREIGN KEY (GuidProfile) REFERENCES dbo.UserProfile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProjectIncidents_Profile */
ALTER TABLE dbo.ProjectIncident ADD CONSTRAINT FK_ProjectIncidents_Profile
	FOREIGN KEY (GuidProfile) REFERENCES dbo.UserProfile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: fk_ProjectIncidentDetail_Profile */
ALTER TABLE dbo.ProjectIncidentDetail ADD CONSTRAINT fk_ProjectIncidentDetail_Profile
	FOREIGN KEY (GuidProfile) REFERENCES dbo.UserProfile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;

/* Add Foreign Key: FK_ProjectProfileRatings_Profile */
ALTER TABLE dbo.ProjectProfileRating ADD CONSTRAINT FK_ProjectProfileRatings_Profile
	FOREIGN KEY (GuidProfile) REFERENCES dbo.UserProfile (GuidProfile)
	ON UPDATE NO ACTION ON DELETE NO ACTION;