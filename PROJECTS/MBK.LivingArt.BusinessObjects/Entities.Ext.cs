﻿ 
 
// <Template>
//   <SolutionTemplate>EF POCO 1</SolutionTemplate>
//   <Version>20140822.0944</Version>
//   <Update>Metadata de identificador</Update>
// </Template>
#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using SFSdotNet.Framework.Common.Entities.Metadata;
using SFSdotNet.Framework.Common.Entities;
using System.Linq.Dynamic;
//using Repository.Pattern.Ef6;
#endregion
namespace MBK.LivingArt.BusinessObjects
{

	  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidCommunicationProject",PropertyDefaultText="Description",RequiredProperties="GuidProject,GuidProfile,Description,PublishDate,StatusCommunication,Project,UserProfile",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class CommunicationProject:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Description != null )		
            		return this.Description.ToString();
				else
					return String.Empty;
			}

		//public CommunicationProject()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidCommunicationProject.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidCommunicationProject)
            {
				_GuidCommunicationProject = guidCommunicationProject;

            }
			private Guid _GuidCommunicationProject;
			[DataMember]
			public Guid GuidCommunicationProject
			{
				get{
					return _GuidCommunicationProject;
				}
				set{
                     
					_GuidCommunicationProject = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "CommunicationProject";
		public static readonly string EntitySetName = "CommunicationProjects";
        public struct PropertyNames {
            public static readonly string GuidCommunicationProject = "GuidCommunicationProject";
            public static readonly string GuidProject = "GuidProject";
            public static readonly string GuidProfile = "GuidProfile";
            public static readonly string Description = "Description";
            public static readonly string PublishDate = "PublishDate";
            public static readonly string StatusCommunication = "StatusCommunication";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string Project = "Project";
            public static readonly string UserProfile = "UserProfile";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidMusicPreference",PropertyDefaultText="Description",RequiredProperties="IsDeleted",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class MusicPreference:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Description != null )		
            		return this.Description.ToString();
				else
					return String.Empty;
			}

		//public MusicPreference()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidMusicPreference.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidMusicPreference)
            {
				_GuidMusicPreference = guidMusicPreference;

            }
			private Guid _GuidMusicPreference;
			[DataMember]
			public Guid GuidMusicPreference
			{
				get{
					return _GuidMusicPreference;
				}
				set{
                     
					_GuidMusicPreference = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "MusicPreference";
		public static readonly string EntitySetName = "MusicPreferences";
        public struct PropertyNames {
            public static readonly string GuidMusicPreference = "GuidMusicPreference";
            public static readonly string Description = "Description";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string ProfileMusicPreferences = "ProfileMusicPreferences";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidPageType",PropertyDefaultText="Name",RequiredProperties="Name,ProfileType",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class PageType:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Name != null )		
            		return this.Name.ToString();
				else
					return String.Empty;
			}

		//public PageType()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidPageType.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidPageType)
            {
				_GuidPageType = guidPageType;

            }
			private Guid _GuidPageType;
			[DataMember]
			public Guid GuidPageType
			{
				get{
					return _GuidPageType;
				}
				set{
                     
					_GuidPageType = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "PageType";
		public static readonly string EntitySetName = "PageTypes";
        public struct PropertyNames {
            public static readonly string GuidPageType = "GuidPageType";
            public static readonly string Name = "Name";
            public static readonly string ProfileType = "ProfileType";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string ProfilePages = "ProfilePages";
            public static readonly string ProjectContracts = "ProjectContracts";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidProfileBlob",PropertyDefaultText="BlobPath",RequiredProperties="GuidProfile,BlobPath,BlobPathThumbnail,BlobType,UserProfile,ExistFile",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class ProfileBlob:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.BlobPath != null )		
            		return this.BlobPath.ToString();
				else
					return String.Empty;
			}

		//public ProfileBlob()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidProfileBlob.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidProfileBlob)
            {
				_GuidProfileBlob = guidProfileBlob;

            }
			private Guid _GuidProfileBlob;
			[DataMember]
			public Guid GuidProfileBlob
			{
				get{
					return _GuidProfileBlob;
				}
				set{
                     
					_GuidProfileBlob = value;
				}
	        }

            
        }
        #endregion
        
			[DataMember]
          	 public Boolean ExistFile { get; set; } //test

			[DataMember]
          	 public String FileName { get; set; } //test

	
       
		 public string files_LAFile { get; set; }

      


	      #region propertyNames
		public static readonly string EntityName = "ProfileBlob";
		public static readonly string EntitySetName = "ProfileBlobs";
        public struct PropertyNames {
            public static readonly string GuidProfileBlob = "GuidProfileBlob";
            public static readonly string GuidProfile = "GuidProfile";
            public static readonly string BlobPath = "BlobPath";
            public static readonly string BlobPathThumbnail = "BlobPathThumbnail";
            public static readonly string BlobType = "BlobType";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string GuidFile = "GuidFile";
            public static readonly string LAFile = "LAFile";
            public static readonly string UserProfile = "UserProfile";
            public static readonly string ExistFile = "ExistFile";
            public static readonly string FileName = "FileName";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidProfileMusicPreference",PropertyDefaultText="Bytes",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class ProfileMusicPreference:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Bytes != null )		
            		return this.Bytes.ToString();
				else
					return String.Empty;
			}

		//public ProfileMusicPreference()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidProfileMusicPreference.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidProfileMusicPreference)
            {
				_GuidProfileMusicPreference = guidProfileMusicPreference;

            }
			private Guid _GuidProfileMusicPreference;
			[DataMember]
			public Guid GuidProfileMusicPreference
			{
				get{
					return _GuidProfileMusicPreference;
				}
				set{
                     
					_GuidProfileMusicPreference = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "ProfileMusicPreference";
		public static readonly string EntitySetName = "ProfileMusicPreferences";
        public struct PropertyNames {
            public static readonly string GuidProfile = "GuidProfile";
            public static readonly string GuidMusicPreference = "GuidMusicPreference";
            public static readonly string GuidProfileMusicPreference = "GuidProfileMusicPreference";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string MusicPreference = "MusicPreference";
            public static readonly string UserProfile = "UserProfile";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidProfilePageBlob",PropertyDefaultText="BlobPath",RequiredProperties="GuidProfilePage,BlobPath,BlobPathThumbnail,BlobType,BlobSection,ProfilePage",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class ProfilePageBlob:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.BlobPath != null )		
            		return this.BlobPath.ToString();
				else
					return String.Empty;
			}

		//public ProfilePageBlob()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidProfilePageBlob.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidProfilePageBlob)
            {
				_GuidProfilePageBlob = guidProfilePageBlob;

            }
			private Guid _GuidProfilePageBlob;
			[DataMember]
			public Guid GuidProfilePageBlob
			{
				get{
					return _GuidProfilePageBlob;
				}
				set{
                     
					_GuidProfilePageBlob = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "ProfilePageBlob";
		public static readonly string EntitySetName = "ProfilePageBlobs";
        public struct PropertyNames {
            public static readonly string GuidProfilePageBlob = "GuidProfilePageBlob";
            public static readonly string GuidProfilePage = "GuidProfilePage";
            public static readonly string BlobPath = "BlobPath";
            public static readonly string BlobPathThumbnail = "BlobPathThumbnail";
            public static readonly string BlobType = "BlobType";
            public static readonly string BlobSection = "BlobSection";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string ProfilePage = "ProfilePage";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidProfilePageStore",PropertyDefaultText="Name",RequiredProperties="GuidProfilePage,Name,Address,ProfilePage",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class ProfilePageStore:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Name != null )		
            		return this.Name.ToString();
				else
					return String.Empty;
			}

		//public ProfilePageStore()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidProfilePageStore.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidProfilePageStore)
            {
				_GuidProfilePageStore = guidProfilePageStore;

            }
			private Guid _GuidProfilePageStore;
			[DataMember]
			public Guid GuidProfilePageStore
			{
				get{
					return _GuidProfilePageStore;
				}
				set{
                     
					_GuidProfilePageStore = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "ProfilePageStore";
		public static readonly string EntitySetName = "ProfilePageStores";
        public struct PropertyNames {
            public static readonly string GuidProfilePageStore = "GuidProfilePageStore";
            public static readonly string GuidProfilePage = "GuidProfilePage";
            public static readonly string Name = "Name";
            public static readonly string Address = "Address";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string ProfilePage = "ProfilePage";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidProfilePageTermContact",PropertyDefaultText="Description",RequiredProperties="GuidProfilePage,Description,ProfilePage",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class ProfilePageTermContract:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Description != null )		
            		return this.Description.ToString();
				else
					return String.Empty;
			}

		//public ProfilePageTermContract()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidProfilePageTermContact.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidProfilePageTermContact)
            {
				_GuidProfilePageTermContact = guidProfilePageTermContact;

            }
			private Guid _GuidProfilePageTermContact;
			[DataMember]
			public Guid GuidProfilePageTermContact
			{
				get{
					return _GuidProfilePageTermContact;
				}
				set{
                     
					_GuidProfilePageTermContact = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "ProfilePageTermContract";
		public static readonly string EntitySetName = "ProfilePageTermContracts";
        public struct PropertyNames {
            public static readonly string GuidProfilePageTermContact = "GuidProfilePageTermContact";
            public static readonly string GuidProfilePage = "GuidProfilePage";
            public static readonly string Description = "Description";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string ProfilePage = "ProfilePage";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidProject",PropertyDefaultText="Description",RequiredProperties="GuidProfile,Description,PublishDate,ProjectDate,IsExternal,Surface,Money,StatusProject,UserProfile",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class Project:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Description != null )		
            		return this.Description.ToString();
				else
					return String.Empty;
			}

		//public Project()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidProject.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidProject)
            {
				_GuidProject = guidProject;

            }
			private Guid _GuidProject;
			[DataMember]
			public Guid GuidProject
			{
				get{
					return _GuidProject;
				}
				set{
                     
					_GuidProject = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "Project";
		public static readonly string EntitySetName = "Projects";
        public struct PropertyNames {
            public static readonly string GuidProject = "GuidProject";
            public static readonly string GuidProfile = "GuidProfile";
            public static readonly string Description = "Description";
            public static readonly string PublishDate = "PublishDate";
            public static readonly string ProjectDate = "ProjectDate";
            public static readonly string IsExternal = "IsExternal";
            public static readonly string Surface = "Surface";
            public static readonly string Money = "Money";
            public static readonly string StatusProject = "StatusProject";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string CommunicationProjects = "CommunicationProjects";
            public static readonly string ProjectIncidents = "ProjectIncidents";
            public static readonly string ProjectsQuestions = "ProjectsQuestions";
            public static readonly string UserProfile = "UserProfile";
            public static readonly string ProjectContracts = "ProjectContracts";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidProjectIncident",PropertyDefaultText="Subject",RequiredProperties="GuidProject,GuidProfile,Subject,PublishDate,StatusIncident,Project",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class ProjectIncident:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Subject != null )		
            		return this.Subject.ToString();
				else
					return String.Empty;
			}

		//public ProjectIncident()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidProjectIncident.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidProjectIncident)
            {
				_GuidProjectIncident = guidProjectIncident;

            }
			private Guid _GuidProjectIncident;
			[DataMember]
			public Guid GuidProjectIncident
			{
				get{
					return _GuidProjectIncident;
				}
				set{
                     
					_GuidProjectIncident = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "ProjectIncident";
		public static readonly string EntitySetName = "ProjectIncidents";
        public struct PropertyNames {
            public static readonly string GuidProjectIncident = "GuidProjectIncident";
            public static readonly string GuidProject = "GuidProject";
            public static readonly string GuidProfile = "GuidProfile";
            public static readonly string Subject = "Subject";
            public static readonly string PublishDate = "PublishDate";
            public static readonly string StatusIncident = "StatusIncident";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string Project = "Project";
            public static readonly string ProjectIncidentDetails = "ProjectIncidentDetails";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidProjectProfileRating",PropertyDefaultText="Comment",RequiredProperties="GuidProfile,GuidProjectContract,Comment,Calification,ProjectContract",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class ProjectProfileRating:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Comment != null )		
            		return this.Comment.ToString();
				else
					return String.Empty;
			}

		//public ProjectProfileRating()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidProjectProfileRating.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidProjectProfileRating)
            {
				_GuidProjectProfileRating = guidProjectProfileRating;

            }
			private Guid _GuidProjectProfileRating;
			[DataMember]
			public Guid GuidProjectProfileRating
			{
				get{
					return _GuidProjectProfileRating;
				}
				set{
                     
					_GuidProjectProfileRating = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "ProjectProfileRating";
		public static readonly string EntitySetName = "ProjectProfileRatings";
        public struct PropertyNames {
            public static readonly string GuidProjectProfileRating = "GuidProjectProfileRating";
            public static readonly string GuidProfile = "GuidProfile";
            public static readonly string GuidProjectContract = "GuidProjectContract";
            public static readonly string Comment = "Comment";
            public static readonly string Calification = "Calification";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string ProjectContract = "ProjectContract";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidProjectQuestion",PropertyDefaultText="StatusQuestion",RequiredProperties="GuidProject,Description,PublishDate,StatusQuestion,Project",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class ProjectsQuestion:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.StatusQuestion != null )		
            		return this.StatusQuestion.ToString();
				else
					return String.Empty;
			}

		//public ProjectsQuestion()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidProjectQuestion.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidProjectQuestion)
            {
				_GuidProjectQuestion = guidProjectQuestion;

            }
			private Guid _GuidProjectQuestion;
			[DataMember]
			public Guid GuidProjectQuestion
			{
				get{
					return _GuidProjectQuestion;
				}
				set{
                     
					_GuidProjectQuestion = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "ProjectsQuestion";
		public static readonly string EntitySetName = "ProjectsQuestions";
        public struct PropertyNames {
            public static readonly string GuidProjectQuestion = "GuidProjectQuestion";
            public static readonly string GuidProject = "GuidProject";
            public static readonly string Description = "Description";
            public static readonly string PublishDate = "PublishDate";
            public static readonly string StatusQuestion = "StatusQuestion";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string Project = "Project";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidProfilePage",PropertyDefaultText="Name",RequiredProperties="GuidProfile,GuidPageType,Name,Description,Address,PhoneNumber,PageType,UserProfile",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class ProfilePage:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Name != null )		
            		return this.Name.ToString();
				else
					return String.Empty;
			}

		//public ProfilePage()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidProfilePage.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidProfilePage)
            {
				_GuidProfilePage = guidProfilePage;

            }
			private Guid _GuidProfilePage;
			[DataMember]
			public Guid GuidProfilePage
			{
				get{
					return _GuidProfilePage;
				}
				set{
                     
					_GuidProfilePage = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "ProfilePage";
		public static readonly string EntitySetName = "ProfilePages";
        public struct PropertyNames {
            public static readonly string GuidProfilePage = "GuidProfilePage";
            public static readonly string GuidProfile = "GuidProfile";
            public static readonly string GuidPageType = "GuidPageType";
            public static readonly string Name = "Name";
            public static readonly string Description = "Description";
            public static readonly string Address = "Address";
            public static readonly string PhoneNumber = "PhoneNumber";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string ContentHTML = "ContentHTML";
            public static readonly string PageType = "PageType";
            public static readonly string ProfilePageBlobs = "ProfilePageBlobs";
            public static readonly string ProfilePageStores = "ProfilePageStores";
            public static readonly string ProfilePageTermContracts = "ProfilePageTermContracts";
            public static readonly string UserProfile = "UserProfile";
            public static readonly string InterestedPages = "InterestedPages";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidProfileEvent",PropertyDefaultText="Description",RequiredProperties="GuidProfile,EventDate,Description,GuidProject,UserProfile",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class ProfileEvent:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Description != null )		
            		return this.Description.ToString();
				else
					return String.Empty;
			}

		//public ProfileEvent()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidProfileEvent.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidProfileEvent)
            {
				_GuidProfileEvent = guidProfileEvent;

            }
			private Guid _GuidProfileEvent;
			[DataMember]
			public Guid GuidProfileEvent
			{
				get{
					return _GuidProfileEvent;
				}
				set{
                     
					_GuidProfileEvent = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "ProfileEvent";
		public static readonly string EntitySetName = "ProfileEvents";
        public struct PropertyNames {
            public static readonly string GuidProfileEvent = "GuidProfileEvent";
            public static readonly string GuidProfile = "GuidProfile";
            public static readonly string EventDate = "EventDate";
            public static readonly string Description = "Description";
            public static readonly string GuidProject = "GuidProject";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string UserProfile = "UserProfile";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidProjectIncidentDetail",PropertyDefaultText="Description",RequiredProperties="GuidProjectIncident,GuidProfile,Description,PublishDate,ProjectIncident",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class ProjectIncidentDetail:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Description != null )		
            		return this.Description.ToString();
				else
					return String.Empty;
			}

		//public ProjectIncidentDetail()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidProjectIncidentDetail.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidProjectIncidentDetail)
            {
				_GuidProjectIncidentDetail = guidProjectIncidentDetail;

            }
			private Guid _GuidProjectIncidentDetail;
			[DataMember]
			public Guid GuidProjectIncidentDetail
			{
				get{
					return _GuidProjectIncidentDetail;
				}
				set{
                     
					_GuidProjectIncidentDetail = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "ProjectIncidentDetail";
		public static readonly string EntitySetName = "ProjectIncidentDetails";
        public struct PropertyNames {
            public static readonly string GuidProjectIncidentDetail = "GuidProjectIncidentDetail";
            public static readonly string GuidProjectIncident = "GuidProjectIncident";
            public static readonly string GuidProfile = "GuidProfile";
            public static readonly string Description = "Description";
            public static readonly string PublishDate = "PublishDate";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string ProjectIncident = "ProjectIncident";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="ProjectContractTermId",PropertyDefaultText="Description",RequiredProperties="GuidProjectContract,Description,ProjectContract",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class ProjectContractTerm:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Description != null )		
            		return this.Description.ToString();
				else
					return String.Empty;
			}

		//public ProjectContractTerm()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.ProjectContractTermId.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid projectContractTermId)
            {
				_ProjectContractTermId = projectContractTermId;

            }
			private Guid _ProjectContractTermId;
			[DataMember]
			public Guid ProjectContractTermId
			{
				get{
					return _ProjectContractTermId;
				}
				set{
                     
					_ProjectContractTermId = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "ProjectContractTerm";
		public static readonly string EntitySetName = "ProjectContractTerms";
        public struct PropertyNames {
            public static readonly string ProjectContractTermId = "ProjectContractTermId";
            public static readonly string GuidProjectContract = "GuidProjectContract";
            public static readonly string Description = "Description";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string ProjectContract = "ProjectContract";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidFile",PropertyDefaultText="FileName",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted", IsFile=true, ExtraSizeField= "FileSize")]
	  [DynamicLinqType]
	  public partial class LAFile:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.FileName != null )		
            		return this.FileName.ToString();
				else
					return String.Empty;
			}

		//public LAFile()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidFile.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidFile)
            {
				_GuidFile = guidFile;

            }
			private Guid _GuidFile;
			[DataMember]
			public Guid GuidFile
			{
				get{
					return _GuidFile;
				}
				set{
                     
					_GuidFile = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "LAFile";
		public static readonly string EntitySetName = "LAFiles";
        public struct PropertyNames {
            public static readonly string GuidFile = "GuidFile";
            public static readonly string FileName = "FileName";
            public static readonly string FileSize = "FileSize";
            public static readonly string FileType = "FileType";
            public static readonly string FileData = "FileData";
            public static readonly string FileStorage = "FileStorage";
            public static readonly string ContainerStorage = "ContainerStorage";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string ProfileBlobs = "ProfileBlobs";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidUser",PropertyDefaultText="Name",RequiredProperties="Name,ProfileType,ProfileStatus,CountMusicPreferencesComputed",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class UserProfile:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Name != null )		
            		return this.Name.ToString();
				else
					return String.Empty;
			}

		//public UserProfile()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidUser.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidUser)
            {
				_GuidUser = guidUser;

            }
			private Guid _GuidUser;
			[DataMember]
			public Guid GuidUser
			{
				get{
					return _GuidUser;
				}
				set{
                     
					_GuidUser = value;
				}
	        }

            
        }
        #endregion
        
			[DataMember]
          	 public Int32 CountMusicPreferencesComputed { get; set; } //test

	
       
      


	      #region propertyNames
		public static readonly string EntityName = "UserProfile";
		public static readonly string EntitySetName = "UserProfiles";
        public struct PropertyNames {
            public static readonly string GuidUser = "GuidUser";
            public static readonly string Name = "Name";
            public static readonly string Alias = "Alias";
            public static readonly string Biography = "Biography";
            public static readonly string Birthdate = "Birthdate";
            public static readonly string ProfileType = "ProfileType";
            public static readonly string ProfileStatus = "ProfileStatus";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string IntegrationID = "IntegrationID";
            public static readonly string CommunicationProjects = "CommunicationProjects";
            public static readonly string ProfileBlobs = "ProfileBlobs";
            public static readonly string ProfileEvents = "ProfileEvents";
            public static readonly string ProfileMusicPreferences = "ProfileMusicPreferences";
            public static readonly string ProfilePages = "ProfilePages";
            public static readonly string Projects = "Projects";
            public static readonly string CountMusicPreferencesComputed = "CountMusicPreferencesComputed";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidInterestedPage",PropertyDefaultText="Bytes",RequiredProperties="GuidProjectContract,GuidProfilePage,Accepted,ProfilePage,ProjectContract",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class InterestedPage:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Bytes != null )		
            		return this.Bytes.ToString();
				else
					return String.Empty;
			}

		//public InterestedPage()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidInterestedPage.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidInterestedPage)
            {
				_GuidInterestedPage = guidInterestedPage;

            }
			private Guid _GuidInterestedPage;
			[DataMember]
			public Guid GuidInterestedPage
			{
				get{
					return _GuidInterestedPage;
				}
				set{
                     
					_GuidInterestedPage = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "InterestedPage";
		public static readonly string EntitySetName = "InterestedPages";
        public struct PropertyNames {
            public static readonly string GuidProjectContract = "GuidProjectContract";
            public static readonly string GuidProfilePage = "GuidProfilePage";
            public static readonly string Accepted = "Accepted";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string GuidInterestedPage = "GuidInterestedPage";
            public static readonly string ProfilePage = "ProfilePage";
            public static readonly string ProjectContract = "ProjectContract";
		}
		#endregion
	}
		  [Serializable()]
	  [EntityInfo(PropertyKeyName="GuidProjectContract",PropertyDefaultText="Description",RequiredProperties="GuidProject,Description,GuidPageType,Money,StatusProjectContracts,Payed,Canceled,PageType,Project",CreatedByPropertyName="CreatedBy",UpdatedByPropertyName="UpdatedBy",CreatedDatePropertyName="CreatedDate",UpdatedDatePropertyName="UpdatedDate",DeletedPropertyName="IsDeleted")]
	  [DynamicLinqType]
	  public partial class ProjectContract:  IMyEntity{
			public SFSdotNet.Framework.Common.GlobalObjects.UserInfo CreatedByUser { get; set; }

			public override string ToString()
            {
	
				if (this.Description != null )		
            		return this.Description.ToString();
				else
					return String.Empty;
			}

		//public ProjectContract()
        //  {

        //  }

	  #region Composite Key
	   public string Key { 
                  get {
                      StringBuilder sb = new StringBuilder();
					sb.Append(this.GuidProjectContract.ToString());
                      return sb.ToString();
                } 
		}
        [Serializable()]
        [DataContract]
        public class CompositeKey {

			
            public CompositeKey(Guid guidProjectContract)
            {
				_GuidProjectContract = guidProjectContract;

            }
			private Guid _GuidProjectContract;
			[DataMember]
			public Guid GuidProjectContract
			{
				get{
					return _GuidProjectContract;
				}
				set{
                     
					_GuidProjectContract = value;
				}
	        }

            
        }
        #endregion
        
	
       
      


	      #region propertyNames
		public static readonly string EntityName = "ProjectContract";
		public static readonly string EntitySetName = "ProjectContracts";
        public struct PropertyNames {
            public static readonly string GuidProjectContract = "GuidProjectContract";
            public static readonly string GuidProject = "GuidProject";
            public static readonly string Description = "Description";
            public static readonly string GuidPageType = "GuidPageType";
            public static readonly string Money = "Money";
            public static readonly string StatusProjectContracts = "StatusProjectContracts";
            public static readonly string Payed = "Payed";
            public static readonly string Canceled = "Canceled";
            public static readonly string CreatedDate = "CreatedDate";
            public static readonly string UpdatedDate = "UpdatedDate";
            public static readonly string CreatedBy = "CreatedBy";
            public static readonly string UpdatedBy = "UpdatedBy";
            public static readonly string Bytes = "Bytes";
            public static readonly string IsDeleted = "IsDeleted";
            public static readonly string InterestedPages = "InterestedPages";
            public static readonly string PageType = "PageType";
            public static readonly string Project = "Project";
            public static readonly string ProjectContractTerms = "ProjectContractTerms";
            public static readonly string ProjectProfileRatings = "ProjectProfileRatings";
		}
		#endregion
	}
	 
	
}
