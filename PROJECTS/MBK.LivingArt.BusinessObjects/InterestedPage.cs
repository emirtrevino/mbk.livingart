
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace MBK.LivingArt.BusinessObjects
{

using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using SFSdotNet.Framework.Common.Entities;
    
[JsonObject(IsReference = true)]
[DataContract(IsReference = true, Namespace = "http://schemas.datacontract.org/2004/07/TrackableEntities.Models")]
public partial class InterestedPage : ITrackable
{

    [DataMember]
        public System.Guid GuidProjectContract { get; set; }

    [DataMember]
        public System.Guid GuidProfilePage { get; set; }

    [DataMember]
        public bool Accepted { get; set; }

    [DataMember]
        public Nullable<System.DateTime> CreatedDate { get; set; }

    [DataMember]
        public Nullable<System.DateTime> UpdatedDate { get; set; }

    [DataMember]
        public Nullable<System.Guid> CreatedBy { get; set; }

    [DataMember]
        public Nullable<System.Guid> UpdatedBy { get; set; }

    [DataMember]
        public Nullable<int> Bytes { get; set; }

    [DataMember]
        public Nullable<bool> IsDeleted { get; set; }

    [DataMember]
        public System.Guid GuidInterestedPage { get; set; }



    [DataMember]
        public ProfilePage ProfilePage { get; set; }

    [DataMember]
        public ProjectContract ProjectContract { get; set; }


    [DataMember]
    public TrackingState TrackingState { get; set; }
    [DataMember]
    public ICollection<string> ModifiedProperties { get; set; }
    [JsonProperty, DataMember]
    private Guid EntityIdentifier { get; set; }
}

}
