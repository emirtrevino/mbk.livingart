﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using MBK.LivingArt.Web.Mvc.Resources;
using System.Runtime.Serialization;
using SFSdotNet.Framework.Web.Mvc.Models;
using SFSdotNet.Framework.Web.Mvc.Extensions;
using BO = MBK.LivingArt.BusinessObjects;
using System.Web.Mvc;
//using SFSdotNet.Framework.Web.Mvc.Validation;
//using SFSdotNet.Framework.Web.Mvc.Models;
using SFSdotNet.Framework.Web.Mvc.Resources;
using SFSdotNet.Framework.Common.Entities.Metadata;
using System.Text;
using MBK.LivingArt.BusinessObjects;
	namespace MBK.LivingArt.Web.Mvc.Models.CommunicationProjects 
	{
	public partial class CommunicationProjectModel: ModelBase{

	  public CommunicationProjectModel(BO.CommunicationProject resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public CommunicationProjectModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidCommunicationProject.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Description != null)
		
            return this.Description.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidCommunicationProject{ get; set; }
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROJECT"/*, NameResourceType=typeof(CommunicationProjectResources)*/)]
	public Guid   GuidProject { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROFILE"/*, NameResourceType=typeof(CommunicationProjectResources)*/)]
	public Guid   GuidProfile { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("DESCRIPTION"/*, NameResourceType=typeof(CommunicationProjectResources)*/)]
	public String   Description { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("PUBLISHDATE"/*, NameResourceType=typeof(CommunicationProjectResources)*/)]
	public DateTime   PublishDate { get; set; }
	public string PublishDateText {
        get {
            if (PublishDate != null)
				return ((DateTime)PublishDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.PublishDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[DataType("Integer")]
	[LocalizedDisplayName("STATUSCOMMUNICATION"/*, NameResourceType=typeof(CommunicationProjectResources)*/)]
	public Int32   StatusCommunication { get; set; }
	public string _StatusCommunicationText = null;
    public string StatusCommunicationText {
        get {
			if (string.IsNullOrEmpty( _StatusCommunicationText ))
				{
				return StatusCommunication.ToString();
	
			}else{
				return _StatusCommunicationText ;
			}			
        }
		set{
			_StatusCommunicationText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(CommunicationProjectResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(CommunicationProjectResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(CommunicationProjectResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(CommunicationProjectResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(CommunicationProjectResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(CommunicationProjectResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.ProjectsController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Description", DataPropertyValue = "GuidProject", FiltrablePropertyPathName="Project.GuidProject")]	

	[LocalizedDisplayName("PROJECT"/*, NameResourceType=typeof(CommunicationProjectResources)*/)]
	public Guid  ? FkProject { get; set; }
		[LocalizedDisplayName("PROJECT"/*, NameResourceType=typeof(CommunicationProjectResources)*/)]
	[Exportable()]
	public string  FkProjectText { get; set; }
    public string FkProjectSafeKey { get; set; }

	
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.UserProfilesController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Name", DataPropertyValue = "GuidUser", FiltrablePropertyPathName="UserProfile.GuidUser")]	

	[LocalizedDisplayName("USERPROFILE"/*, NameResourceType=typeof(CommunicationProjectResources)*/)]
	public Guid  ? FkUserProfile { get; set; }
		[LocalizedDisplayName("USERPROFILE"/*, NameResourceType=typeof(CommunicationProjectResources)*/)]
	[Exportable()]
	public string  FkUserProfileText { get; set; }
    public string FkUserProfileSafeKey { get; set; }

	
		
		
	public override string SafeKey
   	{
		get
        {
			if(this.GuidCommunicationProject != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidCommunicationProject").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(CommunicationProjectModel model){
            
		this.GuidCommunicationProject = model.GuidCommunicationProject;
		this.GuidProject = model.GuidProject;
		this.GuidProfile = model.GuidProfile;
		this.Description = model.Description;
		this.PublishDate = model.PublishDate;
		this.StatusCommunication = model.StatusCommunication;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.CommunicationProject GetBusinessObject()
        {
            BusinessObjects.CommunicationProject result = new BusinessObjects.CommunicationProject();


			       
	if (this.GuidCommunicationProject != null )
				result.GuidCommunicationProject = (Guid)this.GuidCommunicationProject;
				
	if (this.GuidProject != null )
				result.GuidProject = (Guid)this.GuidProject;
				
	if (this.GuidProfile != null )
				result.GuidProfile = (Guid)this.GuidProfile;
				
	if (this.Description != null )
				result.Description = (String)this.Description.Trim().Replace("\t", String.Empty);
				
				result.PublishDate = (DateTime)this.PublishDate;		
				
	if (this.StatusCommunication != null )
				result.StatusCommunication = (Int32)this.StatusCommunication;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
			result.Project = new BusinessObjects.Project() { GuidProject= (Guid)this.FkProject };
				
			result.UserProfile = new BusinessObjects.UserProfile() { GuidUser= (Guid)this.FkUserProfile };
				

            return result;
        }
        public void Bind(BusinessObjects.CommunicationProject businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidCommunicationProject = businessObject.GuidCommunicationProject;
				
			this.GuidProject = businessObject.GuidProject;
				
			this.GuidProfile = businessObject.GuidProfile;
				
			this.Description = businessObject.Description != null ? businessObject.Description.Trim().Replace("\t", String.Empty) : "";
				
				this.PublishDate = (DateTime)businessObject.PublishDate;
			this.StatusCommunication = businessObject.StatusCommunication;
				
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
	        if (businessObject.Project != null){
	                	this.FkProjectText = businessObject.Project.Description != null ? businessObject.Project.Description.ToString() : "";; 
										
										
				this.FkProject = businessObject.Project.GuidProject;
                this.FkProjectSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.Project,"GuidProject").Replace("/","-");

			}
	        if (businessObject.UserProfile != null){
	                	this.FkUserProfileText = businessObject.UserProfile.Name != null ? businessObject.UserProfile.Name.ToString() : "";; 
										
										
				this.FkUserProfile = businessObject.UserProfile.GuidUser;
                this.FkUserProfileSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.UserProfile,"GuidUser").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.LivingArt.Web.Mvc.Models.MusicPreferences 
	{
	public partial class MusicPreferenceModel: ModelBase{

	  public MusicPreferenceModel(BO.MusicPreference resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public MusicPreferenceModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidMusicPreference.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Description != null)
		
            return this.Description.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidMusicPreference{ get; set; }
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("DESCRIPTION"/*, NameResourceType=typeof(MusicPreferenceResources)*/)]
	public String   Description { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(MusicPreferenceResources)*/)]
	public Boolean   IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
			//Aplicar formato si esta especificado
				return IsDeleted.ToString();
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(MusicPreferenceResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(MusicPreferenceResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(MusicPreferenceResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(MusicPreferenceResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(MusicPreferenceResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
		
		[LocalizedDisplayName("PROFILEMUSICPREFERENCES"/*, NameResourceType=typeof(MusicPreferenceResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="ProfileMusicPreferences.Count()", ModelPartialType="ProfileMusicPreferences.ProfileMusicPreference", BusinessObjectSetName = "ProfileMusicPreferences")]
        public List<ProfileMusicPreferences.ProfileMusicPreferenceModel> ProfileMusicPreferences { get; set; }			
	
	public override string SafeKey
   	{
		get
        {
			if(this.GuidMusicPreference != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidMusicPreference").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(MusicPreferenceModel model){
            
		this.GuidMusicPreference = model.GuidMusicPreference;
		this.Description = model.Description;
		this.IsDeleted = model.IsDeleted;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
        }

        public BusinessObjects.MusicPreference GetBusinessObject()
        {
            BusinessObjects.MusicPreference result = new BusinessObjects.MusicPreference();


			       
	if (this.GuidMusicPreference != null )
				result.GuidMusicPreference = (Guid)this.GuidMusicPreference;
				
	if (this.Description != null )
				result.Description = (String)this.Description.Trim().Replace("\t", String.Empty);
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				

            return result;
        }
        public void Bind(BusinessObjects.MusicPreference businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidMusicPreference = businessObject.GuidMusicPreference;
				
				
	if (businessObject.Description != null )
				this.Description = (String)businessObject.Description;
			this.IsDeleted = businessObject.IsDeleted;
				
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
           
        }
	}
}
	namespace MBK.LivingArt.Web.Mvc.Models.PageTypes 
	{
	public partial class PageTypeModel: ModelBase{

	  public PageTypeModel(BO.PageType resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public PageTypeModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidPageType.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Name != null)
		
            return this.Name.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidPageType{ get; set; }
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("NAME"/*, NameResourceType=typeof(PageTypeResources)*/)]
	public String   Name { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[DataType("Integer")]
	[LocalizedDisplayName("PROFILETYPE"/*, NameResourceType=typeof(PageTypeResources)*/)]
	public Int32   ProfileType { get; set; }
	public string _ProfileTypeText = null;
    public string ProfileTypeText {
        get {
			if (string.IsNullOrEmpty( _ProfileTypeText ))
				{
				return ProfileType.ToString();
	
			}else{
				return _ProfileTypeText ;
			}			
        }
		set{
			_ProfileTypeText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(PageTypeResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(PageTypeResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(PageTypeResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(PageTypeResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(PageTypeResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(PageTypeResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
		
		[LocalizedDisplayName("PROFILEPAGES"/*, NameResourceType=typeof(PageTypeResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="ProfilePages.Count()", ModelPartialType="ProfilePages.ProfilePage", BusinessObjectSetName = "ProfilePages")]
        public List<ProfilePages.ProfilePageModel> ProfilePages { get; set; }			
	
		[LocalizedDisplayName("PROJECTCONTRACTS"/*, NameResourceType=typeof(PageTypeResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="ProjectContracts.Count()", ModelPartialType="ProjectContracts.ProjectContract", BusinessObjectSetName = "ProjectContracts")]
        public List<ProjectContracts.ProjectContractModel> ProjectContracts { get; set; }			
	
	public override string SafeKey
   	{
		get
        {
			if(this.GuidPageType != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidPageType").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(PageTypeModel model){
            
		this.GuidPageType = model.GuidPageType;
		this.Name = model.Name;
		this.ProfileType = model.ProfileType;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.PageType GetBusinessObject()
        {
            BusinessObjects.PageType result = new BusinessObjects.PageType();


			       
	if (this.GuidPageType != null )
				result.GuidPageType = (Guid)this.GuidPageType;
				
	if (this.Name != null )
				result.Name = (String)this.Name.Trim().Replace("\t", String.Empty);
				
	if (this.ProfileType != null )
				result.ProfileType = (Int32)this.ProfileType;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				

            return result;
        }
        public void Bind(BusinessObjects.PageType businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidPageType = businessObject.GuidPageType;
				
			this.Name = businessObject.Name != null ? businessObject.Name.Trim().Replace("\t", String.Empty) : "";
				
			this.ProfileType = businessObject.ProfileType;
				
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
           
        }
	}
}
	namespace MBK.LivingArt.Web.Mvc.Models.ProfileBlobs 
	{
	public partial class ProfileBlobModel: ModelBase{

	  public ProfileBlobModel(BO.ProfileBlob resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public ProfileBlobModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidProfileBlob.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.BlobPath != null)
		
            return this.BlobPath.ToString();
			else
				return "";
		
        }    
			

       
		 public string files_LAFile { get; set; }

	
		[SystemProperty()]		
		public Guid? GuidProfileBlob{ get; set; }
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROFILE"/*, NameResourceType=typeof(ProfileBlobResources)*/)]
	public Guid   GuidProfile { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("BLOBPATH"/*, NameResourceType=typeof(ProfileBlobResources)*/)]
	public String   BlobPath { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("BLOBPATHTHUMBNAIL"/*, NameResourceType=typeof(ProfileBlobResources)*/)]
	public String   BlobPathThumbnail { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[DataType("Integer")]
	[LocalizedDisplayName("BLOBTYPE"/*, NameResourceType=typeof(ProfileBlobResources)*/)]
	public Int32   BlobType { get; set; }
	public string _BlobTypeText = null;
    public string BlobTypeText {
        get {
			if (string.IsNullOrEmpty( _BlobTypeText ))
				{
				return BlobType.ToString();
	
			}else{
				return _BlobTypeText ;
			}			
        }
		set{
			_BlobTypeText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(ProfileBlobResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(ProfileBlobResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(ProfileBlobResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(ProfileBlobResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(ProfileBlobResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(ProfileBlobResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDFILE"/*, NameResourceType=typeof(ProfileBlobResources)*/)]
	public Guid  ? GuidFile { get; set; }
		
		
	
[Exportable()]
		
	
 
			//[RelationFilterable( FiltrablePropertyPathName="LAFile.GuidFile")]
			[RelationFilterable(DataClassProvider = typeof(Controllers.LAFilesController), GetByKeyMethod="GetByKey", GetAllMethod = "GetByJson", DataPropertyText = "FileName", DataPropertyValue = "GuidFile", FiltrablePropertyPathName="LAFile.GuidFile")]	

			//1234
	[LookUp("MBK.LivingArt", "MBKLivingArt","LAFiles", "ListViewGen", "FileName", "GuidFile")]	

	//TODO: Hacer dinamicos los campos. Ya existe la funcionalidad
	[FileData("FileData", "FileName", "FileType", "FileSize", "GuidFile", "LAFiles.LAFileModel", true)]
				[LocalizedDisplayName("LAFILE"/*, NameResourceType=typeof(ProfileBlobResources)*/)]
	public Guid  ? FkLAFile { get; set; }
		[LocalizedDisplayName("LAFILE"/*, NameResourceType=typeof(ProfileBlobResources)*/)]
	[Exportable()]
	public string  FkLAFileText { get; set; }
    public string FkLAFileSafeKey { get; set; }

	
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.UserProfilesController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Name", DataPropertyValue = "GuidUser", FiltrablePropertyPathName="UserProfile.GuidUser")]	

	[LocalizedDisplayName("USERPROFILE"/*, NameResourceType=typeof(ProfileBlobResources)*/)]
	public Guid  ? FkUserProfile { get; set; }
		[LocalizedDisplayName("USERPROFILE"/*, NameResourceType=typeof(ProfileBlobResources)*/)]
	[Exportable()]
	public string  FkUserProfileText { get; set; }
    public string FkUserProfileSafeKey { get; set; }

	
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable(DisableFilterableInSubfilter=true)]

	[LocalizedDisplayName("EXISTFILE"/*, NameResourceType=typeof(ProfileBlobResources)*/)]
	public Boolean   ExistFile { get; set; }
	public string _ExistFileText = null;
    public string ExistFileText {
        get {
			if (string.IsNullOrEmpty( _ExistFileText ))
				{
			//Aplicar formato si esta especificado
				return ExistFile.ToString();
	
			}else{
				return _ExistFileText ;
			}			
        }
		set{
			_ExistFileText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable(DisableFilterableInSubfilter=true)]

	[DataType("RichEditorAdvanced")]
	[LocalizedDisplayName("FILENAME"/*, NameResourceType=typeof(ProfileBlobResources)*/)]
	public String   FileName { get; set; }
		
		
		
	public override string SafeKey
   	{
		get
        {
			if(this.GuidProfileBlob != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidProfileBlob").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(ProfileBlobModel model){
            
		this.GuidProfileBlob = model.GuidProfileBlob;
		this.GuidProfile = model.GuidProfile;
		this.BlobPath = model.BlobPath;
		this.BlobPathThumbnail = model.BlobPathThumbnail;
		this.BlobType = model.BlobType;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
		this.GuidFile = model.GuidFile;
		this.ExistFile = model.ExistFile;
		this.FileName = model.FileName;
        }

        public BusinessObjects.ProfileBlob GetBusinessObject()
        {
            BusinessObjects.ProfileBlob result = new BusinessObjects.ProfileBlob();


			       
	   result.files_LAFile = this.files_LAFile;
	if (this.GuidProfileBlob != null )
				result.GuidProfileBlob = (Guid)this.GuidProfileBlob;
				
	if (this.GuidProfile != null )
				result.GuidProfile = (Guid)this.GuidProfile;
				
	if (this.BlobPath != null )
				result.BlobPath = (String)this.BlobPath.Trim().Replace("\t", String.Empty);
				
	if (this.BlobPathThumbnail != null )
				result.BlobPathThumbnail = (String)this.BlobPathThumbnail.Trim().Replace("\t", String.Empty);
				
	if (this.BlobType != null )
				result.BlobType = (Int32)this.BlobType;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
	if (this.GuidFile != null )
				result.GuidFile = (Guid)this.GuidFile;
				
			
			if(this.FkLAFile != null )
			result.LAFile = new BusinessObjects.LAFile() { GuidFile= (Guid)this.FkLAFile };
				
			result.UserProfile = new BusinessObjects.UserProfile() { GuidUser= (Guid)this.FkUserProfile };
				
	if (this.ExistFile != null )
				result.ExistFile = (Boolean)this.ExistFile;
				
	if (this.FileName != null )
				result.FileName = (String)this.FileName.Trim().Replace("\t", String.Empty);
				

            return result;
        }
        public void Bind(BusinessObjects.ProfileBlob businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidProfileBlob = businessObject.GuidProfileBlob;
				
			this.GuidProfile = businessObject.GuidProfile;
				
			this.BlobPath = businessObject.BlobPath != null ? businessObject.BlobPath.Trim().Replace("\t", String.Empty) : "";
				
			this.BlobPathThumbnail = businessObject.BlobPathThumbnail != null ? businessObject.BlobPathThumbnail.Trim().Replace("\t", String.Empty) : "";
				
			this.BlobType = businessObject.BlobType;
				
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
				
	if (businessObject.GuidFile != null )
				this.GuidFile = (Guid)businessObject.GuidFile;
			this.ExistFile = businessObject.ExistFile;
				
				
	if (businessObject.FileName != null )
				this.FileName = (String)businessObject.FileName;
	        if (businessObject.LAFile != null){
	                	this.FkLAFileText = businessObject.LAFile.FileName != null ? businessObject.LAFile.FileName.ToString() : "";; 
										
				this.FkLAFile = businessObject.LAFile.GuidFile;
                this.FkLAFileSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.LAFile,"GuidFile").Replace("/","-");

			}
	        if (businessObject.UserProfile != null){
	                	this.FkUserProfileText = businessObject.UserProfile.Name != null ? businessObject.UserProfile.Name.ToString() : "";; 
										
										
				this.FkUserProfile = businessObject.UserProfile.GuidUser;
                this.FkUserProfileSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.UserProfile,"GuidUser").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.LivingArt.Web.Mvc.Models.ProfileMusicPreferences 
	{
	public partial class ProfileMusicPreferenceModel: ModelBase{

	  public ProfileMusicPreferenceModel(BO.ProfileMusicPreference resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public ProfileMusicPreferenceModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidProfileMusicPreference.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Bytes != null)
		
            return this.Bytes.ToString();
			else
				return "";
		
        }    
			

       
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROFILE"/*, NameResourceType=typeof(ProfileMusicPreferenceResources)*/)]
	public Guid  ? GuidProfile { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDMUSICPREFERENCE"/*, NameResourceType=typeof(ProfileMusicPreferenceResources)*/)]
	public Guid  ? GuidMusicPreference { get; set; }
		
		
	
		[SystemProperty()]		
		public Guid? GuidProfileMusicPreference{ get; set; }
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(ProfileMusicPreferenceResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(ProfileMusicPreferenceResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(ProfileMusicPreferenceResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(ProfileMusicPreferenceResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(ProfileMusicPreferenceResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(ProfileMusicPreferenceResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.MusicPreferencesController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Description", DataPropertyValue = "GuidMusicPreference", FiltrablePropertyPathName="MusicPreference.GuidMusicPreference")]	

	[LocalizedDisplayName("MUSICPREFERENCE"/*, NameResourceType=typeof(ProfileMusicPreferenceResources)*/)]
	public Guid  ? FkMusicPreference { get; set; }
		[LocalizedDisplayName("MUSICPREFERENCE"/*, NameResourceType=typeof(ProfileMusicPreferenceResources)*/)]
	[Exportable()]
	public string  FkMusicPreferenceText { get; set; }
    public string FkMusicPreferenceSafeKey { get; set; }

	
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.UserProfilesController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Name", DataPropertyValue = "GuidUser", FiltrablePropertyPathName="UserProfile.GuidUser")]	

	[LocalizedDisplayName("USERPROFILE"/*, NameResourceType=typeof(ProfileMusicPreferenceResources)*/)]
	public Guid  ? FkUserProfile { get; set; }
		[LocalizedDisplayName("USERPROFILE"/*, NameResourceType=typeof(ProfileMusicPreferenceResources)*/)]
	[Exportable()]
	public string  FkUserProfileText { get; set; }
    public string FkUserProfileSafeKey { get; set; }

	
		
		
	public override string SafeKey
   	{
		get
        {
			if(this.GuidProfileMusicPreference != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidProfileMusicPreference").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(ProfileMusicPreferenceModel model){
            
		this.GuidProfile = model.GuidProfile;
		this.GuidMusicPreference = model.GuidMusicPreference;
		this.GuidProfileMusicPreference = model.GuidProfileMusicPreference;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.ProfileMusicPreference GetBusinessObject()
        {
            BusinessObjects.ProfileMusicPreference result = new BusinessObjects.ProfileMusicPreference();


			       
	if (this.GuidProfile != null )
				result.GuidProfile = (Guid)this.GuidProfile;
				
	if (this.GuidMusicPreference != null )
				result.GuidMusicPreference = (Guid)this.GuidMusicPreference;
				
	if (this.GuidProfileMusicPreference != null )
				result.GuidProfileMusicPreference = (Guid)this.GuidProfileMusicPreference;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
			
			if(this.FkMusicPreference != null )
			result.MusicPreference = new BusinessObjects.MusicPreference() { GuidMusicPreference= (Guid)this.FkMusicPreference };
				
			
			if(this.FkUserProfile != null )
			result.UserProfile = new BusinessObjects.UserProfile() { GuidUser= (Guid)this.FkUserProfile };
				

            return result;
        }
        public void Bind(BusinessObjects.ProfileMusicPreference businessObject)
        {
				this.BusinessObjectObject = businessObject;

				
	if (businessObject.GuidProfile != null )
				this.GuidProfile = (Guid)businessObject.GuidProfile;
				
	if (businessObject.GuidMusicPreference != null )
				this.GuidMusicPreference = (Guid)businessObject.GuidMusicPreference;
			this.GuidProfileMusicPreference = businessObject.GuidProfileMusicPreference;
				
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
	        if (businessObject.MusicPreference != null){
	                	this.FkMusicPreferenceText = businessObject.MusicPreference.Description != null ? businessObject.MusicPreference.Description.ToString() : "";; 
										
				this.FkMusicPreference = businessObject.MusicPreference.GuidMusicPreference;
                this.FkMusicPreferenceSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.MusicPreference,"GuidMusicPreference").Replace("/","-");

			}
	        if (businessObject.UserProfile != null){
	                	this.FkUserProfileText = businessObject.UserProfile.Name != null ? businessObject.UserProfile.Name.ToString() : "";; 
										
										
				this.FkUserProfile = businessObject.UserProfile.GuidUser;
                this.FkUserProfileSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.UserProfile,"GuidUser").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.LivingArt.Web.Mvc.Models.ProfilePageBlobs 
	{
	public partial class ProfilePageBlobModel: ModelBase{

	  public ProfilePageBlobModel(BO.ProfilePageBlob resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public ProfilePageBlobModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidProfilePageBlob.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.BlobPath != null)
		
            return this.BlobPath.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidProfilePageBlob{ get; set; }
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROFILEPAGE"/*, NameResourceType=typeof(ProfilePageBlobResources)*/)]
	public Guid   GuidProfilePage { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("BLOBPATH"/*, NameResourceType=typeof(ProfilePageBlobResources)*/)]
	public String   BlobPath { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("BLOBPATHTHUMBNAIL"/*, NameResourceType=typeof(ProfilePageBlobResources)*/)]
	public String   BlobPathThumbnail { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[DataType("Integer")]
	[LocalizedDisplayName("BLOBTYPE"/*, NameResourceType=typeof(ProfilePageBlobResources)*/)]
	public Int32   BlobType { get; set; }
	public string _BlobTypeText = null;
    public string BlobTypeText {
        get {
			if (string.IsNullOrEmpty( _BlobTypeText ))
				{
				return BlobType.ToString();
	
			}else{
				return _BlobTypeText ;
			}			
        }
		set{
			_BlobTypeText = value;
		}
        
    }

		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[DataType("Integer")]
	[LocalizedDisplayName("BLOBSECTION"/*, NameResourceType=typeof(ProfilePageBlobResources)*/)]
	public Int32   BlobSection { get; set; }
	public string _BlobSectionText = null;
    public string BlobSectionText {
        get {
			if (string.IsNullOrEmpty( _BlobSectionText ))
				{
				return BlobSection.ToString();
	
			}else{
				return _BlobSectionText ;
			}			
        }
		set{
			_BlobSectionText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(ProfilePageBlobResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(ProfilePageBlobResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(ProfilePageBlobResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(ProfilePageBlobResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(ProfilePageBlobResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(ProfilePageBlobResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.ProfilePagesController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Name", DataPropertyValue = "GuidProfilePage", FiltrablePropertyPathName="ProfilePage.GuidProfilePage")]	

	[LocalizedDisplayName("PROFILEPAGE"/*, NameResourceType=typeof(ProfilePageBlobResources)*/)]
	public Guid  ? FkProfilePage { get; set; }
		[LocalizedDisplayName("PROFILEPAGE"/*, NameResourceType=typeof(ProfilePageBlobResources)*/)]
	[Exportable()]
	public string  FkProfilePageText { get; set; }
    public string FkProfilePageSafeKey { get; set; }

	
		
		
	public override string SafeKey
   	{
		get
        {
			if(this.GuidProfilePageBlob != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidProfilePageBlob").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(ProfilePageBlobModel model){
            
		this.GuidProfilePageBlob = model.GuidProfilePageBlob;
		this.GuidProfilePage = model.GuidProfilePage;
		this.BlobPath = model.BlobPath;
		this.BlobPathThumbnail = model.BlobPathThumbnail;
		this.BlobType = model.BlobType;
		this.BlobSection = model.BlobSection;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.ProfilePageBlob GetBusinessObject()
        {
            BusinessObjects.ProfilePageBlob result = new BusinessObjects.ProfilePageBlob();


			       
	if (this.GuidProfilePageBlob != null )
				result.GuidProfilePageBlob = (Guid)this.GuidProfilePageBlob;
				
	if (this.GuidProfilePage != null )
				result.GuidProfilePage = (Guid)this.GuidProfilePage;
				
	if (this.BlobPath != null )
				result.BlobPath = (String)this.BlobPath.Trim().Replace("\t", String.Empty);
				
	if (this.BlobPathThumbnail != null )
				result.BlobPathThumbnail = (String)this.BlobPathThumbnail.Trim().Replace("\t", String.Empty);
				
	if (this.BlobType != null )
				result.BlobType = (Int32)this.BlobType;
				
	if (this.BlobSection != null )
				result.BlobSection = (Int32)this.BlobSection;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
			result.ProfilePage = new BusinessObjects.ProfilePage() { GuidProfilePage= (Guid)this.FkProfilePage };
				

            return result;
        }
        public void Bind(BusinessObjects.ProfilePageBlob businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidProfilePageBlob = businessObject.GuidProfilePageBlob;
				
			this.GuidProfilePage = businessObject.GuidProfilePage;
				
			this.BlobPath = businessObject.BlobPath != null ? businessObject.BlobPath.Trim().Replace("\t", String.Empty) : "";
				
			this.BlobPathThumbnail = businessObject.BlobPathThumbnail != null ? businessObject.BlobPathThumbnail.Trim().Replace("\t", String.Empty) : "";
				
			this.BlobType = businessObject.BlobType;
				
			this.BlobSection = businessObject.BlobSection;
				
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
	        if (businessObject.ProfilePage != null){
	                	this.FkProfilePageText = businessObject.ProfilePage.Name != null ? businessObject.ProfilePage.Name.ToString() : "";; 
										
										
				this.FkProfilePage = businessObject.ProfilePage.GuidProfilePage;
                this.FkProfilePageSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.ProfilePage,"GuidProfilePage").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.LivingArt.Web.Mvc.Models.ProfilePageStores 
	{
	public partial class ProfilePageStoreModel: ModelBase{

	  public ProfilePageStoreModel(BO.ProfilePageStore resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public ProfilePageStoreModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidProfilePageStore.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Name != null)
		
            return this.Name.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidProfilePageStore{ get; set; }
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROFILEPAGE"/*, NameResourceType=typeof(ProfilePageStoreResources)*/)]
	public Guid   GuidProfilePage { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("NAME"/*, NameResourceType=typeof(ProfilePageStoreResources)*/)]
	public String   Name { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("ADDRESS"/*, NameResourceType=typeof(ProfilePageStoreResources)*/)]
	public String   Address { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(ProfilePageStoreResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(ProfilePageStoreResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(ProfilePageStoreResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(ProfilePageStoreResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(ProfilePageStoreResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(ProfilePageStoreResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.ProfilePagesController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Name", DataPropertyValue = "GuidProfilePage", FiltrablePropertyPathName="ProfilePage.GuidProfilePage")]	

	[LocalizedDisplayName("PROFILEPAGE"/*, NameResourceType=typeof(ProfilePageStoreResources)*/)]
	public Guid  ? FkProfilePage { get; set; }
		[LocalizedDisplayName("PROFILEPAGE"/*, NameResourceType=typeof(ProfilePageStoreResources)*/)]
	[Exportable()]
	public string  FkProfilePageText { get; set; }
    public string FkProfilePageSafeKey { get; set; }

	
		
		
	public override string SafeKey
   	{
		get
        {
			if(this.GuidProfilePageStore != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidProfilePageStore").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(ProfilePageStoreModel model){
            
		this.GuidProfilePageStore = model.GuidProfilePageStore;
		this.GuidProfilePage = model.GuidProfilePage;
		this.Name = model.Name;
		this.Address = model.Address;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.ProfilePageStore GetBusinessObject()
        {
            BusinessObjects.ProfilePageStore result = new BusinessObjects.ProfilePageStore();


			       
	if (this.GuidProfilePageStore != null )
				result.GuidProfilePageStore = (Guid)this.GuidProfilePageStore;
				
	if (this.GuidProfilePage != null )
				result.GuidProfilePage = (Guid)this.GuidProfilePage;
				
	if (this.Name != null )
				result.Name = (String)this.Name.Trim().Replace("\t", String.Empty);
				
	if (this.Address != null )
				result.Address = (String)this.Address.Trim().Replace("\t", String.Empty);
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
			result.ProfilePage = new BusinessObjects.ProfilePage() { GuidProfilePage= (Guid)this.FkProfilePage };
				

            return result;
        }
        public void Bind(BusinessObjects.ProfilePageStore businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidProfilePageStore = businessObject.GuidProfilePageStore;
				
			this.GuidProfilePage = businessObject.GuidProfilePage;
				
			this.Name = businessObject.Name != null ? businessObject.Name.Trim().Replace("\t", String.Empty) : "";
				
			this.Address = businessObject.Address != null ? businessObject.Address.Trim().Replace("\t", String.Empty) : "";
				
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
	        if (businessObject.ProfilePage != null){
	                	this.FkProfilePageText = businessObject.ProfilePage.Name != null ? businessObject.ProfilePage.Name.ToString() : "";; 
										
										
				this.FkProfilePage = businessObject.ProfilePage.GuidProfilePage;
                this.FkProfilePageSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.ProfilePage,"GuidProfilePage").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.LivingArt.Web.Mvc.Models.ProfilePageTermContracts 
	{
	public partial class ProfilePageTermContractModel: ModelBase{

	  public ProfilePageTermContractModel(BO.ProfilePageTermContract resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public ProfilePageTermContractModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidProfilePageTermContact.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Description != null)
		
            return this.Description.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidProfilePageTermContact{ get; set; }
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROFILEPAGE"/*, NameResourceType=typeof(ProfilePageTermContractResources)*/)]
	public Guid   GuidProfilePage { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("DESCRIPTION"/*, NameResourceType=typeof(ProfilePageTermContractResources)*/)]
	public String   Description { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(ProfilePageTermContractResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(ProfilePageTermContractResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(ProfilePageTermContractResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(ProfilePageTermContractResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(ProfilePageTermContractResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(ProfilePageTermContractResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.ProfilePagesController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Name", DataPropertyValue = "GuidProfilePage", FiltrablePropertyPathName="ProfilePage.GuidProfilePage")]	

	[LocalizedDisplayName("PROFILEPAGE"/*, NameResourceType=typeof(ProfilePageTermContractResources)*/)]
	public Guid  ? FkProfilePage { get; set; }
		[LocalizedDisplayName("PROFILEPAGE"/*, NameResourceType=typeof(ProfilePageTermContractResources)*/)]
	[Exportable()]
	public string  FkProfilePageText { get; set; }
    public string FkProfilePageSafeKey { get; set; }

	
		
		
	public override string SafeKey
   	{
		get
        {
			if(this.GuidProfilePageTermContact != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidProfilePageTermContact").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(ProfilePageTermContractModel model){
            
		this.GuidProfilePageTermContact = model.GuidProfilePageTermContact;
		this.GuidProfilePage = model.GuidProfilePage;
		this.Description = model.Description;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.ProfilePageTermContract GetBusinessObject()
        {
            BusinessObjects.ProfilePageTermContract result = new BusinessObjects.ProfilePageTermContract();


			       
	if (this.GuidProfilePageTermContact != null )
				result.GuidProfilePageTermContact = (Guid)this.GuidProfilePageTermContact;
				
	if (this.GuidProfilePage != null )
				result.GuidProfilePage = (Guid)this.GuidProfilePage;
				
	if (this.Description != null )
				result.Description = (String)this.Description.Trim().Replace("\t", String.Empty);
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
			result.ProfilePage = new BusinessObjects.ProfilePage() { GuidProfilePage= (Guid)this.FkProfilePage };
				

            return result;
        }
        public void Bind(BusinessObjects.ProfilePageTermContract businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidProfilePageTermContact = businessObject.GuidProfilePageTermContact;
				
			this.GuidProfilePage = businessObject.GuidProfilePage;
				
			this.Description = businessObject.Description != null ? businessObject.Description.Trim().Replace("\t", String.Empty) : "";
				
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
	        if (businessObject.ProfilePage != null){
	                	this.FkProfilePageText = businessObject.ProfilePage.Name != null ? businessObject.ProfilePage.Name.ToString() : "";; 
										
										
				this.FkProfilePage = businessObject.ProfilePage.GuidProfilePage;
                this.FkProfilePageSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.ProfilePage,"GuidProfilePage").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.LivingArt.Web.Mvc.Models.Projects 
	{
	public partial class ProjectModel: ModelBase{

	  public ProjectModel(BO.Project resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public ProjectModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidProject.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Description != null)
		
            return this.Description.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidProject{ get; set; }
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROFILE"/*, NameResourceType=typeof(ProjectResources)*/)]
	public Guid   GuidProfile { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("DESCRIPTION"/*, NameResourceType=typeof(ProjectResources)*/)]
	public String   Description { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("PUBLISHDATE"/*, NameResourceType=typeof(ProjectResources)*/)]
	public DateTime   PublishDate { get; set; }
	public string PublishDateText {
        get {
            if (PublishDate != null)
				return ((DateTime)PublishDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.PublishDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("PROJECTDATE"/*, NameResourceType=typeof(ProjectResources)*/)]
	public DateTime   ProjectDate { get; set; }
	public string ProjectDateText {
        get {
            if (ProjectDate != null)
				return ((DateTime)ProjectDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.ProjectDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("ISEXTERNAL"/*, NameResourceType=typeof(ProjectResources)*/)]
	public Boolean   IsExternal { get; set; }
	public string _IsExternalText = null;
    public string IsExternalText {
        get {
			if (string.IsNullOrEmpty( _IsExternalText ))
				{
			//Aplicar formato si esta especificado
				return IsExternal.ToString();
	
			}else{
				return _IsExternalText ;
			}			
        }
		set{
			_IsExternalText = value;
		}
        
    }

		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("SURFACE"/*, NameResourceType=typeof(ProjectResources)*/)]
	public Decimal   Surface { get; set; }
	public string _SurfaceText = null;
    public string SurfaceText {
        get {
			if (string.IsNullOrEmpty( _SurfaceText ))
				{
				return Surface.ToString();
	
			}else{
				return _SurfaceText ;
			}			
        }
		set{
			_SurfaceText = value;
		}
        
    }

		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("MONEY"/*, NameResourceType=typeof(ProjectResources)*/)]
	public Decimal   Money { get; set; }
	public string _MoneyText = null;
    public string MoneyText {
        get {
			if (string.IsNullOrEmpty( _MoneyText ))
				{
				return Money.ToString();
	
			}else{
				return _MoneyText ;
			}			
        }
		set{
			_MoneyText = value;
		}
        
    }

		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[DataType("Integer")]
	[LocalizedDisplayName("STATUSPROJECT"/*, NameResourceType=typeof(ProjectResources)*/)]
	public Int32   StatusProject { get; set; }
	public string _StatusProjectText = null;
    public string StatusProjectText {
        get {
			if (string.IsNullOrEmpty( _StatusProjectText ))
				{
				return StatusProject.ToString();
	
			}else{
				return _StatusProjectText ;
			}			
        }
		set{
			_StatusProjectText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(ProjectResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(ProjectResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(ProjectResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(ProjectResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(ProjectResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(ProjectResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.UserProfilesController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Name", DataPropertyValue = "GuidUser", FiltrablePropertyPathName="UserProfile.GuidUser")]	

	[LocalizedDisplayName("USERPROFILE"/*, NameResourceType=typeof(ProjectResources)*/)]
	public Guid  ? FkUserProfile { get; set; }
		[LocalizedDisplayName("USERPROFILE"/*, NameResourceType=typeof(ProjectResources)*/)]
	[Exportable()]
	public string  FkUserProfileText { get; set; }
    public string FkUserProfileSafeKey { get; set; }

	
		
		
		[LocalizedDisplayName("COMMUNICATIONPROJECTS"/*, NameResourceType=typeof(ProjectResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="CommunicationProjects.Count()", ModelPartialType="CommunicationProjects.CommunicationProject", BusinessObjectSetName = "CommunicationProjects")]
        public List<CommunicationProjects.CommunicationProjectModel> CommunicationProjects { get; set; }			
	
		[LocalizedDisplayName("PROJECTINCIDENTS"/*, NameResourceType=typeof(ProjectResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="ProjectIncidents.Count()", ModelPartialType="ProjectIncidents.ProjectIncident", BusinessObjectSetName = "ProjectIncidents")]
        public List<ProjectIncidents.ProjectIncidentModel> ProjectIncidents { get; set; }			
	
		[LocalizedDisplayName("PROJECTSQUESTIONS"/*, NameResourceType=typeof(ProjectResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="ProjectsQuestions.Count()", ModelPartialType="ProjectsQuestions.ProjectsQuestion", BusinessObjectSetName = "ProjectsQuestions")]
        public List<ProjectsQuestions.ProjectsQuestionModel> ProjectsQuestions { get; set; }			
	
		[LocalizedDisplayName("PROJECTCONTRACTS"/*, NameResourceType=typeof(ProjectResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="ProjectContracts.Count()", ModelPartialType="ProjectContracts.ProjectContract", BusinessObjectSetName = "ProjectContracts")]
        public List<ProjectContracts.ProjectContractModel> ProjectContracts { get; set; }			
	
	public override string SafeKey
   	{
		get
        {
			if(this.GuidProject != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidProject").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(ProjectModel model){
            
		this.GuidProject = model.GuidProject;
		this.GuidProfile = model.GuidProfile;
		this.Description = model.Description;
		this.PublishDate = model.PublishDate;
		this.ProjectDate = model.ProjectDate;
		this.IsExternal = model.IsExternal;
		this.Surface = model.Surface;
		this.Money = model.Money;
		this.StatusProject = model.StatusProject;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.Project GetBusinessObject()
        {
            BusinessObjects.Project result = new BusinessObjects.Project();


			       
	if (this.GuidProject != null )
				result.GuidProject = (Guid)this.GuidProject;
				
	if (this.GuidProfile != null )
				result.GuidProfile = (Guid)this.GuidProfile;
				
	if (this.Description != null )
				result.Description = (String)this.Description.Trim().Replace("\t", String.Empty);
				
				result.PublishDate = (DateTime)this.PublishDate;		
				
				result.ProjectDate = (DateTime)this.ProjectDate;		
				
	if (this.IsExternal != null )
				result.IsExternal = (Boolean)this.IsExternal;
				
	if (this.Surface != null )
				result.Surface = (Decimal)this.Surface;
				
	if (this.Money != null )
				result.Money = (Decimal)this.Money;
				
	if (this.StatusProject != null )
				result.StatusProject = (Int32)this.StatusProject;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
			result.UserProfile = new BusinessObjects.UserProfile() { GuidUser= (Guid)this.FkUserProfile };
				

            return result;
        }
        public void Bind(BusinessObjects.Project businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidProject = businessObject.GuidProject;
				
			this.GuidProfile = businessObject.GuidProfile;
				
			this.Description = businessObject.Description != null ? businessObject.Description.Trim().Replace("\t", String.Empty) : "";
				
				this.PublishDate = (DateTime)businessObject.PublishDate;
				this.ProjectDate = (DateTime)businessObject.ProjectDate;
			this.IsExternal = businessObject.IsExternal;
				
			this.Surface = businessObject.Surface;
				
			this.Money = businessObject.Money;
				
			this.StatusProject = businessObject.StatusProject;
				
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
	        if (businessObject.UserProfile != null){
	                	this.FkUserProfileText = businessObject.UserProfile.Name != null ? businessObject.UserProfile.Name.ToString() : "";; 
										
										
				this.FkUserProfile = businessObject.UserProfile.GuidUser;
                this.FkUserProfileSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.UserProfile,"GuidUser").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.LivingArt.Web.Mvc.Models.ProjectIncidents 
	{
	public partial class ProjectIncidentModel: ModelBase{

	  public ProjectIncidentModel(BO.ProjectIncident resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public ProjectIncidentModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidProjectIncident.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Subject != null)
		
            return this.Subject.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidProjectIncident{ get; set; }
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROJECT"/*, NameResourceType=typeof(ProjectIncidentResources)*/)]
	public Guid   GuidProject { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROFILE"/*, NameResourceType=typeof(ProjectIncidentResources)*/)]
	public Guid   GuidProfile { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("SUBJECT"/*, NameResourceType=typeof(ProjectIncidentResources)*/)]
	public String   Subject { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("PUBLISHDATE"/*, NameResourceType=typeof(ProjectIncidentResources)*/)]
	public DateTime   PublishDate { get; set; }
	public string PublishDateText {
        get {
            if (PublishDate != null)
				return ((DateTime)PublishDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.PublishDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[DataType("Integer")]
	[LocalizedDisplayName("STATUSINCIDENT"/*, NameResourceType=typeof(ProjectIncidentResources)*/)]
	public Int32   StatusIncident { get; set; }
	public string _StatusIncidentText = null;
    public string StatusIncidentText {
        get {
			if (string.IsNullOrEmpty( _StatusIncidentText ))
				{
				return StatusIncident.ToString();
	
			}else{
				return _StatusIncidentText ;
			}			
        }
		set{
			_StatusIncidentText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(ProjectIncidentResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(ProjectIncidentResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(ProjectIncidentResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(ProjectIncidentResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(ProjectIncidentResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(ProjectIncidentResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.ProjectsController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Description", DataPropertyValue = "GuidProject", FiltrablePropertyPathName="Project.GuidProject")]	

	[LocalizedDisplayName("PROJECT"/*, NameResourceType=typeof(ProjectIncidentResources)*/)]
	public Guid  ? FkProject { get; set; }
		[LocalizedDisplayName("PROJECT"/*, NameResourceType=typeof(ProjectIncidentResources)*/)]
	[Exportable()]
	public string  FkProjectText { get; set; }
    public string FkProjectSafeKey { get; set; }

	
		
		
		[LocalizedDisplayName("PROJECTINCIDENTDETAILS"/*, NameResourceType=typeof(ProjectIncidentResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="ProjectIncidentDetails.Count()", ModelPartialType="ProjectIncidentDetails.ProjectIncidentDetail", BusinessObjectSetName = "ProjectIncidentDetails")]
        public List<ProjectIncidentDetails.ProjectIncidentDetailModel> ProjectIncidentDetails { get; set; }			
	
	public override string SafeKey
   	{
		get
        {
			if(this.GuidProjectIncident != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidProjectIncident").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(ProjectIncidentModel model){
            
		this.GuidProjectIncident = model.GuidProjectIncident;
		this.GuidProject = model.GuidProject;
		this.GuidProfile = model.GuidProfile;
		this.Subject = model.Subject;
		this.PublishDate = model.PublishDate;
		this.StatusIncident = model.StatusIncident;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.ProjectIncident GetBusinessObject()
        {
            BusinessObjects.ProjectIncident result = new BusinessObjects.ProjectIncident();


			       
	if (this.GuidProjectIncident != null )
				result.GuidProjectIncident = (Guid)this.GuidProjectIncident;
				
	if (this.GuidProject != null )
				result.GuidProject = (Guid)this.GuidProject;
				
	if (this.GuidProfile != null )
				result.GuidProfile = (Guid)this.GuidProfile;
				
	if (this.Subject != null )
				result.Subject = (String)this.Subject.Trim().Replace("\t", String.Empty);
				
				result.PublishDate = (DateTime)this.PublishDate;		
				
	if (this.StatusIncident != null )
				result.StatusIncident = (Int32)this.StatusIncident;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
			result.Project = new BusinessObjects.Project() { GuidProject= (Guid)this.FkProject };
				

            return result;
        }
        public void Bind(BusinessObjects.ProjectIncident businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidProjectIncident = businessObject.GuidProjectIncident;
				
			this.GuidProject = businessObject.GuidProject;
				
			this.GuidProfile = businessObject.GuidProfile;
				
			this.Subject = businessObject.Subject != null ? businessObject.Subject.Trim().Replace("\t", String.Empty) : "";
				
				this.PublishDate = (DateTime)businessObject.PublishDate;
			this.StatusIncident = businessObject.StatusIncident;
				
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
	        if (businessObject.Project != null){
	                	this.FkProjectText = businessObject.Project.Description != null ? businessObject.Project.Description.ToString() : "";; 
										
										
				this.FkProject = businessObject.Project.GuidProject;
                this.FkProjectSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.Project,"GuidProject").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.LivingArt.Web.Mvc.Models.ProjectProfileRatings 
	{
	public partial class ProjectProfileRatingModel: ModelBase{

	  public ProjectProfileRatingModel(BO.ProjectProfileRating resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public ProjectProfileRatingModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidProjectProfileRating.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Comment != null)
		
            return this.Comment.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidProjectProfileRating{ get; set; }
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROFILE"/*, NameResourceType=typeof(ProjectProfileRatingResources)*/)]
	public Guid   GuidProfile { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROJECTCONTRACT"/*, NameResourceType=typeof(ProjectProfileRatingResources)*/)]
	public Guid   GuidProjectContract { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("COMMENT"/*, NameResourceType=typeof(ProjectProfileRatingResources)*/)]
	public String   Comment { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[DataType("Integer")]
	[LocalizedDisplayName("CALIFICATION"/*, NameResourceType=typeof(ProjectProfileRatingResources)*/)]
	public Int32   Calification { get; set; }
	public string _CalificationText = null;
    public string CalificationText {
        get {
			if (string.IsNullOrEmpty( _CalificationText ))
				{
				return Calification.ToString();
	
			}else{
				return _CalificationText ;
			}			
        }
		set{
			_CalificationText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(ProjectProfileRatingResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(ProjectProfileRatingResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(ProjectProfileRatingResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(ProjectProfileRatingResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(ProjectProfileRatingResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(ProjectProfileRatingResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.ProjectContractsController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Description", DataPropertyValue = "GuidProjectContract", FiltrablePropertyPathName="ProjectContract.GuidProjectContract")]	

	[LocalizedDisplayName("PROJECTCONTRACT"/*, NameResourceType=typeof(ProjectProfileRatingResources)*/)]
	public Guid  ? FkProjectContract { get; set; }
		[LocalizedDisplayName("PROJECTCONTRACT"/*, NameResourceType=typeof(ProjectProfileRatingResources)*/)]
	[Exportable()]
	public string  FkProjectContractText { get; set; }
    public string FkProjectContractSafeKey { get; set; }

	
		
		
	public override string SafeKey
   	{
		get
        {
			if(this.GuidProjectProfileRating != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidProjectProfileRating").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(ProjectProfileRatingModel model){
            
		this.GuidProjectProfileRating = model.GuidProjectProfileRating;
		this.GuidProfile = model.GuidProfile;
		this.GuidProjectContract = model.GuidProjectContract;
		this.Comment = model.Comment;
		this.Calification = model.Calification;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.ProjectProfileRating GetBusinessObject()
        {
            BusinessObjects.ProjectProfileRating result = new BusinessObjects.ProjectProfileRating();


			       
	if (this.GuidProjectProfileRating != null )
				result.GuidProjectProfileRating = (Guid)this.GuidProjectProfileRating;
				
	if (this.GuidProfile != null )
				result.GuidProfile = (Guid)this.GuidProfile;
				
	if (this.GuidProjectContract != null )
				result.GuidProjectContract = (Guid)this.GuidProjectContract;
				
	if (this.Comment != null )
				result.Comment = (String)this.Comment.Trim().Replace("\t", String.Empty);
				
	if (this.Calification != null )
				result.Calification = (Int32)this.Calification;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
			result.ProjectContract = new BusinessObjects.ProjectContract() { GuidProjectContract= (Guid)this.FkProjectContract };
				

            return result;
        }
        public void Bind(BusinessObjects.ProjectProfileRating businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidProjectProfileRating = businessObject.GuidProjectProfileRating;
				
			this.GuidProfile = businessObject.GuidProfile;
				
			this.GuidProjectContract = businessObject.GuidProjectContract;
				
			this.Comment = businessObject.Comment != null ? businessObject.Comment.Trim().Replace("\t", String.Empty) : "";
				
			this.Calification = businessObject.Calification;
				
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
	        if (businessObject.ProjectContract != null){
	                	this.FkProjectContractText = businessObject.ProjectContract.Description != null ? businessObject.ProjectContract.Description.ToString() : "";; 
										
										
				this.FkProjectContract = businessObject.ProjectContract.GuidProjectContract;
                this.FkProjectContractSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.ProjectContract,"GuidProjectContract").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.LivingArt.Web.Mvc.Models.ProjectsQuestions 
	{
	public partial class ProjectsQuestionModel: ModelBase{

	  public ProjectsQuestionModel(BO.ProjectsQuestion resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public ProjectsQuestionModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidProjectQuestion.ToString();
            }
        }
			
			
        public override string ToString()
        {
		
            return this.StatusQuestion.ToString();
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidProjectQuestion{ get; set; }
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROJECT"/*, NameResourceType=typeof(ProjectsQuestionResources)*/)]
	public Guid   GuidProject { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
 
	[DataType("File")]
	[LocalizedDisplayName("DESCRIPTION"/*, NameResourceType=typeof(ProjectsQuestionResources)*/)]
	public Byte[]   Description { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("PUBLISHDATE"/*, NameResourceType=typeof(ProjectsQuestionResources)*/)]
	public DateTime   PublishDate { get; set; }
	public string PublishDateText {
        get {
            if (PublishDate != null)
				return ((DateTime)PublishDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.PublishDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[DataType("Integer")]
	[LocalizedDisplayName("STATUSQUESTION"/*, NameResourceType=typeof(ProjectsQuestionResources)*/)]
	public Int32   StatusQuestion { get; set; }
	public string _StatusQuestionText = null;
    public string StatusQuestionText {
        get {
			if (string.IsNullOrEmpty( _StatusQuestionText ))
				{
				return StatusQuestion.ToString();
	
			}else{
				return _StatusQuestionText ;
			}			
        }
		set{
			_StatusQuestionText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(ProjectsQuestionResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(ProjectsQuestionResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(ProjectsQuestionResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(ProjectsQuestionResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(ProjectsQuestionResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(ProjectsQuestionResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.ProjectsController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Description", DataPropertyValue = "GuidProject", FiltrablePropertyPathName="Project.GuidProject")]	

	[LocalizedDisplayName("PROJECT"/*, NameResourceType=typeof(ProjectsQuestionResources)*/)]
	public Guid  ? FkProject { get; set; }
		[LocalizedDisplayName("PROJECT"/*, NameResourceType=typeof(ProjectsQuestionResources)*/)]
	[Exportable()]
	public string  FkProjectText { get; set; }
    public string FkProjectSafeKey { get; set; }

	
		
		
	public override string SafeKey
   	{
		get
        {
			if(this.GuidProjectQuestion != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidProjectQuestion").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(ProjectsQuestionModel model){
            
		this.GuidProjectQuestion = model.GuidProjectQuestion;
		this.GuidProject = model.GuidProject;
		this.Description = model.Description;
		this.PublishDate = model.PublishDate;
		this.StatusQuestion = model.StatusQuestion;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.ProjectsQuestion GetBusinessObject()
        {
            BusinessObjects.ProjectsQuestion result = new BusinessObjects.ProjectsQuestion();


			       
	if (this.GuidProjectQuestion != null )
				result.GuidProjectQuestion = (Guid)this.GuidProjectQuestion;
				
	if (this.GuidProject != null )
				result.GuidProject = (Guid)this.GuidProject;
				
				if(this.Description != null)
					result.Description = (Byte[])this.Description;
			
				
				result.PublishDate = (DateTime)this.PublishDate;		
				
	if (this.StatusQuestion != null )
				result.StatusQuestion = (Int32)this.StatusQuestion;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
			result.Project = new BusinessObjects.Project() { GuidProject= (Guid)this.FkProject };
				

            return result;
        }
        public void Bind(BusinessObjects.ProjectsQuestion businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidProjectQuestion = businessObject.GuidProjectQuestion;
				
			this.GuidProject = businessObject.GuidProject;
				
			if (businessObject.Description != null )
				this.Description = businessObject.Description;			
				this.PublishDate = (DateTime)businessObject.PublishDate;
			this.StatusQuestion = businessObject.StatusQuestion;
				
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
	        if (businessObject.Project != null){
	                	this.FkProjectText = businessObject.Project.Description != null ? businessObject.Project.Description.ToString() : "";; 
										
										
				this.FkProject = businessObject.Project.GuidProject;
                this.FkProjectSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.Project,"GuidProject").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.LivingArt.Web.Mvc.Models.ProfilePages 
	{
	public partial class ProfilePageModel: ModelBase{

	  public ProfilePageModel(BO.ProfilePage resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public ProfilePageModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidProfilePage.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Name != null)
		
            return this.Name.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidProfilePage{ get; set; }
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROFILE"/*, NameResourceType=typeof(ProfilePageResources)*/)]
	public Guid   GuidProfile { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPAGETYPE"/*, NameResourceType=typeof(ProfilePageResources)*/)]
	public Guid   GuidPageType { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("NAME"/*, NameResourceType=typeof(ProfilePageResources)*/)]
	public String   Name { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("DESCRIPTION"/*, NameResourceType=typeof(ProfilePageResources)*/)]
	public String   Description { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("ADDRESS"/*, NameResourceType=typeof(ProfilePageResources)*/)]
	public String   Address { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("PHONENUMBER"/*, NameResourceType=typeof(ProfilePageResources)*/)]
	public String   PhoneNumber { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(ProfilePageResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(ProfilePageResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(ProfilePageResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(ProfilePageResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(ProfilePageResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(ProfilePageResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[DataType("RichEditorAdvanced")]
	[LocalizedDisplayName("CONTENTHTML"/*, NameResourceType=typeof(ProfilePageResources)*/)]
	public String   ContentHTML { get; set; }
		
		
	
[Exportable()]
   // [HiddenRequired()]		
		
	
 
			//[RelationFilterable( FiltrablePropertyPathName="PageType.GuidPageType")]
			[RelationFilterable(DataClassProvider = typeof(Controllers.PageTypesController), GetByKeyMethod="GetByKey", GetAllMethod = "GetByJson", DataPropertyText = "Name", DataPropertyValue = "GuidPageType", FiltrablePropertyPathName="PageType.GuidPageType")]	

			//1234
	[AutoComplete("PageTypes", "GetByJson", "filter", "Name", "GuidPageType")]	
			[LocalizedDisplayName("PAGETYPE"/*, NameResourceType=typeof(ProfilePageResources)*/)]
	public Guid  ? FkPageType { get; set; }
		[LocalizedDisplayName("PAGETYPE"/*, NameResourceType=typeof(ProfilePageResources)*/)]
	[Exportable()]
	public string  FkPageTypeText { get; set; }
    public string FkPageTypeSafeKey { get; set; }

	
		
	
[Exportable()]
   // [HiddenRequired()]		
		
	
 
			//[RelationFilterable( FiltrablePropertyPathName="UserProfile.GuidUser")]
			[RelationFilterable(DataClassProvider = typeof(Controllers.UserProfilesController), GetByKeyMethod="GetByKey", GetAllMethod = "GetByJson", DataPropertyText = "Name", DataPropertyValue = "GuidUser", FiltrablePropertyPathName="UserProfile.GuidUser")]	

			//1234
	[LookUp("MBK.LivingArt", "MBKLivingArt","UserProfiles", "ListViewGen", "Name", "GuidUser")]	
			[LocalizedDisplayName("USERPROFILE"/*, NameResourceType=typeof(ProfilePageResources)*/)]
	public Guid  ? FkUserProfile { get; set; }
		[LocalizedDisplayName("USERPROFILE"/*, NameResourceType=typeof(ProfilePageResources)*/)]
	[Exportable()]
	public string  FkUserProfileText { get; set; }
    public string FkUserProfileSafeKey { get; set; }

	
		
		
		[LocalizedDisplayName("PROFILEPAGEBLOBS"/*, NameResourceType=typeof(ProfilePageResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="ProfilePageBlobs.Count()", ModelPartialType="ProfilePageBlobs.ProfilePageBlob", BusinessObjectSetName = "ProfilePageBlobs")]
        public List<ProfilePageBlobs.ProfilePageBlobModel> ProfilePageBlobs { get; set; }			
	
		[LocalizedDisplayName("PROFILEPAGESTORES"/*, NameResourceType=typeof(ProfilePageResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="ProfilePageStores.Count()", ModelPartialType="ProfilePageStores.ProfilePageStore", BusinessObjectSetName = "ProfilePageStores")]
        public List<ProfilePageStores.ProfilePageStoreModel> ProfilePageStores { get; set; }			
	
		[LocalizedDisplayName("PROFILEPAGETERMCONTRACTS"/*, NameResourceType=typeof(ProfilePageResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="ProfilePageTermContracts.Count()", ModelPartialType="ProfilePageTermContracts.ProfilePageTermContract", BusinessObjectSetName = "ProfilePageTermContracts")]
        public List<ProfilePageTermContracts.ProfilePageTermContractModel> ProfilePageTermContracts { get; set; }			
	
		[LocalizedDisplayName("INTERESTEDPAGES"/*, NameResourceType=typeof(ProfilePageResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="InterestedPages.Count()", ModelPartialType="InterestedPages.InterestedPage", BusinessObjectSetName = "InterestedPages")]
        public List<InterestedPages.InterestedPageModel> InterestedPages { get; set; }			
	
	public override string SafeKey
   	{
		get
        {
			if(this.GuidProfilePage != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidProfilePage").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(ProfilePageModel model){
            
		this.GuidProfilePage = model.GuidProfilePage;
		this.GuidProfile = model.GuidProfile;
		this.GuidPageType = model.GuidPageType;
		this.Name = model.Name;
		this.Description = model.Description;
		this.Address = model.Address;
		this.PhoneNumber = model.PhoneNumber;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
		this.ContentHTML = model.ContentHTML;
        }

        public BusinessObjects.ProfilePage GetBusinessObject()
        {
            BusinessObjects.ProfilePage result = new BusinessObjects.ProfilePage();


			       
	if (this.GuidProfilePage != null )
				result.GuidProfilePage = (Guid)this.GuidProfilePage;
				
	if (this.GuidProfile != null )
				result.GuidProfile = (Guid)this.GuidProfile;
				
	if (this.GuidPageType != null )
				result.GuidPageType = (Guid)this.GuidPageType;
				
	if (this.Name != null )
				result.Name = (String)this.Name.Trim().Replace("\t", String.Empty);
				
	if (this.Description != null )
				result.Description = (String)this.Description.Trim().Replace("\t", String.Empty);
				
	if (this.Address != null )
				result.Address = (String)this.Address.Trim().Replace("\t", String.Empty);
				
	if (this.PhoneNumber != null )
				result.PhoneNumber = (String)this.PhoneNumber.Trim().Replace("\t", String.Empty);
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
	if (this.ContentHTML != null )
				result.ContentHTML = (String)this.ContentHTML.Trim().Replace("\t", String.Empty);
				
			result.PageType = new BusinessObjects.PageType() { GuidPageType= (Guid)this.FkPageType };
				
			result.UserProfile = new BusinessObjects.UserProfile() { GuidUser= (Guid)this.FkUserProfile };
				

            return result;
        }
        public void Bind(BusinessObjects.ProfilePage businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidProfilePage = businessObject.GuidProfilePage;
				
			this.GuidProfile = businessObject.GuidProfile;
				
			this.GuidPageType = businessObject.GuidPageType;
				
			this.Name = businessObject.Name != null ? businessObject.Name.Trim().Replace("\t", String.Empty) : "";
				
			this.Description = businessObject.Description != null ? businessObject.Description.Trim().Replace("\t", String.Empty) : "";
				
			this.Address = businessObject.Address != null ? businessObject.Address.Trim().Replace("\t", String.Empty) : "";
				
			this.PhoneNumber = businessObject.PhoneNumber != null ? businessObject.PhoneNumber.Trim().Replace("\t", String.Empty) : "";
				
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
				
	if (businessObject.ContentHTML != null )
				this.ContentHTML = (String)businessObject.ContentHTML;
	        if (businessObject.PageType != null){
	                	this.FkPageTypeText = businessObject.PageType.Name != null ? businessObject.PageType.Name.ToString() : "";; 
										
										
				this.FkPageType = businessObject.PageType.GuidPageType;
                this.FkPageTypeSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.PageType,"GuidPageType").Replace("/","-");

			}
	        if (businessObject.UserProfile != null){
	                	this.FkUserProfileText = businessObject.UserProfile.Name != null ? businessObject.UserProfile.Name.ToString() : "";; 
										
										
				this.FkUserProfile = businessObject.UserProfile.GuidUser;
                this.FkUserProfileSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.UserProfile,"GuidUser").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.LivingArt.Web.Mvc.Models.ProfileEvents 
	{
	public partial class ProfileEventModel: ModelBase{

	  public ProfileEventModel(BO.ProfileEvent resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public ProfileEventModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidProfileEvent.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Description != null)
		
            return this.Description.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidProfileEvent{ get; set; }
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROFILE"/*, NameResourceType=typeof(ProfileEventResources)*/)]
	public Guid   GuidProfile { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("EVENTDATE"/*, NameResourceType=typeof(ProfileEventResources)*/)]
	public DateTime   EventDate { get; set; }
	public string EventDateText {
        get {
            if (EventDate != null)
				return ((DateTime)EventDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.EventDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("DESCRIPTION"/*, NameResourceType=typeof(ProfileEventResources)*/)]
	public String   Description { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROJECT"/*, NameResourceType=typeof(ProfileEventResources)*/)]
	public Guid   GuidProject { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(ProfileEventResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(ProfileEventResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(ProfileEventResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(ProfileEventResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(ProfileEventResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(ProfileEventResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
   // [HiddenRequired()]		
		
	
 
			//[RelationFilterable( FiltrablePropertyPathName="UserProfile.GuidUser")]
			[RelationFilterable(DataClassProvider = typeof(Controllers.UserProfilesController), GetByKeyMethod="GetByKey", GetAllMethod = "GetByJson", DataPropertyText = "Name", DataPropertyValue = "GuidUser", FiltrablePropertyPathName="UserProfile.GuidUser")]	

			//1234
	[AutoComplete("UserProfiles", "GetByJson", "filter", "Name", "GuidUser")]	
			[LocalizedDisplayName("USERPROFILE"/*, NameResourceType=typeof(ProfileEventResources)*/)]
	public Guid  ? FkUserProfile { get; set; }
		[LocalizedDisplayName("USERPROFILE"/*, NameResourceType=typeof(ProfileEventResources)*/)]
	[Exportable()]
	public string  FkUserProfileText { get; set; }
    public string FkUserProfileSafeKey { get; set; }

	
		
		
	public override string SafeKey
   	{
		get
        {
			if(this.GuidProfileEvent != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidProfileEvent").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(ProfileEventModel model){
            
		this.GuidProfileEvent = model.GuidProfileEvent;
		this.GuidProfile = model.GuidProfile;
		this.EventDate = model.EventDate;
		this.Description = model.Description;
		this.GuidProject = model.GuidProject;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.ProfileEvent GetBusinessObject()
        {
            BusinessObjects.ProfileEvent result = new BusinessObjects.ProfileEvent();


			       
	if (this.GuidProfileEvent != null )
				result.GuidProfileEvent = (Guid)this.GuidProfileEvent;
				
	if (this.GuidProfile != null )
				result.GuidProfile = (Guid)this.GuidProfile;
				
				result.EventDate = (DateTime)this.EventDate;		
				
	if (this.Description != null )
				result.Description = (String)this.Description.Trim().Replace("\t", String.Empty);
				
	if (this.GuidProject != null )
				result.GuidProject = (Guid)this.GuidProject;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
	
				if (!string.IsNullOrEmpty(this.FkUserProfileText)){
					if (this.FkUserProfileText.Contains("|")) this.FkUserProfileText = this.FkUserProfileText.Split(char.Parse("|"))[1];
					result.UserProfile = new BusinessObjects.UserProfile() { GuidUser= (Guid)this.FkUserProfile };
				
				}else{
                    if (this.GuidProfile != null)
                    {
                    result.UserProfile = new BusinessObjects.UserProfile() { GuidUser  = this.GuidProfile };
                    }
                else
                    {
                        result.UserProfile = new BusinessObjects.UserProfile() { GuidUser = (Guid)this.FkUserProfile };

                    }
            }
		
				

            return result;
        }
        public void Bind(BusinessObjects.ProfileEvent businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidProfileEvent = businessObject.GuidProfileEvent;
				
			this.GuidProfile = businessObject.GuidProfile;
				
				this.EventDate = (DateTime)businessObject.EventDate;
			this.Description = businessObject.Description != null ? businessObject.Description.Trim().Replace("\t", String.Empty) : "";
				
			this.GuidProject = businessObject.GuidProject;
				
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
	        if (businessObject.UserProfile != null){
	                	this.FkUserProfileText = businessObject.UserProfile.Name != null ? businessObject.UserProfile.Name.ToString() : "";; 
										
										
				this.FkUserProfile = businessObject.UserProfile.GuidUser;
                this.FkUserProfileSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.UserProfile,"GuidUser").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.LivingArt.Web.Mvc.Models.ProjectIncidentDetails 
	{
	public partial class ProjectIncidentDetailModel: ModelBase{

	  public ProjectIncidentDetailModel(BO.ProjectIncidentDetail resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public ProjectIncidentDetailModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidProjectIncidentDetail.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Description != null)
		
            return this.Description.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidProjectIncidentDetail{ get; set; }
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROJECTINCIDENT"/*, NameResourceType=typeof(ProjectIncidentDetailResources)*/)]
	public Guid   GuidProjectIncident { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROFILE"/*, NameResourceType=typeof(ProjectIncidentDetailResources)*/)]
	public Guid   GuidProfile { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("DESCRIPTION"/*, NameResourceType=typeof(ProjectIncidentDetailResources)*/)]
	public String   Description { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("PUBLISHDATE"/*, NameResourceType=typeof(ProjectIncidentDetailResources)*/)]
	public DateTime   PublishDate { get; set; }
	public string PublishDateText {
        get {
            if (PublishDate != null)
				return ((DateTime)PublishDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.PublishDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(ProjectIncidentDetailResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(ProjectIncidentDetailResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(ProjectIncidentDetailResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(ProjectIncidentDetailResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(ProjectIncidentDetailResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(ProjectIncidentDetailResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.ProjectIncidentsController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Subject", DataPropertyValue = "GuidProjectIncident", FiltrablePropertyPathName="ProjectIncident.GuidProjectIncident")]	

	[LocalizedDisplayName("PROJECTINCIDENT"/*, NameResourceType=typeof(ProjectIncidentDetailResources)*/)]
	public Guid  ? FkProjectIncident { get; set; }
		[LocalizedDisplayName("PROJECTINCIDENT"/*, NameResourceType=typeof(ProjectIncidentDetailResources)*/)]
	[Exportable()]
	public string  FkProjectIncidentText { get; set; }
    public string FkProjectIncidentSafeKey { get; set; }

	
		
		
	public override string SafeKey
   	{
		get
        {
			if(this.GuidProjectIncidentDetail != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidProjectIncidentDetail").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(ProjectIncidentDetailModel model){
            
		this.GuidProjectIncidentDetail = model.GuidProjectIncidentDetail;
		this.GuidProjectIncident = model.GuidProjectIncident;
		this.GuidProfile = model.GuidProfile;
		this.Description = model.Description;
		this.PublishDate = model.PublishDate;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.ProjectIncidentDetail GetBusinessObject()
        {
            BusinessObjects.ProjectIncidentDetail result = new BusinessObjects.ProjectIncidentDetail();


			       
	if (this.GuidProjectIncidentDetail != null )
				result.GuidProjectIncidentDetail = (Guid)this.GuidProjectIncidentDetail;
				
	if (this.GuidProjectIncident != null )
				result.GuidProjectIncident = (Guid)this.GuidProjectIncident;
				
	if (this.GuidProfile != null )
				result.GuidProfile = (Guid)this.GuidProfile;
				
	if (this.Description != null )
				result.Description = (String)this.Description.Trim().Replace("\t", String.Empty);
				
				result.PublishDate = (DateTime)this.PublishDate;		
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
			result.ProjectIncident = new BusinessObjects.ProjectIncident() { GuidProjectIncident= (Guid)this.FkProjectIncident };
				

            return result;
        }
        public void Bind(BusinessObjects.ProjectIncidentDetail businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidProjectIncidentDetail = businessObject.GuidProjectIncidentDetail;
				
			this.GuidProjectIncident = businessObject.GuidProjectIncident;
				
			this.GuidProfile = businessObject.GuidProfile;
				
			this.Description = businessObject.Description != null ? businessObject.Description.Trim().Replace("\t", String.Empty) : "";
				
				this.PublishDate = (DateTime)businessObject.PublishDate;
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
	        if (businessObject.ProjectIncident != null){
	                	this.FkProjectIncidentText = businessObject.ProjectIncident.Subject != null ? businessObject.ProjectIncident.Subject.ToString() : "";; 
										
										
				this.FkProjectIncident = businessObject.ProjectIncident.GuidProjectIncident;
                this.FkProjectIncidentSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.ProjectIncident,"GuidProjectIncident").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.LivingArt.Web.Mvc.Models.ProjectContractTerms 
	{
	public partial class ProjectContractTermModel: ModelBase{

	  public ProjectContractTermModel(BO.ProjectContractTerm resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public ProjectContractTermModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.ProjectContractTermId.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Description != null)
		
            return this.Description.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? ProjectContractTermId{ get; set; }
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROJECTCONTRACT"/*, NameResourceType=typeof(ProjectContractTermResources)*/)]
	public Guid   GuidProjectContract { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("DESCRIPTION"/*, NameResourceType=typeof(ProjectContractTermResources)*/)]
	public String   Description { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(ProjectContractTermResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(ProjectContractTermResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(ProjectContractTermResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(ProjectContractTermResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(ProjectContractTermResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(ProjectContractTermResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.ProjectContractsController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Description", DataPropertyValue = "GuidProjectContract", FiltrablePropertyPathName="ProjectContract.GuidProjectContract")]	

	[LocalizedDisplayName("PROJECTCONTRACT"/*, NameResourceType=typeof(ProjectContractTermResources)*/)]
	public Guid  ? FkProjectContract { get; set; }
		[LocalizedDisplayName("PROJECTCONTRACT"/*, NameResourceType=typeof(ProjectContractTermResources)*/)]
	[Exportable()]
	public string  FkProjectContractText { get; set; }
    public string FkProjectContractSafeKey { get; set; }

	
		
		
	public override string SafeKey
   	{
		get
        {
			if(this.ProjectContractTermId != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"ProjectContractTermId").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(ProjectContractTermModel model){
            
		this.ProjectContractTermId = model.ProjectContractTermId;
		this.GuidProjectContract = model.GuidProjectContract;
		this.Description = model.Description;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.ProjectContractTerm GetBusinessObject()
        {
            BusinessObjects.ProjectContractTerm result = new BusinessObjects.ProjectContractTerm();


			       
	if (this.ProjectContractTermId != null )
				result.ProjectContractTermId = (Guid)this.ProjectContractTermId;
				
	if (this.GuidProjectContract != null )
				result.GuidProjectContract = (Guid)this.GuidProjectContract;
				
	if (this.Description != null )
				result.Description = (String)this.Description.Trim().Replace("\t", String.Empty);
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
			result.ProjectContract = new BusinessObjects.ProjectContract() { GuidProjectContract= (Guid)this.FkProjectContract };
				

            return result;
        }
        public void Bind(BusinessObjects.ProjectContractTerm businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.ProjectContractTermId = businessObject.ProjectContractTermId;
				
			this.GuidProjectContract = businessObject.GuidProjectContract;
				
			this.Description = businessObject.Description != null ? businessObject.Description.Trim().Replace("\t", String.Empty) : "";
				
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
	        if (businessObject.ProjectContract != null){
	                	this.FkProjectContractText = businessObject.ProjectContract.Description != null ? businessObject.ProjectContract.Description.ToString() : "";; 
										
										
				this.FkProjectContract = businessObject.ProjectContract.GuidProjectContract;
                this.FkProjectContractSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.ProjectContract,"GuidProjectContract").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.LivingArt.Web.Mvc.Models.LAFiles 
	{
	public partial class LAFileModel: ModelBase{

	  public LAFileModel(BO.LAFile resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public LAFileModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidFile.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.FileName != null)
		
            return this.FileName.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidFile{ get; set; }
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("FILENAME"/*, NameResourceType=typeof(LAFileResources)*/)]
	public String   FileName { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[DataType("Integer")]
	[LocalizedDisplayName("FILESIZE"/*, NameResourceType=typeof(LAFileResources)*/)]
	public Int64  ? FileSize { get; set; }
	public string _FileSizeText = null;
    public string FileSizeText {
        get {
			if (string.IsNullOrEmpty( _FileSizeText ))
				{
	
            if (FileSize != null)
				return FileSize.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _FileSizeText ;
			}			
        }
		set{
			_FileSizeText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("FILETYPE"/*, NameResourceType=typeof(LAFileResources)*/)]
	public String   FileType { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
 
	[DataType("File")]
	[LocalizedDisplayName("FILEDATA"/*, NameResourceType=typeof(LAFileResources)*/)]
	public Byte[]   FileData { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("FILESTORAGE"/*, NameResourceType=typeof(LAFileResources)*/)]
	public String   FileStorage { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("CONTAINERSTORAGE"/*, NameResourceType=typeof(LAFileResources)*/)]
	public String   ContainerStorage { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(LAFileResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(LAFileResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(LAFileResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(LAFileResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(LAFileResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(LAFileResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
		
		[LocalizedDisplayName("PROFILEBLOBS"/*, NameResourceType=typeof(LAFileResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="ProfileBlobs.Count()", ModelPartialType="ProfileBlobs.ProfileBlob", BusinessObjectSetName = "ProfileBlobs")]
        public List<ProfileBlobs.ProfileBlobModel> ProfileBlobs { get; set; }			
	
	public override string SafeKey
   	{
		get
        {
			if(this.GuidFile != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidFile").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(LAFileModel model){
            
		this.GuidFile = model.GuidFile;
		this.FileName = model.FileName;
		this.FileSize = model.FileSize;
		this.FileType = model.FileType;
		this.FileData = model.FileData;
		this.FileStorage = model.FileStorage;
		this.ContainerStorage = model.ContainerStorage;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.LAFile GetBusinessObject()
        {
            BusinessObjects.LAFile result = new BusinessObjects.LAFile();


			       
	if (this.GuidFile != null )
				result.GuidFile = (Guid)this.GuidFile;
				
	if (this.FileName != null )
				result.FileName = (String)this.FileName.Trim().Replace("\t", String.Empty);
				
	if (this.FileSize != null )
				result.FileSize = (Int64)this.FileSize;
				
	if (this.FileType != null )
				result.FileType = (String)this.FileType.Trim().Replace("\t", String.Empty);
				
				if(this.FileData != null)
					result.FileData = (Byte[])this.FileData;
			
				
	if (this.FileStorage != null )
				result.FileStorage = (String)this.FileStorage.Trim().Replace("\t", String.Empty);
				
	if (this.ContainerStorage != null )
				result.ContainerStorage = (String)this.ContainerStorage.Trim().Replace("\t", String.Empty);
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				

            return result;
        }
        public void Bind(BusinessObjects.LAFile businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidFile = businessObject.GuidFile;
				
				
	if (businessObject.FileName != null )
				this.FileName = (String)businessObject.FileName;
				
	if (businessObject.FileSize != null )
				this.FileSize = (Int64)businessObject.FileSize;
				
	if (businessObject.FileType != null )
				this.FileType = (String)businessObject.FileType;
			if (businessObject.FileData != null )
				this.FileData = businessObject.FileData;			
				
	if (businessObject.FileStorage != null )
				this.FileStorage = (String)businessObject.FileStorage;
				
	if (businessObject.ContainerStorage != null )
				this.ContainerStorage = (String)businessObject.ContainerStorage;
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
           
        }
	}
}
	namespace MBK.LivingArt.Web.Mvc.Models.UserProfiles 
	{
	public partial class UserProfileModel: ModelBase{

	  public UserProfileModel(BO.UserProfile resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public UserProfileModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidUser.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Name != null)
		
            return this.Name.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidUser{ get; set; }
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("NAME"/*, NameResourceType=typeof(UserProfileResources)*/)]
	public String   Name { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("ALIAS"/*, NameResourceType=typeof(UserProfileResources)*/)]
	public String   Alias { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("BIOGRAPHY"/*, NameResourceType=typeof(UserProfileResources)*/)]
	public String   Biography { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("BIRTHDATE"/*, NameResourceType=typeof(UserProfileResources)*/)]
	public DateTime  ? Birthdate { get; set; }
	public string BirthdateText {
        get {
            if (Birthdate != null)
				return ((DateTime)Birthdate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.Birthdate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[DataType("Integer")]
	[LocalizedDisplayName("PROFILETYPE"/*, NameResourceType=typeof(UserProfileResources)*/)]
	public Int32   ProfileType { get; set; }
	public string _ProfileTypeText = null;
    public string ProfileTypeText {
        get {
			if (string.IsNullOrEmpty( _ProfileTypeText ))
				{
				return ProfileType.ToString();
	
			}else{
				return _ProfileTypeText ;
			}			
        }
		set{
			_ProfileTypeText = value;
		}
        
    }

		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[DataType("Integer")]
	[LocalizedDisplayName("PROFILESTATUS"/*, NameResourceType=typeof(UserProfileResources)*/)]
	public Int32   ProfileStatus { get; set; }
	public string _ProfileStatusText = null;
    public string ProfileStatusText {
        get {
			if (string.IsNullOrEmpty( _ProfileStatusText ))
				{
				return ProfileStatus.ToString();
	
			}else{
				return _ProfileStatusText ;
			}			
        }
		set{
			_ProfileStatusText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(UserProfileResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(UserProfileResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(UserProfileResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(UserProfileResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(UserProfileResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(UserProfileResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("INTEGRATIONID"/*, NameResourceType=typeof(UserProfileResources)*/)]
	public Guid  ? IntegrationID { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable(DisableFilterableInSubfilter=true)]

	[DataType("Integer")]
	[LocalizedDisplayName("COUNTMUSICPREFERENCESCOMPUTED"/*, NameResourceType=typeof(UserProfileResources)*/)]
	public Int32   CountMusicPreferencesComputed { get; set; }
	public string _CountMusicPreferencesComputedText = null;
    public string CountMusicPreferencesComputedText {
        get {
			if (string.IsNullOrEmpty( _CountMusicPreferencesComputedText ))
				{
                return string.Format("{0:C}", CountMusicPreferencesComputed);
	
			}else{
				return _CountMusicPreferencesComputedText ;
			}			
        }
		set{
			_CountMusicPreferencesComputedText = value;
		}
        
    }

		
		
		
		[LocalizedDisplayName("COMMUNICATIONPROJECTS"/*, NameResourceType=typeof(UserProfileResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="CommunicationProjects.Count()", ModelPartialType="CommunicationProjects.CommunicationProject", BusinessObjectSetName = "CommunicationProjects")]
        public List<CommunicationProjects.CommunicationProjectModel> CommunicationProjects { get; set; }			
	
		[LocalizedDisplayName("PROFILEBLOBS"/*, NameResourceType=typeof(UserProfileResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="ProfileBlobs.Count()", ModelPartialType="ProfileBlobs.ProfileBlob", BusinessObjectSetName = "ProfileBlobs")]
        public List<ProfileBlobs.ProfileBlobModel> ProfileBlobs { get; set; }			
	
		[LocalizedDisplayName("PROFILEEVENTS"/*, NameResourceType=typeof(UserProfileResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="ProfileEvents.Count()", ModelPartialType="ProfileEvents.ProfileEvent", BusinessObjectSetName = "ProfileEvents")]
        public List<ProfileEvents.ProfileEventModel> ProfileEvents { get; set; }			
	
		[LocalizedDisplayName("PROFILEMUSICPREFERENCES"/*, NameResourceType=typeof(UserProfileResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="ProfileMusicPreferences.Count()", ModelPartialType="ProfileMusicPreferences.ProfileMusicPreference", BusinessObjectSetName = "ProfileMusicPreferences")]
        public List<ProfileMusicPreferences.ProfileMusicPreferenceModel> ProfileMusicPreferences { get; set; }			
	
		[LocalizedDisplayName("PROFILEPAGES"/*, NameResourceType=typeof(UserProfileResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="ProfilePages.Count()", ModelPartialType="ProfilePages.ProfilePage", BusinessObjectSetName = "ProfilePages")]
        public List<ProfilePages.ProfilePageModel> ProfilePages { get; set; }			
	
		[LocalizedDisplayName("PROJECTS"/*, NameResourceType=typeof(UserProfileResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="Projects.Count()", ModelPartialType="Projects.Project", BusinessObjectSetName = "Projects")]
        public List<Projects.ProjectModel> Projects { get; set; }			
	
	public override string SafeKey
   	{
		get
        {
			if(this.GuidUser != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidUser").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(UserProfileModel model){
            
		this.GuidUser = model.GuidUser;
		this.Name = model.Name;
		this.Alias = model.Alias;
		this.Biography = model.Biography;
		this.Birthdate = model.Birthdate;
		this.ProfileType = model.ProfileType;
		this.ProfileStatus = model.ProfileStatus;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
		this.IntegrationID = model.IntegrationID;
		this.CountMusicPreferencesComputed = model.CountMusicPreferencesComputed;
        }

        public BusinessObjects.UserProfile GetBusinessObject()
        {
            BusinessObjects.UserProfile result = new BusinessObjects.UserProfile();


			       
	if (this.GuidUser != null )
				result.GuidUser = (Guid)this.GuidUser;
				
	if (this.Name != null )
				result.Name = (String)this.Name.Trim().Replace("\t", String.Empty);
				
	if (this.Alias != null )
				result.Alias = (String)this.Alias.Trim().Replace("\t", String.Empty);
				
	if (this.Biography != null )
				result.Biography = (String)this.Biography.Trim().Replace("\t", String.Empty);
				
				if(this.Birthdate != null)
					if (this.Birthdate != null)
				result.Birthdate = (DateTime)this.Birthdate;		
				
	if (this.ProfileType != null )
				result.ProfileType = (Int32)this.ProfileType;
				
	if (this.ProfileStatus != null )
				result.ProfileStatus = (Int32)this.ProfileStatus;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
	if (this.IntegrationID != null )
				result.IntegrationID = (Guid)this.IntegrationID;
				
	if (this.CountMusicPreferencesComputed != null )
				result.CountMusicPreferencesComputed = (Int32)this.CountMusicPreferencesComputed;
				

            return result;
        }
        public void Bind(BusinessObjects.UserProfile businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidUser = businessObject.GuidUser;
				
			this.Name = businessObject.Name != null ? businessObject.Name.Trim().Replace("\t", String.Empty) : "";
				
				
	if (businessObject.Alias != null )
				this.Alias = (String)businessObject.Alias;
				
	if (businessObject.Biography != null )
				this.Biography = (String)businessObject.Biography;
				if (businessObject.Birthdate != null )
				this.Birthdate = (DateTime)businessObject.Birthdate;
			this.ProfileType = businessObject.ProfileType;
				
			this.ProfileStatus = businessObject.ProfileStatus;
				
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
				
	if (businessObject.IntegrationID != null )
				this.IntegrationID = (Guid)businessObject.IntegrationID;
			this.CountMusicPreferencesComputed = businessObject.CountMusicPreferencesComputed;
				
           
        }
	}
}
	namespace MBK.LivingArt.Web.Mvc.Models.InterestedPages 
	{
	public partial class InterestedPageModel: ModelBase{

	  public InterestedPageModel(BO.InterestedPage resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public InterestedPageModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidInterestedPage.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Bytes != null)
		
            return this.Bytes.ToString();
			else
				return "";
		
        }    
			

       
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROJECTCONTRACT"/*, NameResourceType=typeof(InterestedPageResources)*/)]
	public Guid   GuidProjectContract { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROFILEPAGE"/*, NameResourceType=typeof(InterestedPageResources)*/)]
	public Guid   GuidProfilePage { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("ACCEPTED"/*, NameResourceType=typeof(InterestedPageResources)*/)]
	public Boolean   Accepted { get; set; }
	public string _AcceptedText = null;
    public string AcceptedText {
        get {
			if (string.IsNullOrEmpty( _AcceptedText ))
				{
			//Aplicar formato si esta especificado
				return Accepted.ToString();
	
			}else{
				return _AcceptedText ;
			}			
        }
		set{
			_AcceptedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(InterestedPageResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(InterestedPageResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(InterestedPageResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(InterestedPageResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(InterestedPageResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(InterestedPageResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
		[SystemProperty()]		
		public Guid? GuidInterestedPage{ get; set; }
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.ProfilePagesController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Name", DataPropertyValue = "GuidProfilePage", FiltrablePropertyPathName="ProfilePage.GuidProfilePage")]	

	[LocalizedDisplayName("PROFILEPAGE"/*, NameResourceType=typeof(InterestedPageResources)*/)]
	public Guid  ? FkProfilePage { get; set; }
		[LocalizedDisplayName("PROFILEPAGE"/*, NameResourceType=typeof(InterestedPageResources)*/)]
	[Exportable()]
	public string  FkProfilePageText { get; set; }
    public string FkProfilePageSafeKey { get; set; }

	
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.ProjectContractsController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Description", DataPropertyValue = "GuidProjectContract", FiltrablePropertyPathName="ProjectContract.GuidProjectContract")]	

	[LocalizedDisplayName("PROJECTCONTRACT"/*, NameResourceType=typeof(InterestedPageResources)*/)]
	public Guid  ? FkProjectContract { get; set; }
		[LocalizedDisplayName("PROJECTCONTRACT"/*, NameResourceType=typeof(InterestedPageResources)*/)]
	[Exportable()]
	public string  FkProjectContractText { get; set; }
    public string FkProjectContractSafeKey { get; set; }

	
		
		
	public override string SafeKey
   	{
		get
        {
			if(this.GuidInterestedPage != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidInterestedPage").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(InterestedPageModel model){
            
		this.GuidProjectContract = model.GuidProjectContract;
		this.GuidProfilePage = model.GuidProfilePage;
		this.Accepted = model.Accepted;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
		this.GuidInterestedPage = model.GuidInterestedPage;
        }

        public BusinessObjects.InterestedPage GetBusinessObject()
        {
            BusinessObjects.InterestedPage result = new BusinessObjects.InterestedPage();


			       
	if (this.GuidProjectContract != null )
				result.GuidProjectContract = (Guid)this.GuidProjectContract;
				
	if (this.GuidProfilePage != null )
				result.GuidProfilePage = (Guid)this.GuidProfilePage;
				
	if (this.Accepted != null )
				result.Accepted = (Boolean)this.Accepted;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
	if (this.GuidInterestedPage != null )
				result.GuidInterestedPage = (Guid)this.GuidInterestedPage;
				
			result.ProfilePage = new BusinessObjects.ProfilePage() { GuidProfilePage= (Guid)this.FkProfilePage };
				
			result.ProjectContract = new BusinessObjects.ProjectContract() { GuidProjectContract= (Guid)this.FkProjectContract };
				

            return result;
        }
        public void Bind(BusinessObjects.InterestedPage businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidProjectContract = businessObject.GuidProjectContract;
				
			this.GuidProfilePage = businessObject.GuidProfilePage;
				
			this.Accepted = businessObject.Accepted;
				
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
			this.GuidInterestedPage = businessObject.GuidInterestedPage;
				
	        if (businessObject.ProfilePage != null){
	                	this.FkProfilePageText = businessObject.ProfilePage.Name != null ? businessObject.ProfilePage.Name.ToString() : "";; 
										
										
				this.FkProfilePage = businessObject.ProfilePage.GuidProfilePage;
                this.FkProfilePageSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.ProfilePage,"GuidProfilePage").Replace("/","-");

			}
	        if (businessObject.ProjectContract != null){
	                	this.FkProjectContractText = businessObject.ProjectContract.Description != null ? businessObject.ProjectContract.Description.ToString() : "";; 
										
										
				this.FkProjectContract = businessObject.ProjectContract.GuidProjectContract;
                this.FkProjectContractSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.ProjectContract,"GuidProjectContract").Replace("/","-");

			}
           
        }
	}
}
	namespace MBK.LivingArt.Web.Mvc.Models.ProjectContracts 
	{
	public partial class ProjectContractModel: ModelBase{

	  public ProjectContractModel(BO.ProjectContract resultObj)
        {

            Bind(resultObj);
        }
#region Tags		
#endregion
		public ProjectContractModel()
        {
		}
		public override string Id
        {
            get
            {
                return this.GuidProjectContract.ToString();
            }
        }
			
			
        public override string ToString()
        {
			if (this.Description != null)
		
            return this.Description.ToString();
			else
				return "";
		
        }    
			

       
	
		[SystemProperty()]		
		public Guid? GuidProjectContract{ get; set; }
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPROJECT"/*, NameResourceType=typeof(ProjectContractResources)*/)]
	public Guid   GuidProject { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("DESCRIPTION"/*, NameResourceType=typeof(ProjectContractResources)*/)]
	public String   Description { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("GUIDPAGETYPE"/*, NameResourceType=typeof(ProjectContractResources)*/)]
	public Guid   GuidPageType { get; set; }
		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("MONEY"/*, NameResourceType=typeof(ProjectContractResources)*/)]
	public Decimal   Money { get; set; }
	public string _MoneyText = null;
    public string MoneyText {
        get {
			if (string.IsNullOrEmpty( _MoneyText ))
				{
				return Money.ToString();
	
			}else{
				return _MoneyText ;
			}			
        }
		set{
			_MoneyText = value;
		}
        
    }

		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[DataType("Integer")]
	[LocalizedDisplayName("STATUSPROJECTCONTRACTS"/*, NameResourceType=typeof(ProjectContractResources)*/)]
	public Int32   StatusProjectContracts { get; set; }
	public string _StatusProjectContractsText = null;
    public string StatusProjectContractsText {
        get {
			if (string.IsNullOrEmpty( _StatusProjectContractsText ))
				{
				return StatusProjectContracts.ToString();
	
			}else{
				return _StatusProjectContractsText ;
			}			
        }
		set{
			_StatusProjectContractsText = value;
		}
        
    }

		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("PAYED"/*, NameResourceType=typeof(ProjectContractResources)*/)]
	public Boolean   Payed { get; set; }
	public string _PayedText = null;
    public string PayedText {
        get {
			if (string.IsNullOrEmpty( _PayedText ))
				{
			//Aplicar formato si esta especificado
				return Payed.ToString();
	
			}else{
				return _PayedText ;
			}			
        }
		set{
			_PayedText = value;
		}
        
    }

		
		
	
[Exportable()]
	
	    [Required()]
		
	[RelationFilterable()] 
	[LocalizedDisplayName("CANCELED"/*, NameResourceType=typeof(ProjectContractResources)*/)]
	public Boolean   Canceled { get; set; }
	public string _CanceledText = null;
    public string CanceledText {
        get {
			if (string.IsNullOrEmpty( _CanceledText ))
				{
			//Aplicar formato si esta especificado
				return Canceled.ToString();
	
			}else{
				return _CanceledText ;
			}			
        }
		set{
			_CanceledText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("CREATEDDATE"/*, NameResourceType=typeof(ProjectContractResources)*/)]
	public DateTime  ? CreatedDate { get; set; }
	public string CreatedDateText {
        get {
            if (CreatedDate != null)
				return ((DateTime)CreatedDate).ToShortDateString() ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.CreatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DateTime(true, false, null)]	
	[LocalizedDisplayName("UPDATEDDATE"/*, NameResourceType=typeof(ProjectContractResources)*/)]
	public DateTime  ? UpdatedDate { get; set; }
	public string UpdatedDateText {
        get {
            if (UpdatedDate != null)
			
                return ((DateTime)UpdatedDate).ToString("s") ;
            else
                return String.Empty;
        }
				set{
					if (!string.IsNullOrEmpty(value))
						this.UpdatedDate = Convert.ToDateTime(value);
    }
		}
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "CreatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("CREATEDBY"/*, NameResourceType=typeof(ProjectContractResources)*/)]
	public Guid  ? CreatedBy { get; set; }
		
		
	
[Exportable()]
		
[RelationFilterable(FiltrablePropertyPathName = "UpdatedBy", IsExternal =true )]
[AutoComplete("SFSdotNetFrameworkSecurity", "secUsers", "FindUsers", "filter", "DisplayName", "GuidUser", true)]	

	[SystemProperty()]
	[LocalizedDisplayName("UPDATEDBY"/*, NameResourceType=typeof(ProjectContractResources)*/)]
	public Guid  ? UpdatedBy { get; set; }
		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[DataType("Integer")]
	[LocalizedDisplayName("BYTES"/*, NameResourceType=typeof(ProjectContractResources)*/)]
	public Int32  ? Bytes { get; set; }
	public string _BytesText = null;
    public string BytesText {
        get {
			if (string.IsNullOrEmpty( _BytesText ))
				{
	
            if (Bytes != null)
				return Bytes.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _BytesText ;
			}			
        }
		set{
			_BytesText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable()] 
	[SystemProperty()]
	[LocalizedDisplayName("ISDELETED"/*, NameResourceType=typeof(ProjectContractResources)*/)]
	public Boolean  ? IsDeleted { get; set; }
	public string _IsDeletedText = null;
    public string IsDeletedText {
        get {
			if (string.IsNullOrEmpty( _IsDeletedText ))
				{
	
            if (IsDeleted != null)

				return IsDeleted.ToString();
				
            else
                return String.Empty;
	
			}else{
				return _IsDeletedText ;
			}			
        }
		set{
			_IsDeletedText = value;
		}
        
    }

		
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.PageTypesController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Name", DataPropertyValue = "GuidPageType", FiltrablePropertyPathName="PageType.GuidPageType")]	

	[LocalizedDisplayName("PAGETYPE"/*, NameResourceType=typeof(ProjectContractResources)*/)]
	public Guid  ? FkPageType { get; set; }
		[LocalizedDisplayName("PAGETYPE"/*, NameResourceType=typeof(ProjectContractResources)*/)]
	[Exportable()]
	public string  FkPageTypeText { get; set; }
    public string FkPageTypeSafeKey { get; set; }

	
		
	
[Exportable()]
		
	[RelationFilterable(DataClassProvider = typeof(Controllers.ProjectsController), GetByKeyMethod="GetByKey", GetAllMethod = "GetAll", DataPropertyText = "Description", DataPropertyValue = "GuidProject", FiltrablePropertyPathName="Project.GuidProject")]	

	[LocalizedDisplayName("PROJECT"/*, NameResourceType=typeof(ProjectContractResources)*/)]
	public Guid  ? FkProject { get; set; }
		[LocalizedDisplayName("PROJECT"/*, NameResourceType=typeof(ProjectContractResources)*/)]
	[Exportable()]
	public string  FkProjectText { get; set; }
    public string FkProjectSafeKey { get; set; }

	
		
		
		[LocalizedDisplayName("INTERESTEDPAGES"/*, NameResourceType=typeof(ProjectContractResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="InterestedPages.Count()", ModelPartialType="InterestedPages.InterestedPage", BusinessObjectSetName = "InterestedPages")]
        public List<InterestedPages.InterestedPageModel> InterestedPages { get; set; }			
	
		[LocalizedDisplayName("PROJECTCONTRACTTERMS"/*, NameResourceType=typeof(ProjectContractResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="ProjectContractTerms.Count()", ModelPartialType="ProjectContractTerms.ProjectContractTerm", BusinessObjectSetName = "ProjectContractTerms")]
        public List<ProjectContractTerms.ProjectContractTermModel> ProjectContractTerms { get; set; }			
	
		[LocalizedDisplayName("PROJECTPROFILERATINGS"/*, NameResourceType=typeof(ProjectContractResources)*/)]
		[RelationFilterable(IsNavigationPropertyMany=true, FiltrablePropertyPathName="ProjectProfileRatings.Count()", ModelPartialType="ProjectProfileRatings.ProjectProfileRating", BusinessObjectSetName = "ProjectProfileRatings")]
        public List<ProjectProfileRatings.ProjectProfileRatingModel> ProjectProfileRatings { get; set; }			
	
	public override string SafeKey
   	{
		get
        {
			if(this.GuidProjectContract != null)
				return SFSdotNet.Framework.Entities.Utils.GetKey(this ,"GuidProjectContract").Replace("/","-");
			else
				return String.Empty;
		}
    }		
		public void Bind(ProjectContractModel model){
            
		this.GuidProjectContract = model.GuidProjectContract;
		this.GuidProject = model.GuidProject;
		this.Description = model.Description;
		this.GuidPageType = model.GuidPageType;
		this.Money = model.Money;
		this.StatusProjectContracts = model.StatusProjectContracts;
		this.Payed = model.Payed;
		this.Canceled = model.Canceled;
		this.CreatedDate = model.CreatedDate;
		this.UpdatedDate = model.UpdatedDate;
		this.CreatedBy = model.CreatedBy;
		this.UpdatedBy = model.UpdatedBy;
		this.Bytes = model.Bytes;
		this.IsDeleted = model.IsDeleted;
        }

        public BusinessObjects.ProjectContract GetBusinessObject()
        {
            BusinessObjects.ProjectContract result = new BusinessObjects.ProjectContract();


			       
	if (this.GuidProjectContract != null )
				result.GuidProjectContract = (Guid)this.GuidProjectContract;
				
	if (this.GuidProject != null )
				result.GuidProject = (Guid)this.GuidProject;
				
	if (this.Description != null )
				result.Description = (String)this.Description.Trim().Replace("\t", String.Empty);
				
	if (this.GuidPageType != null )
				result.GuidPageType = (Guid)this.GuidPageType;
				
	if (this.Money != null )
				result.Money = (Decimal)this.Money;
				
	if (this.StatusProjectContracts != null )
				result.StatusProjectContracts = (Int32)this.StatusProjectContracts;
				
	if (this.Payed != null )
				result.Payed = (Boolean)this.Payed;
				
	if (this.Canceled != null )
				result.Canceled = (Boolean)this.Canceled;
				
				if(this.CreatedDate != null)
					if (this.CreatedDate != null)
				result.CreatedDate = (DateTime)this.CreatedDate;		
				
				if(this.UpdatedDate != null)
					if (this.UpdatedDate != null)
				result.UpdatedDate = (DateTime)this.UpdatedDate;		
				
	if (this.CreatedBy != null )
				result.CreatedBy = (Guid)this.CreatedBy;
				
	if (this.UpdatedBy != null )
				result.UpdatedBy = (Guid)this.UpdatedBy;
				
	if (this.Bytes != null )
				result.Bytes = (Int32)this.Bytes;
				
	if (this.IsDeleted != null )
				result.IsDeleted = (Boolean)this.IsDeleted;
				
			result.PageType = new BusinessObjects.PageType() { GuidPageType= (Guid)this.FkPageType };
				
			result.Project = new BusinessObjects.Project() { GuidProject= (Guid)this.FkProject };
				

            return result;
        }
        public void Bind(BusinessObjects.ProjectContract businessObject)
        {
				this.BusinessObjectObject = businessObject;

			this.GuidProjectContract = businessObject.GuidProjectContract;
				
			this.GuidProject = businessObject.GuidProject;
				
			this.Description = businessObject.Description != null ? businessObject.Description.Trim().Replace("\t", String.Empty) : "";
				
			this.GuidPageType = businessObject.GuidPageType;
				
			this.Money = businessObject.Money;
				
			this.StatusProjectContracts = businessObject.StatusProjectContracts;
				
			this.Payed = businessObject.Payed;
				
			this.Canceled = businessObject.Canceled;
				
				if (businessObject.CreatedDate != null )
				this.CreatedDate = (DateTime)businessObject.CreatedDate;
				if (businessObject.UpdatedDate != null )
				this.UpdatedDate = (DateTime)businessObject.UpdatedDate;
				
	if (businessObject.CreatedBy != null )
				this.CreatedBy = (Guid)businessObject.CreatedBy;
				
	if (businessObject.UpdatedBy != null )
				this.UpdatedBy = (Guid)businessObject.UpdatedBy;
				
	if (businessObject.Bytes != null )
				this.Bytes = (Int32)businessObject.Bytes;
				
	if (businessObject.IsDeleted != null )
				this.IsDeleted = (Boolean)businessObject.IsDeleted;
	        if (businessObject.PageType != null){
	                	this.FkPageTypeText = businessObject.PageType.Name != null ? businessObject.PageType.Name.ToString() : "";; 
										
										
				this.FkPageType = businessObject.PageType.GuidPageType;
                this.FkPageTypeSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.PageType,"GuidPageType").Replace("/","-");

			}
	        if (businessObject.Project != null){
	                	this.FkProjectText = businessObject.Project.Description != null ? businessObject.Project.Description.ToString() : "";; 
										
										
				this.FkProject = businessObject.Project.GuidProject;
                this.FkProjectSafeKey  = SFSdotNet.Framework.Entities.Utils.GetKey(businessObject.Project,"GuidProject").Replace("/","-");

			}
           
        }
	}
}
