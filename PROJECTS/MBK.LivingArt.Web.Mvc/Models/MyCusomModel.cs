﻿using SFSdotNet.Framework.Web.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MBK.LivingArt.Web.Mvc.Models
{
    public class MyCustomModel:ModelBase
    {
        //public string SafeKey { get; set; }
        public string Name { get; set; }

        public DateTime  Fecha { get; set; }
        public Decimal Dinero { get; set; }
        public Boolean SiONo { get; set; }

    }
}