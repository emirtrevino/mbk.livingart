﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MBK.LivingArt.Web.Mvc.Models.ProfileEvents;
using SFSdotNet.Framework.Web.Mvc;
using SFSdotNet.Framework.Web.Mvc.Models;
using MBK.LivingArt.BusinessObjects;
using SFSdotNet.Framework.Entities;
using SFSdotNet.Framework.My;

namespace MBK.LivingArt.Web.Mvc.Controllers
{


    public partial class ProfileEventsController
    {
        public ActionResult MyCustomMethodA(ApiWrapper<ProfileEventModel> wrap, ContextRequest contextRequest)
        {

            BR.ProfileEventsBR.Instance.MyCustomMethodA(wrap.Item.GetBusinessObject(), contextRequest);

            return Content("ok");
        }
        partial void OnShowing(object sender, MyEventArgs<UIModel<ProfileEventModel>> e)
        {
            if (this.IsEditOrDetailsForm(e.UIModel))
            {

                e.UIModel.SetType(SystemProperties.GlobalUser, ProfileEvent.PropertyNames.UserProfile);
            }
        }

    }
}