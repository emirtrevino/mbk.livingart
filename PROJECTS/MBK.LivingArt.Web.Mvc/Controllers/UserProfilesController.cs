﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MBK.LivingArt.Web.Mvc.Models.UserProfiles;
using SFSdotNet.Framework.Web.Mvc;
using SFSdotNet.Framework.Web.Mvc.Models;
using MBK.LivingArt.BusinessObjects;
using MBK.LivingArt.BR;
using SFSdotNet.Framework.Common.Entities;
using MBK.LivingArt.Web.Mvc.Models;
using SFSdotNet.Framework.Web.Mvc.Security;
using static SFSdotNet.Framework.Globals.Security;

namespace MBK.LivingArt.Web.Mvc.Controllers
{
    public partial class UserProfilesController
    {
        //partial void OnAuthorization(object sender, AuthorizationContext context)
        //{
        //   if (context.HttpContext.Request.IsAuthenticated == true)
        //    {
        //        context.HttpContext.SkipAuthorization = true;
        //    }
        //}

        public ActionResult MyCustomForm()
        {
            MyCustomModel model = new Models.MyCustomModel();
            model.Name = "Prueba";
            UIModel uiModel = new UIModel();
            //uiModel.CleanLayout = true;
          //  uiModel.JsFiles.Add(VirtualPathUtility.ToAbsolute("~/MBKLivingArt/Content/js/JavaScript.js"));
            uiModel.UILayoutFile = "";
            ViewData["uiModel"] = uiModel;

            return View("MyCustomForm", model);
        }
        [HttpPost()]
        public ActionResult Guardar(MyCustomModel model)
        {


            return ResolveResponse("Ok blala", SFSdotNet.Framework.My.MessageResultTypes.Ok);
        }
        [MyAuthorize()]
        public ActionResult MyCustomForm2()
        {
            MyCustomModel model = new Models.MyCustomModel();
            model.Name = "Prueba";

             
            model.Name = SFSdotNet.Framework.My.Context.CurrentContext.User.UserName;
            UIModel uiModel = this.GetUIModel(model);
            uiModel.ActionName = "Guardar";
            uiModel.IsMultiColumn = true;
            uiModel.UIVersion = 2;
            uiModel.NewUILayoutTool = true;
            //uiModel.CleanLayout = true;
            //  uiModel.JsFiles.Add(VirtualPathUtility.ToAbsolute("~/MBKLivingArt/Content/js/JavaScript.js"));
            //uiModel.UILayoutFile = "";
            //ViewData["uiModel"] = uiModel;

            return ResolveView(uiModel, model);
            //return View("MyCustomForm", model);
        }

        partial void OnCustomActionExecuting(object sender, MyEventArgs<ContextActionModel<UserProfileModel>> e)
        {
            //if (e.UIModel.ActionKey == "activate")
            //{
            //    UserProfilesBR.Instance.ActivateUsers(e.UIModel.Query, e.UIModel.SelectedKeys.Cast<Guid>().ToArray(), e.UIModel.ContextRequest);
            //}
        }

        partial void OnCustomActionExecutingBackground(object sender, MyEventArgs<ContextActionModel<UserProfileModel>> e)
        {
            if (e.UIModel.ActionKey == "activate")
            {
                UserProfilesBR.Instance.ActivateUsers(e.UIModel.Query, e.UIModel.SelectedKeys.Cast<Guid>().ToArray(), e.UIModel.ContextRequest);
            }
        }
        partial void OnActionsCreated(object sender, MyEventArgs<UIModel<UserProfileModel>> e)
        {

            var permission = new SFSdotNet.Framework.Security.Permissions();
           
            if (permission.IsAllowed(SFSdotNet.Framework.My.Context.CurrentContext.User, e.UIModel.ModuleKey, "UserProfile","activate")) {

                ActionModel action = new ActionModel();
                action.ActionKey = "activate";
                action.Title = "Activar";
                action.IsBackground = true;

                e.UIModel.Actions.Add(action);
            }
        }
        public UserProfilesController()
        {
            uiText = new SFSdotNet.Framework.Globalization.TextUI("MBKLivingArt", "UserProfile", GetContextRequest());

        }
        SFSdotNet.Framework.Globalization.TextUI uiText;

        
        partial void OnShowing(object sender, MyEventArgs<UIModel<UserProfileModel>> e)
        {

           // e.UIModel.
            //if (this.IsItemsOrListForm(e.UIModel))
            //{

            //}
            
            if (e.UIModel.ContextType == UIModelContextTypes.ListForm || e.UIModel.ContextType == UIModelContextTypes.Items)
            {
                // columnas queremos ver en la lista y el orden
                e.UIModel.HeaderPartialView = "ListScripts";

                e.UIModel.SetOrder(UserProfile.PropertyNames.Name, true);
                e.UIModel.SetOrder(UserProfile.PropertyNames.Birthdate);
                

                e.UIModel.SetHide(UserProfile.PropertyNames.ProfileStatus , true);
                if (SFSdotNet.Framework.My.Context.CurrentContext.IsUserInRole(
                            SFSdotNet.Framework.My.Context.CurrentContext.User,
                            SFSdotNet.Framework.My.Context.CurrentContext.Company,
                            "rol-prueba", GetContextRequest()))
                {
                    e.UIModel.SetOrder(UserProfile.PropertyNames.Alias);
                    


                }

                e.UIModel.SetOrder(UserProfile.PropertyNames.CountMusicPreferencesComputed);

            }
            else  if (this.IsEditOrDetailsForm(e.UIModel))
            {
                // columnas y orden del formulario
                e.UIModel.HeaderPartialView = "EditScripts";
               
                //e.UIModel.Properties.FirstOrDefault().RegularExpression = "";

                e.UIModel.SetOrder(UserProfile.PropertyNames.Name, HorizontalFieldSizes.H_50_Percent);
                e.UIModel.SetOrder(UserProfile.PropertyNames.Alias, HorizontalFieldSizes.H_50_Percent);

                e.UIModel.SetOrder(UserProfile.PropertyNames.Biography);

                var propertyBiography = e.UIModel.Properties.FirstOrDefault(p=> p.PropertyName == UserProfile.PropertyNames.Biography);
                propertyBiography.IsMultiline = true;
                NameValue<string> nv = new NameValue<String>();
                propertyBiography.PropertyDisplayName = uiText.GetText("Biografia-custom", nv.Add("es", "Esta es la biografía"), nv.Add("en", "This is the bio") ); //Resources.LocalResources.DIAGNOSTIC;

                //propertyBiography.RegularExpression = "";

                List<SelectListItem> items = new List<SelectListItem>();
                items.Add(new SelectListItem() { Value ="1", Text = "Artista"  });
                items.Add(new SelectListItem() { Value = "2", Text = "Interesado" });

                SelectList list = new SelectList(items, "Value", "Text");

                e.UIModel.SetNavigation(UserProfile.PropertyNames.ProfileType, list);
                e.UIModel.AddEmptyField(HorizontalFieldSizes.H_100_Percent, "MiEspacioVacio");
               //GetPropertyDefinition("ssdadaa", )
                e.UIModel.SetOrder(UserProfile.PropertyNames.Birthdate, HorizontalFieldSizes.H_33_Percent);
                e.UIModel.SetOrder(UserProfile.PropertyNames.ProfileType, HorizontalFieldSizes.H_33_Percent);
                e.UIModel.SetOrder(UserProfile.PropertyNames.ProfileStatus, HorizontalFieldSizes.H_33_Percent);

                if (e.UIModel.ContextType == UIModelContextTypes.DisplayForm)
                {
                    e.UIModel.SetOrder(UserProfile.PropertyNames.ProfileMusicPreferences);
                }
                // validaciones

                if (true)
                {
                    e.UIModel.SetRequired(UserProfile.PropertyNames.Alias);
                }
            }
        }
    }
}