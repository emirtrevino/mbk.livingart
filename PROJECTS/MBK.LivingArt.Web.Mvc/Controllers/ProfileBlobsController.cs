﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MBK.LivingArt.Web.Mvc.Models.ProfileBlobs;
using SFSdotNet.Framework.Web.Mvc;
using SFSdotNet.Framework.Web.Mvc.Models;
using MBK.LivingArt.BusinessObjects;

namespace MBK.LivingArt.Web.Mvc.Controllers
{
    public partial class ProfileBlobsController
    {
        partial void OnShowing(object sender, MyEventArgs<UIModel<ProfileBlobModel>> e)
        {
            if (this.IsItemsOrListForm(e.UIModel))
            {
                e.UIModel.SetOrder(ProfileBlob.PropertyNames.LAFile);

                e.UIModel.SetHide(ProfileBlob.PropertyNames.GuidFile, true);
                e.UIModel.SetHide(ProfileBlob.PropertyNames.ExistFile, true);
                //e.UIModel.SetHide(ProfileBlob.PropertyNames., true);

            }
        }
    }
}