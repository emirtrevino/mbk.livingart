﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcContrib.PortableAreas;
using System.Web.Mvc;
using System.Configuration;
using System.Web.Routing;
using SFSdotNet.Framework.My;
using SFSdotNet.Framework.Web.Mvc;

namespace MBK.LivingArt.Web.Mvc
{
    public partial class MBKLivingArtWebMvcRegistration : PortableAreaRegistration, IRegistrationModuleWithInfo
    {

        public override void RegisterArea(System.Web.Mvc.AreaRegistrationContext context, IApplicationBus bus)
        {
            #region MBKLivingArt/Views/Shared/
            context.MapRoute("MBKLivingArt_Views_Shared", "MBKLivingArt/Views/Shared/{resourceName}",
                new { controller = "EmbeddedResource", action = "Index", resourcePath = "Views/Shared" },
                new string[] { "MvcContrib.PortableAreas" });
            #endregion

            #region MBKLivingArt/Content/Themes/Default/
            context.MapRoute("MBKLivingArt_ResourceRoute_theme", "MBKLivingArt/Content/Themes/Default/{resourceName}",
                new { controller = "EmbeddedResource", action = "Index", resourcePath = "Content/Themes/Default" },
                new string[] { "MvcContrib.PortableAreas" });
            #endregion
            #region MBKLivingArt/Content/Themes/Default/css/
            context.MapRoute("MBKLivingArt_ResourceRoute_theme_css", "MBKLivingArt/Content/Themes/Default/css/{resourceName}",
    new { controller = "EmbeddedResource", action = "Index", resourcePath = "Content/Themes/Default/css" },
    new string[] { "MvcContrib.PortableAreas" });
            #endregion



           
            context.MapRoute("MBKLivingArt_ResourceRoute_js", "MBKLivingArt/Content/js/{resourceName}",
    new { controller = "EmbeddedResource", action = "Index", resourcePath = "Content/js" },
    new string[] { "MvcContrib.PortableAreas" });
     


            #region MBKLivingArt/Content/Themes/Default/img/
            context.MapRoute("MBKLivingArt_ResourceRoute_theme_img", "MBKLivingArt/Content/Themes/Default/img/{resourceName}",
    new { controller = "EmbeddedResource", action = "Index", resourcePath = "Content/Themes/Default/img" },
    new string[] { "MvcContrib.PortableAreas" });
            #endregion
            #region MBKLivingArt/Content/img/
            context.MapRoute("MBKLivingArt_ResourceRoute_img", "MBKLivingArt/Content/img/{resourceName}",
new { controller = "EmbeddedResource", action = "Index", resourcePath = "Content/img" },
new string[] { "MvcContrib.PortableAreas" });
            #endregion

            context.MapRoute("MBKLivingArt_ResourceImageRoute", "MBKLivingArt/images/{resourceName}",
                new { controller = "EmbeddedResource", action = "Index", resourcePath = "images" },
                new string[] { "MvcContrib.PortableAreas" });

            context.MapRoute("MBKLivingArt_Default", 
                "MBKLivingArt/{controller}/{action}",
                new { controller = "Home", 
                    action = "index" },
                new string[] { "MBK.LivingArt.Web.Mvc.Controllers" 
                });

            context.MapRoute(
              "MBKLivingArt_Id", // Route name
              "MBKLivingArt/{controller}/{action}/{id}", // URL with parameters
              new { controller = "Home", 
                  action = "Index", 
                  id = UrlParameter.Optional }, 
                  new[] { "MBK.LivingArt.Web.Mvc.Controllers" 
                  });

            //context.MapRoute(
            // "MBKLivingArt_usemode_Id", // Route name
            // "MBKLivingArt/{controller}/usemode/{usemode}/{action}/{id}", // URL with parameters
            // new
            // {
            //     controller = "Home",
            //     action = "Index",
            //     id = UrlParameter.Optional
            // },
            //     new[] { "MBK.LivingArt.Web.Mvc.Controllers" 
            //      });
          

            
        
            //ControllerBuilder.Current.DefaultNamespaces.Add("MBK.LivingArt.Web.Mvc.Controllers");

            this.RegisterAreaEmbeddedResources();
            if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["AutoInjectPermissionsOnStartup"]))
                SecuritySettings.PermissionsInitialization();

            OnAreaRegistration(this, new EventArgs());
        }
        public  SFSdotNet.Framework.My.BusinessModuleApp GetModuleInfo()
        {
            SFSdotNet.Framework.My.BusinessModuleApp result = new SFSdotNet.Framework.My.BusinessModuleApp();
            result.BusinessModulePath = "MBKLivingArt";
            //result.DefaultOwnLayout = VirtualPathUtility.ToAbsolute("~/") + "Areas/" + result.BusinessModulePath + "/Views/Shared/_Layout.cshtml";
            result.UseOwnLayout = false;
            OnBusinessAppInfoRequest(this, result );
            return result;
        }
        partial void OnBusinessAppInfoRequest(object sender, BusinessModuleApp e);
        partial void OnAreaRegistration(object sender, EventArgs e);

        public override string AreaName
        {
            get
            {
                return "MBKLivingArt";
            }
        }
    }
}
