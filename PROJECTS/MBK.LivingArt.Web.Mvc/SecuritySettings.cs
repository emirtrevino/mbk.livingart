
 
 
// <Template>
//   <SolutionTemplate></SolutionTemplate>
//   <Version>20140213.2136</Version>
//   <Update>Agregado el objeto ContextRequest en todas las peticiones de reglas de negocio</Update>
// </Template>using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using System.Collections.Generic;
using SFSdotNet.Framework.Security.BusinessObjects;
using SFSdotNet.Framework.My;


namespace MBK.LivingArt.Web.Mvc
{
    public partial class SecuritySettings
    {
		static partial void OnCreatingRolesAndUsers(object sender, secModule module);
		static partial void OnIntegrationSetting(object sender, SFSdotNet.Framework.Security.BusinessObjects.secModule module, ContextRequest contextRequest);
        
        public static void PermissionsInitialization() {
			ContextRequest contextRequest = new ContextRequest();
            string moduleKey = "MBKLivingArt";

			var module = SFSdotNet.Framework.Security.SiteMapBuilder.AddModuleIfNotExist(moduleKey, moduleKey, "MBK.LivingArt", null);

            #region permissions
            secPermission createPermission = SFSdotNet.Framework.Security.Permissions.AddPermissionIfNotExist("c", "Create", module, contextRequest);
            secPermission readPermission = SFSdotNet.Framework.Security.Permissions.AddPermissionIfNotExist("r", "Read", module, contextRequest);
            secPermission updatePermission = SFSdotNet.Framework.Security.Permissions.AddPermissionIfNotExist("u", "Update", module, contextRequest);
            secPermission deletePermission = SFSdotNet.Framework.Security.Permissions.AddPermissionIfNotExist("d", "Delete", module, contextRequest);
			
			OnCreatingRolesAndUsers(null, module);
			 OnIntegrationSetting(null, module, contextRequest);
		
			//if (SFSdotNet.Framework.Security.BR.secRolesBR.Instance.GetBy(p => p.LoweredRoleName == "superadmin").Count == 0)
            //    SFSdotNet.Framework.Security.BR.secRolesBR.Instance.Create(new secRole() { LoweredRoleName="superadmin", RoleName="superadmin" });


			 if (SFSdotNet.Framework.Security.BR.secRolesBR.Instance.GetBy(p => p.LoweredRoleName == "mbklivingart admin", contextRequest).Count == 0)
                SFSdotNet.Framework.Security.BR.secRolesBR.Instance.Create(new secRole() { LoweredRoleName="mbklivingart admin", RoleName="MBKLivingArt Admin" }, contextRequest);
            
            
			AddPermissionIfNotExist(module, null, readPermission);
            #endregion
			
			secBusinessObject bo = null;
			secModuleObjectPermission mop =  null;
            secRoleModuleObjectPermission rmop = null;
			#region BusinessObjects
			
            #region CommunicationProject
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "CommunicationProject" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="CommunicationProject",EntitySetName="CommunicationProjects", Name="CommunicationProject", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "CommunicationProjects";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region MusicPreference
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "MusicPreference" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="MusicPreference",EntitySetName="MusicPreferences", Name="MusicPreference", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "MusicPreferences";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region PageType
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "PageType" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="PageType",EntitySetName="PageTypes", Name="PageType", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "PageTypes";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region ProfileBlob
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "ProfileBlob" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="ProfileBlob",EntitySetName="ProfileBlobs", Name="ProfileBlob", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "ProfileBlobs";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region ProfileMusicPreference
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "ProfileMusicPreference" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="ProfileMusicPreference",EntitySetName="ProfileMusicPreferences", Name="ProfileMusicPreference", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "ProfileMusicPreferences";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region ProfilePageBlob
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "ProfilePageBlob" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="ProfilePageBlob",EntitySetName="ProfilePageBlobs", Name="ProfilePageBlob", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "ProfilePageBlobs";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region ProfilePageStore
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "ProfilePageStore" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="ProfilePageStore",EntitySetName="ProfilePageStores", Name="ProfilePageStore", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "ProfilePageStores";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region ProfilePageTermContract
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "ProfilePageTermContract" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="ProfilePageTermContract",EntitySetName="ProfilePageTermContracts", Name="ProfilePageTermContract", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "ProfilePageTermContracts";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region Project
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "Project" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="Project",EntitySetName="Projects", Name="Project", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "Projects";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region ProjectIncident
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "ProjectIncident" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="ProjectIncident",EntitySetName="ProjectIncidents", Name="ProjectIncident", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "ProjectIncidents";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region ProjectProfileRating
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "ProjectProfileRating" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="ProjectProfileRating",EntitySetName="ProjectProfileRatings", Name="ProjectProfileRating", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "ProjectProfileRatings";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region ProjectsQuestion
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "ProjectsQuestion" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="ProjectsQuestion",EntitySetName="ProjectsQuestions", Name="ProjectsQuestion", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "ProjectsQuestions";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region ProfilePage
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "ProfilePage" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="ProfilePage",EntitySetName="ProfilePages", Name="ProfilePage", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "ProfilePages";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region ProfileEvent
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "ProfileEvent" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="ProfileEvent",EntitySetName="ProfileEvents", Name="ProfileEvent", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "ProfileEvents";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region ProjectIncidentDetail
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "ProjectIncidentDetail" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="ProjectIncidentDetail",EntitySetName="ProjectIncidentDetails", Name="ProjectIncidentDetail", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "ProjectIncidentDetails";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region ProjectContractTerm
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "ProjectContractTerm" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="ProjectContractTerm",EntitySetName="ProjectContractTerms", Name="ProjectContractTerm", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "ProjectContractTerms";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region LAFile
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "LAFile" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="LAFile",EntitySetName="LAFiles", Name="LAFile", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "LAFiles";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region UserProfile
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "UserProfile" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="UserProfile",EntitySetName="UserProfiles", Name="UserProfile", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "UserProfiles";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region InterestedPage
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "InterestedPage" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="InterestedPage",EntitySetName="InterestedPages", Name="InterestedPage", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "InterestedPages";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #region ProjectContract
            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.GetBy(p=>p.BusinessObjectKey == "ProjectContract" && p.secModule.ModuleKey == module.ModuleKey , contextRequest).FirstOrDefault();
            if (bo == null) {
                            bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Create(new secBusinessObject() { BusinessObjectKey="ProjectContract",EntitySetName="ProjectContracts", Name="ProjectContract", secModule = module, GuidBusinessObject= Guid.NewGuid()  }, contextRequest);
            }else{
				bo.EntitySetName = "ProjectContracts";
				bo = SFSdotNet.Framework.Security.BR.secBusinessObjectsBR.Instance.Update(bo, contextRequest);
			}
            AddPermissionIfNotExist(module, bo, createPermission);
            AddPermissionIfNotExist(module, bo, readPermission);
            AddPermissionIfNotExist(module, bo, updatePermission);
            AddPermissionIfNotExist(module, bo, deletePermission);
            #endregion

            #endregion
        }
		
		public static void AddPermissionIfNotExist(secModule module, secBusinessObject businessObject, secPermission permission) {
              // se asume que el permiso ya existe
            // se asume que el m?dulo ya existe
             ContextRequest contextRequest = new ContextRequest();
            if (businessObject == null)
            {
                // m?dulo y permiso ya existen
                secPermission mp = SFSdotNet.Framework.Security.BR.secPermissionsBR.Instance.GetBy(
                p => p.PermissionKey == permission.PermissionKey
                && p.secModules.Any(x=>x.ModuleKey == module.ModuleKey)
                , contextRequest).FirstOrDefault();
                if (mp == null) {

                    SFSdotNet.Framework.Security.BR.secPermissionsBR.Instance.AddRelation(permission, module);
                }

                secRoleModulePermission rmp = SFSdotNet.Framework.Security.BR.secRoleModulePermissionsBR.Instance.GetBy(
                    p => p.secRole.LoweredRoleName == "superadmin"
                    && p.secModule.ModuleKey == module.ModuleKey
                
                    && p.secPermission.PermissionKey == permission.PermissionKey, contextRequest).FirstOrDefault();
                if (rmp == null) {
                    rmp = new secRoleModulePermission();
					rmp.secRole = SFSdotNet.Framework.Security.BR.secRolesBR.Instance.GetBy(p => p.LoweredRoleName == "superadmin", contextRequest).FirstOrDefault();
                    rmp.secModule = module;
                    rmp.secPermission = permission;
					rmp.IsAllowed = true;
                    SFSdotNet.Framework.Security.BR.secRoleModulePermissionsBR.Instance.Create(rmp, contextRequest);
                }
				rmp = SFSdotNet.Framework.Security.BR.secRoleModulePermissionsBR.Instance.GetBy(
                    p => p.secRole.LoweredRoleName == "mbklivingart admin"
                    && p.secModule.ModuleKey == module.ModuleKey
                
                    && p.secPermission.PermissionKey == permission.PermissionKey, contextRequest).FirstOrDefault();
                if (rmp == null) {
                    rmp = new secRoleModulePermission();
					rmp.secRole = SFSdotNet.Framework.Security.BR.secRolesBR.Instance.GetBy(p => p.LoweredRoleName == "mbklivingart admin", contextRequest).FirstOrDefault();
                    rmp.secModule = module;
                    rmp.secPermission = permission;
					rmp.IsAllowed = true;
                    SFSdotNet.Framework.Security.BR.secRoleModulePermissionsBR.Instance.Create(rmp, contextRequest);
                }
            }
            else { 
				secModuleObjectPermission mop = SFSdotNet.Framework.Security.BR.secModuleObjectPermissionsBR.Instance.GetBy(
					p => p.secPermission.PermissionKey == permission.PermissionKey
					&& p.secBusinessObject.BusinessObjectKey == businessObject.BusinessObjectKey
					&& p.secModule.ModuleKey == module.ModuleKey
					, contextRequest).FirstOrDefault();
				if (mop == null)
				{
					mop = new secModuleObjectPermission();
					mop.secModule = module;
					mop.secBusinessObject = businessObject;
					mop.secPermission = permission;

					mop = SFSdotNet.Framework.Security.BR.secModuleObjectPermissionsBR.Instance.Create(mop, contextRequest);
				}

				secRoleModuleObjectPermission rmop = SFSdotNet.Framework.Security.BR.secRoleModuleObjectPermissionsBR.Instance.GetBy(
					p => p.secRole.LoweredRoleName == "superadmin"
					&& p.secModule.ModuleKey == module.ModuleKey 
					&& p.secBusinessObject.BusinessObjectKey == businessObject.BusinessObjectKey 
					&& p.secPermission.PermissionKey == permission.PermissionKey, contextRequest).FirstOrDefault();
				if (rmop == null)
				{
					rmop = new secRoleModuleObjectPermission();
					rmop.secBusinessObject = businessObject ;
					rmop.secModule = module;
					rmop.secRole = SFSdotNet.Framework.Security.BR.secRolesBR.Instance.GetBy(p => p.LoweredRoleName == "superadmin", contextRequest).FirstOrDefault();
					rmop.secPermission = permission ;
					rmop.IsAllowed = true;

					SFSdotNet.Framework.Security.BR.secRoleModuleObjectPermissionsBR.Instance.Create(rmop, contextRequest);

				}
				
				rmop = SFSdotNet.Framework.Security.BR.secRoleModuleObjectPermissionsBR.Instance.GetBy(
					p => p.secRole.LoweredRoleName == "mbklivingart admin"
					&& p.secModule.ModuleKey == module.ModuleKey 
					&& p.secBusinessObject.BusinessObjectKey == businessObject.BusinessObjectKey 
					&& p.secPermission.PermissionKey == permission.PermissionKey, contextRequest).FirstOrDefault();
				if (rmop == null)
				{
					rmop = new secRoleModuleObjectPermission();
					rmop.secBusinessObject = businessObject ;
					rmop.secModule = module;
					rmop.secRole = SFSdotNet.Framework.Security.BR.secRolesBR.Instance.GetBy(p => p.LoweredRoleName == "mbklivingart admin", contextRequest).FirstOrDefault();
					rmop.secPermission = permission ;
					rmop.IsAllowed = true;

					SFSdotNet.Framework.Security.BR.secRoleModuleObjectPermissionsBR.Instance.Create(rmop, contextRequest);

				}
			}

        }

    }
}
