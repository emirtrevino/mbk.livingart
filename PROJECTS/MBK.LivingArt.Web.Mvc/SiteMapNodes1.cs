
 
 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcSiteMapProvider;
using MBK.LivingArt.Web.Mvc.Resources;
using SFSdotNet.Framework.Web.Mvc.Resources;


namespace MBK.LivingArt.Web.Mvc
{
    public partial class DynamicNodeProvider : DynamicNodeProviderBase
    {
        partial void OnCreatingNodes(object sender, ref List<DynamicNode> nodes);
        partial void OnCreatedNodes(object sender, ref List<DynamicNode> nodes);
   
       public override IEnumerable<DynamicNode> GetDynamicNodeCollection(ISiteMapNode startNode)
        {
            List<DynamicNode> nodes = new List<DynamicNode>();
            DynamicNode node = null;
             SFSdotNet.Framework.Globalization.TextUI textUI = new SFSdotNet.Framework.Globalization.TextUI("MBKLivingArt", null);

			node = new DynamicNode();
            node.Title = ModuleResources.MODULE_NAME;
			
            node.Controller = "Navigation";
            node.Area = "";
            node.Action = "Index";
            node.Key = "MBKLivingArt";
			node.RouteValues.Add("id", node.Key);
			node.RouteValues.Add("overrideModule", "MBKLivingArt");

 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("permissionKey", "r");    
 
 
			textUI.SetTextTo(node, "Title", typeof(ModuleResources), "MODULE_NAME");
                   
            nodes.Add(node);

			
            node = new DynamicNode();
            node.Title = ModuleResources.CATALOGS;
			  
            node.Controller = "Navigation";
            node.Area = "";
            node.Action = "Index";
            node.Key = "MBKLivingArt_Catalogs";
			node.RouteValues.Add("id", node.Key);
			node.RouteValues.Add("overrideModule", "MBKLivingArt");

			node.Attributes.Add("moduleKey", "MBKLivingArt");
			node.ParentKey = "MBKLivingArt";
			 textUI.SetTextTo(node, "Title", typeof(ModuleResources), "CATALOGS");
         
            nodes.Add(node);
			
			
          /*  node = new DynamicNode();
            node.Title = ModuleResources.all_catalogs;
			 
            node.Controller = "Navigation";
            node.Area = "";
            node.Action = "Index";
            node.Key = "MBKLivingArt_all_catalogs";
			node.RouteValues.Add("id", node.Key);
			node.Attributes.Add("moduleKey", "MBKLivingArt");
			node.RouteValues.Add("overrideModule", "MBKLivingArt");

			node.ParentKey = "MBKLivingArt_Catalogs";
			 textUI.SetTextTo(node, "Title", typeof(ModuleResources), "all_catalogs");
          
            nodes.Add(node);*/


            node = new DynamicNode();
            //node.Title = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.USERS_AND_ROLES ;
            node.Controller = "secBusinessObjects";
            node.Area = "SFSdotNetFrameworkSecurity";
            node.Action = "Index";
            node.Key = "MBKLivingArt_all_catalogs";
            node.ParentKey = "MBKLivingArt_Catalogs";
            node.Attributes.Add("moduleKey", "SFSdotNetFrameworkSecurity");
            node.RouteValues.Add("overrideModule", "MBKLivingArt");
            node.RouteValues.Add("usemode", "all-catalogs");

            //node.Attributes.Add("permissionKey", "admin");
            textUI.SetTextTo(node, "Title", typeof(SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages), "ALL_CATALOGS");

            nodes.Add(node);

            
			//node for contents
			node = new DynamicNode();
            node.Title = ModuleResources.MODULE_NAME;
		  
            node.Controller = "Home";
            node.Area = "";
            node.Action = "App";
            node.Key = "MBKLivingArt_home_app_c";
            node.RouteValues.Add("id", "MBKLivingArt");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
			node.RouteValues.Add("overrideModule", "MBKLivingArt");

            node.ParentKey = "allapps";
				 textUI.SetTextTo(node, "Title", typeof(ModuleResources), "MODULE_NAME");
         
            nodes.Add(node);

			
		    OnCreatingNodes(this, ref nodes);

           List<SFSdotNet.Framework.Globalization.UITexts> entityTexts =   textUI.GetItems("en");
           string single = "";
           string plural = "";
           SFSdotNet.Framework.Globalization.UITexts entityText = null ;
	

			#region CommunicationProject
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "CommunicationProject");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "CommunicationProjects";
		
		       node.Controller = "CommunicationProjects";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_CommunicationProject_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "CommunicationProject");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "CommunicationProjects";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_CommunicationProject_Create";
			node.ParentKey = "MBKLivingArt_CommunicationProject_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "CommunicationProject");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = CommunicationProjectResources.COMMUNICATIONPROJECTS_EDIT;
            //node.Controller = "CommunicationProjects";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_CommunicationProject_Edit";
			//node.ParentKey = "MBKLivingArt_CommunicationProject_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "CommunicationProject";
            node.Controller = "CommunicationProjects";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_CommunicationProject_Details";
			node.ParentKey = "MBKLivingArt_CommunicationProject_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "CommunicationProject");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region MusicPreference
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "MusicPreference");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "MusicPreferences";
		
		       node.Controller = "MusicPreferences";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_MusicPreference_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "MusicPreference");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "MusicPreferences";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_MusicPreference_Create";
			node.ParentKey = "MBKLivingArt_MusicPreference_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "MusicPreference");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = MusicPreferenceResources.MUSICPREFERENCES_EDIT;
            //node.Controller = "MusicPreferences";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_MusicPreference_Edit";
			//node.ParentKey = "MBKLivingArt_MusicPreference_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "MusicPreference";
            node.Controller = "MusicPreferences";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_MusicPreference_Details";
			node.ParentKey = "MBKLivingArt_MusicPreference_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "MusicPreference");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region PageType
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "PageType");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "PageTypes";
		
		       node.Controller = "PageTypes";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_PageType_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "PageType");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "PageTypes";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_PageType_Create";
			node.ParentKey = "MBKLivingArt_PageType_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "PageType");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = PageTypeResources.PAGETYPES_EDIT;
            //node.Controller = "PageTypes";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_PageType_Edit";
			//node.ParentKey = "MBKLivingArt_PageType_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "PageType";
            node.Controller = "PageTypes";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_PageType_Details";
			node.ParentKey = "MBKLivingArt_PageType_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "PageType");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region ProfileBlob
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "ProfileBlob");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "ProfileBlobs";
		
		       node.Controller = "ProfileBlobs";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_ProfileBlob_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfileBlob");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "ProfileBlobs";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_ProfileBlob_Create";
			node.ParentKey = "MBKLivingArt_ProfileBlob_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfileBlob");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = ProfileBlobResources.PROFILEBLOBS_EDIT;
            //node.Controller = "ProfileBlobs";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_ProfileBlob_Edit";
			//node.ParentKey = "MBKLivingArt_ProfileBlob_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "ProfileBlob";
            node.Controller = "ProfileBlobs";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_ProfileBlob_Details";
			node.ParentKey = "MBKLivingArt_ProfileBlob_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfileBlob");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region ProfileMusicPreference
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "ProfileMusicPreference");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "ProfileMusicPreferences";
		
		       node.Controller = "ProfileMusicPreferences";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_ProfileMusicPreference_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfileMusicPreference");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "ProfileMusicPreferences";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_ProfileMusicPreference_Create";
			node.ParentKey = "MBKLivingArt_ProfileMusicPreference_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfileMusicPreference");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = ProfileMusicPreferenceResources.PROFILEMUSICPREFERENCES_EDIT;
            //node.Controller = "ProfileMusicPreferences";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_ProfileMusicPreference_Edit";
			//node.ParentKey = "MBKLivingArt_ProfileMusicPreference_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "ProfileMusicPreference";
            node.Controller = "ProfileMusicPreferences";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_ProfileMusicPreference_Details";
			node.ParentKey = "MBKLivingArt_ProfileMusicPreference_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfileMusicPreference");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region ProfilePageBlob
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "ProfilePageBlob");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "ProfilePageBlobs";
		
		       node.Controller = "ProfilePageBlobs";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_ProfilePageBlob_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfilePageBlob");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "ProfilePageBlobs";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_ProfilePageBlob_Create";
			node.ParentKey = "MBKLivingArt_ProfilePageBlob_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfilePageBlob");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = ProfilePageBlobResources.PROFILEPAGEBLOBS_EDIT;
            //node.Controller = "ProfilePageBlobs";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_ProfilePageBlob_Edit";
			//node.ParentKey = "MBKLivingArt_ProfilePageBlob_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "ProfilePageBlob";
            node.Controller = "ProfilePageBlobs";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_ProfilePageBlob_Details";
			node.ParentKey = "MBKLivingArt_ProfilePageBlob_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfilePageBlob");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region ProfilePageStore
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "ProfilePageStore");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "ProfilePageStores";
		
		       node.Controller = "ProfilePageStores";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_ProfilePageStore_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfilePageStore");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "ProfilePageStores";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_ProfilePageStore_Create";
			node.ParentKey = "MBKLivingArt_ProfilePageStore_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfilePageStore");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = ProfilePageStoreResources.PROFILEPAGESTORES_EDIT;
            //node.Controller = "ProfilePageStores";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_ProfilePageStore_Edit";
			//node.ParentKey = "MBKLivingArt_ProfilePageStore_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "ProfilePageStore";
            node.Controller = "ProfilePageStores";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_ProfilePageStore_Details";
			node.ParentKey = "MBKLivingArt_ProfilePageStore_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfilePageStore");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region ProfilePageTermContract
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "ProfilePageTermContract");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "ProfilePageTermContracts";
		
		       node.Controller = "ProfilePageTermContracts";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_ProfilePageTermContract_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfilePageTermContract");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "ProfilePageTermContracts";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_ProfilePageTermContract_Create";
			node.ParentKey = "MBKLivingArt_ProfilePageTermContract_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfilePageTermContract");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = ProfilePageTermContractResources.PROFILEPAGETERMCONTRACTS_EDIT;
            //node.Controller = "ProfilePageTermContracts";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_ProfilePageTermContract_Edit";
			//node.ParentKey = "MBKLivingArt_ProfilePageTermContract_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "ProfilePageTermContract";
            node.Controller = "ProfilePageTermContracts";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_ProfilePageTermContract_Details";
			node.ParentKey = "MBKLivingArt_ProfilePageTermContract_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfilePageTermContract");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region Project
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "Project");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "Projects";
		
		       node.Controller = "Projects";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_Project_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "Project");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "Projects";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_Project_Create";
			node.ParentKey = "MBKLivingArt_Project_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "Project");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = ProjectResources.PROJECTS_EDIT;
            //node.Controller = "Projects";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_Project_Edit";
			//node.ParentKey = "MBKLivingArt_Project_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "Project";
            node.Controller = "Projects";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_Project_Details";
			node.ParentKey = "MBKLivingArt_Project_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "Project");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region ProjectIncident
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "ProjectIncident");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "ProjectIncidents";
		
		       node.Controller = "ProjectIncidents";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_ProjectIncident_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProjectIncident");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "ProjectIncidents";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_ProjectIncident_Create";
			node.ParentKey = "MBKLivingArt_ProjectIncident_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProjectIncident");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = ProjectIncidentResources.PROJECTINCIDENTS_EDIT;
            //node.Controller = "ProjectIncidents";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_ProjectIncident_Edit";
			//node.ParentKey = "MBKLivingArt_ProjectIncident_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "ProjectIncident";
            node.Controller = "ProjectIncidents";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_ProjectIncident_Details";
			node.ParentKey = "MBKLivingArt_ProjectIncident_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProjectIncident");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region ProjectProfileRating
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "ProjectProfileRating");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "ProjectProfileRatings";
		
		       node.Controller = "ProjectProfileRatings";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_ProjectProfileRating_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProjectProfileRating");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "ProjectProfileRatings";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_ProjectProfileRating_Create";
			node.ParentKey = "MBKLivingArt_ProjectProfileRating_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProjectProfileRating");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = ProjectProfileRatingResources.PROJECTPROFILERATINGS_EDIT;
            //node.Controller = "ProjectProfileRatings";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_ProjectProfileRating_Edit";
			//node.ParentKey = "MBKLivingArt_ProjectProfileRating_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "ProjectProfileRating";
            node.Controller = "ProjectProfileRatings";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_ProjectProfileRating_Details";
			node.ParentKey = "MBKLivingArt_ProjectProfileRating_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProjectProfileRating");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region ProjectsQuestion
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "ProjectsQuestion");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "ProjectsQuestions";
		
		       node.Controller = "ProjectsQuestions";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_ProjectsQuestion_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProjectsQuestion");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "ProjectsQuestions";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_ProjectsQuestion_Create";
			node.ParentKey = "MBKLivingArt_ProjectsQuestion_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProjectsQuestion");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = ProjectsQuestionResources.PROJECTSQUESTIONS_EDIT;
            //node.Controller = "ProjectsQuestions";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_ProjectsQuestion_Edit";
			//node.ParentKey = "MBKLivingArt_ProjectsQuestion_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "ProjectsQuestion";
            node.Controller = "ProjectsQuestions";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_ProjectsQuestion_Details";
			node.ParentKey = "MBKLivingArt_ProjectsQuestion_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProjectsQuestion");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region ProfilePage
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "ProfilePage");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "ProfilePages";
		
		       node.Controller = "ProfilePages";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_ProfilePage_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfilePage");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "ProfilePages";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_ProfilePage_Create";
			node.ParentKey = "MBKLivingArt_ProfilePage_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfilePage");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = ProfilePageResources.PROFILEPAGES_EDIT;
            //node.Controller = "ProfilePages";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_ProfilePage_Edit";
			//node.ParentKey = "MBKLivingArt_ProfilePage_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "ProfilePage";
            node.Controller = "ProfilePages";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_ProfilePage_Details";
			node.ParentKey = "MBKLivingArt_ProfilePage_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfilePage");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region ProfileEvent
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "ProfileEvent");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "ProfileEvents";
		
		       node.Controller = "ProfileEvents";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_ProfileEvent_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfileEvent");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "ProfileEvents";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_ProfileEvent_Create";
			node.ParentKey = "MBKLivingArt_ProfileEvent_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfileEvent");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = ProfileEventResources.PROFILEEVENTS_EDIT;
            //node.Controller = "ProfileEvents";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_ProfileEvent_Edit";
			//node.ParentKey = "MBKLivingArt_ProfileEvent_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "ProfileEvent";
            node.Controller = "ProfileEvents";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_ProfileEvent_Details";
			node.ParentKey = "MBKLivingArt_ProfileEvent_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProfileEvent");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region ProjectIncidentDetail
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "ProjectIncidentDetail");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "ProjectIncidentDetails";
		
		       node.Controller = "ProjectIncidentDetails";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_ProjectIncidentDetail_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProjectIncidentDetail");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "ProjectIncidentDetails";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_ProjectIncidentDetail_Create";
			node.ParentKey = "MBKLivingArt_ProjectIncidentDetail_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProjectIncidentDetail");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = ProjectIncidentDetailResources.PROJECTINCIDENTDETAILS_EDIT;
            //node.Controller = "ProjectIncidentDetails";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_ProjectIncidentDetail_Edit";
			//node.ParentKey = "MBKLivingArt_ProjectIncidentDetail_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "ProjectIncidentDetail";
            node.Controller = "ProjectIncidentDetails";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_ProjectIncidentDetail_Details";
			node.ParentKey = "MBKLivingArt_ProjectIncidentDetail_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProjectIncidentDetail");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region ProjectContractTerm
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "ProjectContractTerm");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "ProjectContractTerms";
		
		       node.Controller = "ProjectContractTerms";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_ProjectContractTerm_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProjectContractTerm");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "ProjectContractTerms";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_ProjectContractTerm_Create";
			node.ParentKey = "MBKLivingArt_ProjectContractTerm_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProjectContractTerm");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = ProjectContractTermResources.PROJECTCONTRACTTERMS_EDIT;
            //node.Controller = "ProjectContractTerms";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_ProjectContractTerm_Edit";
			//node.ParentKey = "MBKLivingArt_ProjectContractTerm_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "ProjectContractTerm";
            node.Controller = "ProjectContractTerms";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_ProjectContractTerm_Details";
			node.ParentKey = "MBKLivingArt_ProjectContractTerm_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProjectContractTerm");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region LAFile
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "LAFile");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "LAFiles";
		
		       node.Controller = "LAFiles";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_LAFile_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "LAFile");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "LAFiles";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_LAFile_Create";
			node.ParentKey = "MBKLivingArt_LAFile_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "LAFile");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = LAFileResources.LAFILES_EDIT;
            //node.Controller = "LAFiles";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_LAFile_Edit";
			//node.ParentKey = "MBKLivingArt_LAFile_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "LAFile";
            node.Controller = "LAFiles";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_LAFile_Details";
			node.ParentKey = "MBKLivingArt_LAFile_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "LAFile");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region UserProfile
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "UserProfile");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "UserProfiles";
		
		       node.Controller = "UserProfiles";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_UserProfile_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "UserProfile");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "UserProfiles";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_UserProfile_Create";
			node.ParentKey = "MBKLivingArt_UserProfile_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "UserProfile");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = UserProfileResources.USERPROFILES_EDIT;
            //node.Controller = "UserProfiles";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_UserProfile_Edit";
			//node.ParentKey = "MBKLivingArt_UserProfile_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "UserProfile";
            node.Controller = "UserProfiles";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_UserProfile_Details";
			node.ParentKey = "MBKLivingArt_UserProfile_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "UserProfile");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region InterestedPage
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "InterestedPage");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "InterestedPages";
		
		       node.Controller = "InterestedPages";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_InterestedPage_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "InterestedPage");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "InterestedPages";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_InterestedPage_Create";
			node.ParentKey = "MBKLivingArt_InterestedPage_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "InterestedPage");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = InterestedPageResources.INTERESTEDPAGES_EDIT;
            //node.Controller = "InterestedPages";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_InterestedPage_Edit";
			//node.ParentKey = "MBKLivingArt_InterestedPage_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "InterestedPage";
            node.Controller = "InterestedPages";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_InterestedPage_Details";
			node.ParentKey = "MBKLivingArt_InterestedPage_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "InterestedPage");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion
			#region ProjectContract
			  plural = "";
            single = "";
            entityText = entityTexts.FirstOrDefault(p => p.EntityKey == "ProjectContract");
            if (entityText != null)
            {
                plural = entityText.PluralName;
                single = entityText.Name;
            }

            node = new DynamicNode();
            node.Title = !string.IsNullOrEmpty(plural) ? plural : "ProjectContracts";
		
		       node.Controller = "ProjectContracts";
            node.Area = "MBKLivingArt";
            node.Action = "Index";
            node.Key = "MBKLivingArt_ProjectContract_List";
			//node.CacheResolvedUrl = true;
			node.ParentKey = "MBKLivingArt_all_catalogs";
 			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProjectContract");
            node.Attributes.Add("permissionKey", "r");            
			nodes.Add(node);

			
			// Create
			node = new DynamicNode();
            node.Title =GlobalMessages.ADD_NEW;
            node.Controller = "ProjectContracts";
            node.Area = "MBKLivingArt";
            node.Action = "CreateGen";
            node.Key = "MBKLivingArt_ProjectContract_Create";
			node.ParentKey = "MBKLivingArt_ProjectContract_List";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProjectContract");
			node.Attributes.Add("visiblemenu", "false");
			
			
            nodes.Add(node);

			// Edit
			//node = new DynamicNode();
            //node.Title = ProjectContractResources.PROJECTCONTRACTS_EDIT;
            //node.Controller = "ProjectContracts";
            //node.Area = "MBKLivingArt";
            //node.Action = "EditGen";
            //node.Key = "MBKLivingArt_ProjectContract_Edit";
			//node.ParentKey = "MBKLivingArt_ProjectContract_List";
			//node.Attributes.Add("visible", "false");
			//node.Attributes.Add("dynamicParameters", "id");
			//node.Attributes.Add("isDynamic", "true");
            //nodes.Add(node);

			// Details
			node = new DynamicNode();
           //node.Title = !string.IsNullOrEmpty(single) ? single : "ProjectContract";
            node.Controller = "ProjectContracts";
            node.Area = "MBKLivingArt";
            node.Action = "DetailsGen";
            node.Key = "MBKLivingArt_ProjectContract_Details";
			node.ParentKey = "MBKLivingArt_ProjectContract_List";
			node.Attributes.Add("visible", "false");
			node.Attributes.Add("dynamicParameters", "id");
			node.Attributes.Add("isDynamic", "true");
			node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.Attributes.Add("businessObjectKey", "ProjectContract");
			node.PreservedRouteParameters.Add("id");
            nodes.Add(node); 

			#endregion

 			OnCreatedNodes(this, ref nodes);
			
			node = new DynamicNode();
            //node.Title = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.SYSTEM;
            node.Controller = "Navigation";
            node.Area = "";
            node.Action = "Index";
            node.Key = "MBKLivingArt_System_override";
            node.RouteValues.Add("id", node.Key);
            node.RouteValues.Add("overrideModule", "MBKLivingArt");
            
			node.ParentKey = "MBKLivingArt";
			node.Attributes.Add("moduleKey", "MBKLivingArt");
			 node.Attributes.Add("permissionKey", "admin");
			 textUI.SetTextTo(node, "Title", typeof(SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages), "SYSTEM");

            nodes.Add(node);



			  node = new DynamicNode();
            //node.Title = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.USERS_AND_ROLES ;
            node.Controller = "secRoles";
            node.Area = "SFSdotNetFrameworkSecurity";
            node.Action = "Index";
            node.Key = "SFSdotNetFrameworkSecurity_MBKLivingArt_roles";
            node.ParentKey = "MBKLivingArt_System_override";
            node.Attributes.Add("moduleKey", "MBKLivingArt");
                      node.RouteValues.Add("overrideModule", "MBKLivingArt");
            node.Attributes.Add("permissionKey", "admin");
            textUI.SetTextTo(node, "Title", typeof(SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages), "ROLES");

            nodes.Add(node);




			 node = new DynamicNode();
            //node.Title = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.USERS_AND_ROLES ;
            node.Controller = "secUserCompanies";
            node.Area = "SFSdotNetFrameworkSecurity";
            node.Action = "Index";
            node.Key = "SFSdotNetFrameworkSecurity_MBKLivingArt_user_companies";
            node.ParentKey = "MBKLivingArt_System_override";
            node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.RouteValues.Add("overrideModule", "MBKLivingArt");
            node.Attributes.Add("permissionKey", "admin");
			textUI.SetTextTo(node, "Title", typeof(SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages), "USERS_AND_ROLES");

            nodes.Add(node);

			
			 node = new DynamicNode();
            node.Controller = "secCompanies";
            node.Area = "SFSdotNetFrameworkSecurity";
            node.Action = "Index";
            node.Key = "SFSdotNetFrameworkSecurity_MBKLivingArt_child_companies";
            node.ParentKey = "MBKLivingArt_System_override";
            node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.RouteValues.Add("overrideModule", "MBKLivingArt");
			node.RouteValues.Add("usemode", "children");
            node.Attributes.Add("permissionKey", "admin");
			textUI.SetTextTo(node, "Title", typeof(SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages), "COMPANIES_CHILDS");

            nodes.Add(node);
			 node = new DynamicNode();
            //node.Title = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.EVENT_LOG;
            node.Controller = "secEventLogs";
            node.Area = "SFSdotNetFrameworkSecurity";
            node.Action = "Index";
            node.Key = "SFSdotNetFrameworkSecurity_MBKLivingArt_EventLogs";
            node.ParentKey = "MBKLivingArt_System_override";
            node.Attributes.Add("moduleKey", "MBKLivingArt");

            node.RouteValues.Add("overrideModule", "MBKLivingArt");

			
            node.Attributes.Add("permissionKey", "admin");
			textUI.SetTextTo(node, "Title", typeof(SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages), "EVENT_LOG");

            nodes.Add(node);
			 node = new DynamicNode();
            //node.Title = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.EVENT_LOG;
            node.Controller = "Dashboard";
            node.Area = "SFSdotNetFrameworkSecurity";
            node.Action = "Statics";
            node.Key = "SFSdotNetFrameworkSecurity_MBKLivingArt_Statics";
            node.ParentKey = "MBKLivingArt_System_override";
            node.Attributes.Add("moduleKey", "MBKLivingArt");

            node.RouteValues.Add("overrideModule", "MBKLivingArt");
            node.Attributes.Add("permissionKey", "admin");
            textUI.SetTextTo(node, "Title", typeof(SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages), "SERVICE_USE_STATICS");

            nodes.Add(node);


			 node = new DynamicNode();
           // node.Title = SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages.CHANGE_AUDITING;
            node.Controller = "secAudits";
            node.Area = "SFSdotNetFrameworkSecurity";
            node.Action = "Index";
            node.Key = "SFSdotNetFrameworkSecurity_MBKLivingArt_ChangeAutiting";
            node.ParentKey = "MBKLivingArt_System_override";
            node.Attributes.Add("moduleKey", "MBKLivingArt");
            node.RouteValues.Add("overrideModule", "MBKLivingArt");
            node.Attributes.Add("permissionKey", "admin");
			 textUI.SetTextTo(node, "Title", typeof(SFSdotNet.Framework.Web.Mvc.Resources.GlobalMessages), "CHANGE_AUDITING");

            nodes.Add(node);




            return nodes;
        }
    }
}
