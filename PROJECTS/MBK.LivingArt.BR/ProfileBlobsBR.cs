﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MBK.LivingArt.BusinessObjects;
using SFSdotNet.Framework.BR;

namespace MBK.LivingArt.BR
{
    public partial class ProfileBlobsBR
    {
        partial void OnQuerySettings(object sender, BusinessRulesEventArgs<ProfileBlob> e)
        {
            e.SetQueryComputedField(ProfileBlob.PropertyNames.ExistFile, "iif(it.LAFile != null, true, false)");
        }
    }
}
