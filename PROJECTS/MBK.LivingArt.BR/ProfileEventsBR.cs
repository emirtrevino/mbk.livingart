﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MBK.LivingArt.BusinessObjects;
using SFSdotNet.Framework.BR;
using SFSdotNet.Framework.My;

namespace MBK.LivingArt.BR
{
    public partial class ProfileEventsBR
    {
        partial void OnCreated(object sender, BusinessRulesEventArgs<ProfileEvent> e)
        {
            
            if (e.Item.UserProfile != null)
            {
                Guid? guidUser = e.Item.UserProfile.GuidUser;
                var userFinded = SFSdotNet.Framework.Security.BR.secUsersBR.Instance.GetBy(p=> p.GuidUser == guidUser ).FirstOrDefault();
                if (userFinded != null)
                {
                    SFSdotNet.Framework.Messages.Messages messages = new SFSdotNet.Framework.Messages.Messages();
                    
                    //SFSdotNet.Framework.Net.Mail email = new SFSdotNet.Framework.Net.Mail();
                    
                    e.ContextRequest.SetParam("footer-html", "<span></span>"); // especificar un html de firma de la plantilla de correo
                    e.ContextRequest.SetParam("use-from", true); // enviar customemailsend de las propiedades de la empresa
                    e.ContextRequest.SetParam("prevent-global-html", true);
                    messages.SendMessage(userFinded, "Invitación a evento", e.Item.Description,  e.ContextRequest, true, null);
                }
            }
        }

        public void MyCustomMethodA(ProfileEvent item, ContextRequest contextRequest)
        {
            
        }

        partial void OnCreating(object sender, BusinessRulesEventArgs<ProfileEvent> e)
        {
            if (e.Item.UserProfile != null)
            {
                var userProfiledFinded = BR.UserProfilesBR.Instance.GetBy(p=> p.GuidUser == e.Item.UserProfile.GuidUser ).FirstOrDefault();

                if (userProfiledFinded == null)
                {
                    Guid? guidUser = e.Item.UserProfile.GuidUser;
                    var userSystemFinded = SFSdotNet.Framework.Security.BR.secUsersBR.Instance.GetBy(p=> p.GuidUser == guidUser).FirstOrDefault();
                    if (userSystemFinded != null)
                    {
                        userProfiledFinded = new UserProfile();
                        userProfiledFinded.GuidUser = guidUser.Value;
                        userProfiledFinded.Name = userSystemFinded.Email;
                        userProfiledFinded.ProfileType = 1;
                        userProfiledFinded.ProfileStatus = 1;
                        userProfiledFinded = BR.UserProfilesBR.Instance.Create(userProfiledFinded, e.ContextRequest);

                        e.Item.UserProfile = userProfiledFinded;

                    }
                }
            }
        }
    }
}
