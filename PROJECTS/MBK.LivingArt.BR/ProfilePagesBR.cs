﻿using MBK.LivingArt.BusinessObjects;
using SFSdotNet.Framework.My;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFSdotNet.Framework.BR;

namespace MBK.LivingArt.BR
{
    public partial class ProfilePagesBR
    {

        partial void OnCounting(object sender, BusinessRulesEventArgs<ProfilePage> e)
        {
            //

        }

        partial void OnDeleted(object sender, BusinessRulesEventArgs<ProfilePage> e)
        {
            //7
        }

        partial void OnDeleting(object sender, BusinessRulesEventArgs<ProfilePage> e)
        {
            //
        }

        partial void OnGetting(object sender, BusinessRulesEventArgs<ProfilePage> e)
        {
            //
           if (!string.IsNullOrEmpty(e.ContextRequest.FreeText))
            {
                e.Filter.Parts.Clear();
                e.Filter.AppendFilter(string.Format("Name.Contains(\"{0}\") OR Description.Contains(\"{0}\")", e.ContextRequest.FreeText));
            }
        }

        partial void OnQuerySettings(object sender, BusinessRulesEventArgs<ProfilePage> e)
        {
            
        }
        partial void OnUpdating(object sender, BusinessRulesEventArgs<ProfilePage> e)
        {
            
        }

        partial void OnUpdated(object sender, BusinessRulesEventArgs<ProfilePage> e)
        {
            //throw new NotImplementedException();
        }
        partial void OnUpdatedAgile(object sender, BusinessRulesEventArgs<ProfilePage> e)
        {
            
        }
        public void CreateProfilePage(ProfilePage item , ContextRequest contextRequest)
        {
            using (EFContext context = new BR.EFContext())
            {

            }
        }

        public void MyCustomProcess(ContextRequest contextRequest)
        {
            var userProfiles = BR.UserProfilesBR.Instance.GetBy(p=> p.Birthdate < new DateTime(1990,1,1));

            List<UserProfile> newUserProfiles = new List<UserProfile>();
            foreach (var userProfile in userProfiles)
            {
                userProfile.ProfileStatus = 0;

                if (userProfile.Alias.StartsWith("A"))
                {
                    BR.UserProfilesBR.Instance.Delete(userProfile, contextRequest);
                }
                UserProfile newUserProfile = new UserProfile();
                newUserProfile.Name = userProfile.Name + " v2";
                newUserProfiles.Add(newUserProfile);
                //newUserProfile = BR.UserProfilesBR.Instance.Create(newUserProfile, contextRequest);
                //BR.UserProfilesBR.Instance.UpdateAgile(userProfile, "ProfileStatus");
                // solo cuando se esta seguro que será solo una conexión a la base
            }

            BR.UserProfilesBR.Instance.UpdateBulk(userProfiles, contextRequest);
            BR.UserProfilesBR.Instance.CreateBulk(newUserProfiles, contextRequest);


        }
        partial void OnCreating(object sender, BusinessRulesEventArgs<ProfilePage> e)
        {
            ProcessImages(e.Item, e.ContextRequest);
        }

        partial void OnTaken(object sender, BusinessRulesEventArgs<ProfilePage> e)
        {

            SFSdotNet.Framework.Data.StaticStorage storage = new SFSdotNet.Framework.Data.StaticStorage();
            storage.ModuleKey = "MBKLivingArt";
            storage.GuidCompany = e.ContextRequest.Company.GuidCompany;
            foreach (var item in e.Items)
            {
                if (!string.IsNullOrEmpty(item.ContentHTML))
                {
                    item.ContentHTML = storage.ReplaceRepositoryPathsForShow(item.ContentHTML);
                }
            }
        }

        private void ProcessImages(ProfilePage content, ContextRequest context)
        {
            //SFSdotNet.Framework.Data.StaticStorage storage = new SFSdotNet.Framework.Data.StaticStorage();
            //storage.ModuleKey = "MBKLivingArt";
            //storage.GuidCompany = context.Company.GuidCompany;
            if (!string.IsNullOrEmpty(content.ContentHTML))
                content.ContentHTML = SFSdotNet.Framework.Data.StaticStorage.ProcessHtmlWithContents(content.ContentHTML, "MBKLivingArt", context);


        }

    }
}
