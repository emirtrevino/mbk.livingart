﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MBK.LivingArt.BusinessObjects;
using SFSdotNet.Framework.BR;

namespace MBK.LivingArt.BR
{
   public partial  class supEntidadPadresBR
    {
        public void MyCustomRule()
        {

        }

        partial void OnCreating(object sender, BusinessRulesEventArgs<supEntidadPadre> e)
        {
            //Antes de insertarse
            e.Item.Name = e.Item.Name + " abc";
            var otrosElementos = BR.supAgentesBR.Instance.GetBy(p => true, e.ContextRequest);
            foreach (var elemento in otrosElementos)
            {

            }
        }

        partial void OnCreated(object sender, BusinessRulesEventArgs<supEntidadPadre> e)
        {
            (new SFSdotNet.Framework.Messages.Messages()).SendMessage("", "","");
        }
    }
}
