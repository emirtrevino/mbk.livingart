﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MBK.LivingArt.BusinessObjects;
using SFSdotNet.Framework.BR;
using SFSdotNet.Framework.My;

namespace MBK.LivingArt.BR
{
    public partial class UserProfilesBR
    {
        public void ActivateUsers(string query, Guid[] guids, ContextRequest contextRequest)
        {
            var process = contextRequest.CurrentContext.StartProcess("activate-users", "Activando usuarios");

            try
            {
                contextRequest.CustomQuery = new CustomQuery();
                contextRequest.CustomQuery.ExtraParams = new object[] { guids };
                
                contextRequest.CustomQuery.IncludeDeleted = true;
                var items = this.GetBy(query, contextRequest);
                int total = items.Count;
                int n = 1;
                foreach (var item in items)
                {

                    System.Threading.Thread.Sleep(10000);
                    item.ProfileStatus = 1;

                    n++;

                    contextRequest.CurrentContext.AddProgressProcess(process.GuidProcess, (n * 100) / total);

                }

                // actualización masiva
                this.UpdateBulk(items, contextRequest);

                contextRequest.CurrentContext.FinishProcess(process, SFSdotNet.Framework.My.MessageResultTypes.Ok);

            }
            catch (Exception ex)
            {
                contextRequest.CurrentContext.FinishProcess(process, SFSdotNet.Framework.My.MessageResultTypes.Error, ex);

            }
        }
        partial void OnQuerySettings(object sender, BusinessRulesEventArgs<UserProfile> e)
        {
            //UserProfile.PropertyNames.
            e.SetQueryComputedField(UserProfile.PropertyNames.CountMusicPreferencesComputed, "it.ProfileMusicPreferences.Count()");
        }

    }
}
