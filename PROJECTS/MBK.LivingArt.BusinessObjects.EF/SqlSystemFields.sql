﻿ 
 
PRINT 'Table CommunicationProject, entity CommunicationProject'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.CommunicationProject', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.CommunicationProject 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.CommunicationProject', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.CommunicationProject 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.CommunicationProject', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.CommunicationProject 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.CommunicationProject', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.CommunicationProject 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.CommunicationProject', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.CommunicationProject 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.CommunicationProject', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.CommunicationProject 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table MusicPreference, entity MusicPreference'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.MusicPreference', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.MusicPreference 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.MusicPreference', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.MusicPreference 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.MusicPreference', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.MusicPreference 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.MusicPreference', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.MusicPreference 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.MusicPreference', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.MusicPreference 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.MusicPreference', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.MusicPreference 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table PageType, entity PageType'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.PageType', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.PageType 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.PageType', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.PageType 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.PageType', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.PageType 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.PageType', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.PageType 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.PageType', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.PageType 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.PageType', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.PageType 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table ProfileBlob, entity ProfileBlob'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfileBlob', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.ProfileBlob 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfileBlob', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.ProfileBlob 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfileBlob', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.ProfileBlob 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfileBlob', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.ProfileBlob 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfileBlob', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.ProfileBlob 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfileBlob', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.ProfileBlob 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table ProfileMusicPreference, entity ProfileMusicPreference'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfileMusicPreference', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.ProfileMusicPreference 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfileMusicPreference', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.ProfileMusicPreference 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfileMusicPreference', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.ProfileMusicPreference 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfileMusicPreference', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.ProfileMusicPreference 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfileMusicPreference', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.ProfileMusicPreference 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfileMusicPreference', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.ProfileMusicPreference 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table ProfilePageBlob, entity ProfilePageBlob'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePageBlob', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.ProfilePageBlob 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePageBlob', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.ProfilePageBlob 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePageBlob', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.ProfilePageBlob 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePageBlob', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.ProfilePageBlob 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePageBlob', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.ProfilePageBlob 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePageBlob', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.ProfilePageBlob 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table ProfilePageStore, entity ProfilePageStore'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePageStore', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.ProfilePageStore 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePageStore', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.ProfilePageStore 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePageStore', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.ProfilePageStore 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePageStore', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.ProfilePageStore 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePageStore', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.ProfilePageStore 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePageStore', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.ProfilePageStore 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table ProfilePageTermContract, entity ProfilePageTermContract'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePageTermContract', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.ProfilePageTermContract 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePageTermContract', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.ProfilePageTermContract 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePageTermContract', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.ProfilePageTermContract 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePageTermContract', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.ProfilePageTermContract 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePageTermContract', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.ProfilePageTermContract 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePageTermContract', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.ProfilePageTermContract 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table Project, entity Project'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Project', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.Project 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Project', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.Project 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Project', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.Project 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Project', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.Project 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Project', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.Project 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.Project', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.Project 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table ProjectContract, entity ProjectContract'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectContract', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.ProjectContract 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectContract', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.ProjectContract 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectContract', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.ProjectContract 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectContract', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.ProjectContract 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectContract', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.ProjectContract 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectContract', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.ProjectContract 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table ProjectIncident, entity ProjectIncident'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectIncident', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.ProjectIncident 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectIncident', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.ProjectIncident 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectIncident', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.ProjectIncident 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectIncident', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.ProjectIncident 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectIncident', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.ProjectIncident 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectIncident', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.ProjectIncident 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table ProjectProfileRating, entity ProjectProfileRating'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectProfileRating', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.ProjectProfileRating 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectProfileRating', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.ProjectProfileRating 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectProfileRating', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.ProjectProfileRating 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectProfileRating', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.ProjectProfileRating 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectProfileRating', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.ProjectProfileRating 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectProfileRating', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.ProjectProfileRating 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table ProjectsQuestion, entity ProjectsQuestion'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectsQuestion', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.ProjectsQuestion 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectsQuestion', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.ProjectsQuestion 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectsQuestion', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.ProjectsQuestion 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectsQuestion', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.ProjectsQuestion 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectsQuestion', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.ProjectsQuestion 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectsQuestion', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.ProjectsQuestion 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table InterestedPage, entity InterestedPage'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.InterestedPage', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.InterestedPage 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.InterestedPage', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.InterestedPage 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.InterestedPage', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.InterestedPage 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.InterestedPage', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.InterestedPage 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.InterestedPage', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.InterestedPage 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.InterestedPage', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.InterestedPage 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table ProfilePage, entity ProfilePage'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePage', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.ProfilePage 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePage', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.ProfilePage 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePage', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.ProfilePage 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePage', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.ProfilePage 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePage', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.ProfilePage 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfilePage', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.ProfilePage 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table ProfileEvent, entity ProfileEvent'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfileEvent', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.ProfileEvent 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfileEvent', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.ProfileEvent 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfileEvent', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.ProfileEvent 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfileEvent', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.ProfileEvent 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfileEvent', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.ProfileEvent 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProfileEvent', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.ProfileEvent 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table ProjectIncidentDetail, entity ProjectIncidentDetail'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectIncidentDetail', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.ProjectIncidentDetail 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectIncidentDetail', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.ProjectIncidentDetail 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectIncidentDetail', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.ProjectIncidentDetail 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectIncidentDetail', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.ProjectIncidentDetail 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectIncidentDetail', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.ProjectIncidentDetail 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectIncidentDetail', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.ProjectIncidentDetail 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table ProjectContractTerm, entity ProjectContractTerm'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectContractTerm', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.ProjectContractTerm 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectContractTerm', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.ProjectContractTerm 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectContractTerm', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.ProjectContractTerm 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectContractTerm', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.ProjectContractTerm 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectContractTerm', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.ProjectContractTerm 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.ProjectContractTerm', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.ProjectContractTerm 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table LAFile, entity LAFile'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.LAFile', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.LAFile 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.LAFile', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.LAFile 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.LAFile', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.LAFile 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.LAFile', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.LAFile 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.LAFile', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.LAFile 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.LAFile', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.LAFile 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO
PRINT 'Table UserProfile, entity UserProfile'
-- CreatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.UserProfile', N'U'),'CreatedDate','ColumnId') is null
begin 
  alter table dbo.UserProfile 
  add CreatedDate DATETIME null 
end
GO
-- UpdatedDate
if COLUMNPROPERTY(OBJECT_ID(N'dbo.UserProfile', N'U'),'UpdatedDate','ColumnId') is null
begin 
  alter table dbo.UserProfile 
  add UpdatedDate DATETIME null 
end
GO
-- CreatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.UserProfile', N'U'),'CreatedBy','ColumnId') is null
begin 
  alter table dbo.UserProfile 
  add CreatedBy uniqueidentifier null 
end
GO
-- UpdatedBy
if COLUMNPROPERTY(OBJECT_ID(N'dbo.UserProfile', N'U'),'UpdatedBy','ColumnId') is null
begin 
  alter table dbo.UserProfile 
  add UpdatedBy uniqueidentifier null 
end
GO

-- Bytes
if COLUMNPROPERTY(OBJECT_ID(N'dbo.UserProfile', N'U'),'Bytes','ColumnId') is null
begin 
  alter table dbo.UserProfile 
  add Bytes [int] null 
end
GO

-- IsDeleted
if COLUMNPROPERTY(OBJECT_ID(N'dbo.UserProfile', N'U'),'IsDeleted','ColumnId') is null
begin 
  alter table dbo.UserProfile 
  add IsDeleted [bit] null DEFAULT ('false') 
end
GO

