﻿ 
 
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'CommunicationProject' and i.name = 'CommunicationProject_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX CommunicationProject_FullStatics_Idx 
    on dbo.CommunicationProject ( 
GuidCommunicationProject 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'MusicPreference' and i.name = 'MusicPreference_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX MusicPreference_FullStatics_Idx 
    on dbo.MusicPreference ( 
GuidMusicPreference 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'PageType' and i.name = 'PageType_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX PageType_FullStatics_Idx 
    on dbo.PageType ( 
GuidPageType 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'ProfileBlob' and i.name = 'ProfileBlob_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX ProfileBlob_FullStatics_Idx 
    on dbo.ProfileBlob ( 
GuidProfileBlob 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'ProfileMusicPreference' and i.name = 'ProfileMusicPreference_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX ProfileMusicPreference_FullStatics_Idx 
    on dbo.ProfileMusicPreference ( 
GuidProfileMusicPreference 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'ProfilePageBlob' and i.name = 'ProfilePageBlob_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX ProfilePageBlob_FullStatics_Idx 
    on dbo.ProfilePageBlob ( 
GuidProfilePageBlob 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'ProfilePageStore' and i.name = 'ProfilePageStore_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX ProfilePageStore_FullStatics_Idx 
    on dbo.ProfilePageStore ( 
GuidProfilePageStore 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'ProfilePageTermContract' and i.name = 'ProfilePageTermContract_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX ProfilePageTermContract_FullStatics_Idx 
    on dbo.ProfilePageTermContract ( 
GuidProfilePageTermContact 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'Project' and i.name = 'Project_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX Project_FullStatics_Idx 
    on dbo.Project ( 
GuidProject 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'ProjectContract' and i.name = 'ProjectContract_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX ProjectContract_FullStatics_Idx 
    on dbo.ProjectContract ( 
GuidProjectContract 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'ProjectIncident' and i.name = 'ProjectIncident_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX ProjectIncident_FullStatics_Idx 
    on dbo.ProjectIncident ( 
GuidProjectIncident 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'ProjectProfileRating' and i.name = 'ProjectProfileRating_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX ProjectProfileRating_FullStatics_Idx 
    on dbo.ProjectProfileRating ( 
GuidProjectProfileRating 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'ProjectsQuestion' and i.name = 'ProjectsQuestion_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX ProjectsQuestion_FullStatics_Idx 
    on dbo.ProjectsQuestion ( 
GuidProjectQuestion 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'InterestedPage' and i.name = 'InterestedPage_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX InterestedPage_FullStatics_Idx 
    on dbo.InterestedPage ( 
GuidProjectContract 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'ProfilePage' and i.name = 'ProfilePage_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX ProfilePage_FullStatics_Idx 
    on dbo.ProfilePage ( 
GuidProfilePage 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'ProfileEvent' and i.name = 'ProfileEvent_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX ProfileEvent_FullStatics_Idx 
    on dbo.ProfileEvent ( 
GuidProfileEvent 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'ProjectIncidentDetail' and i.name = 'ProjectIncidentDetail_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX ProjectIncidentDetail_FullStatics_Idx 
    on dbo.ProjectIncidentDetail ( 
GuidProjectIncidentDetail 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'ProjectContractTerm' and i.name = 'ProjectContractTerm_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX ProjectContractTerm_FullStatics_Idx 
    on dbo.ProjectContractTerm ( 
ProjectContractTermId 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'LAFile' and i.name = 'LAFile_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX LAFile_FullStatics_Idx 
    on dbo.LAFile ( 
GuidFile 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)
IF NOT EXISTS (
    SELECT * FROM sys.tables t
    INNER JOIN sys.schemas s ON t.schema_id = s.schema_id
    INNER JOIN sys.indexes i on i.object_id = t.object_id
    WHERE s.name = 'dbo' AND t.name = 'UserProfile' and i.name = 'UserProfile_FullStatics_Idx'
) 
    CREATE NONCLUSTERED INDEX UserProfile_FullStatics_Idx 
    on dbo.UserProfile ( 
GuidUser 

        ,CreatedDate desc 

	,UpdatedDate desc 
		,CreatedBy 
		,IsDeleted desc
    ) 
	
	INCLUDE(Bytes)
	WITH (ONLINE = ON)

