﻿ 
 

#region using
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MBK.LivingArt.Client.Models;

using System.Configuration;
using System.Collections.Generic;

#endregion
namespace MBK.LivingArt.Tests
{

	 [TestClass]
	  public class AllBasicTests
	  {
	    private string url = null;
        public AllBasicTests()
        {
            url = ConfigurationManager.AppSettings["urlApp"];
        }
		private ApiWrapper<TModel> GetWrapper<TModel>() where TModel:class 
        {
            
            ApiWrapper<TModel> apiWrapper = new ApiWrapper<TModel>();
            apiWrapper.Username = ConfigurationManager.AppSettings["username"];
            apiWrapper.Password = ConfigurationManager.AppSettings["password"];
            apiWrapper.CompanyId = ConfigurationManager.AppSettings["companyId"];
            return apiWrapper;
        }
	public MusicPreference MusicPreferenceSample(){
		 MusicPreference item = new MusicPreference();
            item.GuidMusicPreference = Guid.NewGuid();

            item.Description = Utils.GetString(10);







		return item;
	}
 [TestMethod]
  public void  MusicPreferenceCreate()
  {
            this. MusicPreferenceCreate(null, null);
   }

	[TestMethod]
	 public void MusicPreferenceCreate( List<MusicPreference> items, MusicPreference item )
        {
            var apiWrapper = GetWrapper<MusicPreference>();
            apiWrapper.EntityKeyName = "MusicPreference";
            apiWrapper.EntitySetName  = "MusicPreferences";
			if (item == null && items == null)
            {
				apiWrapper.Item = MusicPreferenceSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.GuidMusicPreference;
				var existent = Services.ApiGetByKey<MusicPreference>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	public PageType PageTypeSample(){
		 PageType item = new PageType();
            item.GuidPageType = Guid.NewGuid();

            item.Name = Utils.GetString(10);

            item.ProfileType = Utils.GetInt();








		return item;
	}
 [TestMethod]
  public void  PageTypeCreate()
  {
            this. PageTypeCreate(null, null);
   }

	[TestMethod]
	 public void PageTypeCreate( List<PageType> items, PageType item )
        {
            var apiWrapper = GetWrapper<PageType>();
            apiWrapper.EntityKeyName = "PageType";
            apiWrapper.EntitySetName  = "PageTypes";
			if (item == null && items == null)
            {
				apiWrapper.Item = PageTypeSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.GuidPageType;
				var existent = Services.ApiGetByKey<PageType>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	public LAFile LAFileSample(){
		 LAFile item = new LAFile();
            item.GuidFile = Guid.NewGuid();

            item.FileName = Utils.GetString(10);

            item.FileSize = Utils.GetInt();


            item.FileType = Utils.GetString(10);

            item.FileStorage = Utils.GetString(10);

            item.ContainerStorage = Utils.GetString(10);







		return item;
	}
 [TestMethod]
  public void  LAFileCreate()
  {
            this. LAFileCreate(null, null);
   }

	[TestMethod]
	 public void LAFileCreate( List<LAFile> items, LAFile item )
        {
            var apiWrapper = GetWrapper<LAFile>();
            apiWrapper.EntityKeyName = "LAFile";
            apiWrapper.EntitySetName  = "LAFiles";
			if (item == null && items == null)
            {
				apiWrapper.Item = LAFileSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.GuidFile;
				var existent = Services.ApiGetByKey<LAFile>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	public UserProfile UserProfileSample(){
		 UserProfile item = new UserProfile();
            item.GuidUser = Guid.NewGuid();

            item.Name = Utils.GetString(10);

            item.Alias = Utils.GetString(10);

            item.Biography = Utils.GetString(10);

            item.Birthdate = Utils.GetDateTime();
			

            item.ProfileType = Utils.GetInt();


            item.ProfileStatus = Utils.GetInt();









            item.CountMusicPreferencesComputed = Utils.GetInt();


		return item;
	}
 [TestMethod]
  public void  UserProfileCreate()
  {
            this. UserProfileCreate(null, null);
   }

	[TestMethod]
	 public void UserProfileCreate( List<UserProfile> items, UserProfile item )
        {
            var apiWrapper = GetWrapper<UserProfile>();
            apiWrapper.EntityKeyName = "UserProfile";
            apiWrapper.EntitySetName  = "UserProfiles";
			if (item == null && items == null)
            {
				apiWrapper.Item = UserProfileSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.GuidUser;
				var existent = Services.ApiGetByKey<UserProfile>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	public Project ProjectSample(){
		 Project item = new Project();
            item.GuidProject = Guid.NewGuid();


            item.Description = Utils.GetString(10);

            item.PublishDate = Utils.GetDateTime();
			

            item.ProjectDate = Utils.GetDateTime();
			


            item.IsExternal = Utils.GetBool();

            item.Surface = Utils.GetDecimal();


            item.Money = Utils.GetDecimal();


            item.StatusProject = Utils.GetInt();








		return item;
	}
 [TestMethod]
  public void  ProjectCreate()
  {
            this. ProjectCreate(null, null);
   }

	[TestMethod]
	 public void ProjectCreate( List<Project> items, Project item )
        {
            var apiWrapper = GetWrapper<Project>();
            apiWrapper.EntityKeyName = "Project";
            apiWrapper.EntitySetName  = "Projects";
			if (item == null && items == null)
            {
				apiWrapper.Item = ProjectSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.GuidProject;
				var existent = Services.ApiGetByKey<Project>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	public CommunicationProject CommunicationProjectSample(){
		 CommunicationProject item = new CommunicationProject();
            item.GuidCommunicationProject = Guid.NewGuid();



            item.Description = Utils.GetString(10);

            item.PublishDate = Utils.GetDateTime();
			

            item.StatusCommunication = Utils.GetInt();








		return item;
	}
 [TestMethod]
  public void  CommunicationProjectCreate()
  {
            this. CommunicationProjectCreate(null, null);
   }

	[TestMethod]
	 public void CommunicationProjectCreate( List<CommunicationProject> items, CommunicationProject item )
        {
            var apiWrapper = GetWrapper<CommunicationProject>();
            apiWrapper.EntityKeyName = "CommunicationProject";
            apiWrapper.EntitySetName  = "CommunicationProjects";
			if (item == null && items == null)
            {
				apiWrapper.Item = CommunicationProjectSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.GuidCommunicationProject;
				var existent = Services.ApiGetByKey<CommunicationProject>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	public ProfileBlob ProfileBlobSample(){
		 ProfileBlob item = new ProfileBlob();
            item.GuidProfileBlob = Guid.NewGuid();


            item.BlobPath = Utils.GetString(10);

            item.BlobPathThumbnail = Utils.GetString(10);

            item.BlobType = Utils.GetInt();










            item.ExistFile = Utils.GetBool();

            item.FileName = Utils.GetString(10);

		return item;
	}
 [TestMethod]
  public void  ProfileBlobCreate()
  {
            this. ProfileBlobCreate(null, null);
   }

	[TestMethod]
	 public void ProfileBlobCreate( List<ProfileBlob> items, ProfileBlob item )
        {
            var apiWrapper = GetWrapper<ProfileBlob>();
            apiWrapper.EntityKeyName = "ProfileBlob";
            apiWrapper.EntitySetName  = "ProfileBlobs";
			if (item == null && items == null)
            {
				apiWrapper.Item = ProfileBlobSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.GuidProfileBlob;
				var existent = Services.ApiGetByKey<ProfileBlob>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	public ProfileMusicPreference ProfileMusicPreferenceSample(){
		 ProfileMusicPreference item = new ProfileMusicPreference();


            item.GuidProfileMusicPreference = Guid.NewGuid();







		return item;
	}
 [TestMethod]
  public void  ProfileMusicPreferenceCreate()
  {
            this. ProfileMusicPreferenceCreate(null, null);
   }

	[TestMethod]
	 public void ProfileMusicPreferenceCreate( List<ProfileMusicPreference> items, ProfileMusicPreference item )
        {
            var apiWrapper = GetWrapper<ProfileMusicPreference>();
            apiWrapper.EntityKeyName = "ProfileMusicPreference";
            apiWrapper.EntitySetName  = "ProfileMusicPreferences";
			if (item == null && items == null)
            {
				apiWrapper.Item = ProfileMusicPreferenceSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.GuidProfileMusicPreference;
				var existent = Services.ApiGetByKey<ProfileMusicPreference>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	public ProfilePage ProfilePageSample(){
		 ProfilePage item = new ProfilePage();
            item.GuidProfilePage = Guid.NewGuid();



            item.Name = Utils.GetString(10);

            item.Description = Utils.GetString(10);

            item.Address = Utils.GetString(10);

            item.PhoneNumber = Utils.GetString(10);







            item.ContentHTML = Utils.GetString(10);

		return item;
	}
 [TestMethod]
  public void  ProfilePageCreate()
  {
            this. ProfilePageCreate(null, null);
   }

	[TestMethod]
	 public void ProfilePageCreate( List<ProfilePage> items, ProfilePage item )
        {
            var apiWrapper = GetWrapper<ProfilePage>();
            apiWrapper.EntityKeyName = "ProfilePage";
            apiWrapper.EntitySetName  = "ProfilePages";
			if (item == null && items == null)
            {
				apiWrapper.Item = ProfilePageSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.GuidProfilePage;
				var existent = Services.ApiGetByKey<ProfilePage>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	public ProfilePageBlob ProfilePageBlobSample(){
		 ProfilePageBlob item = new ProfilePageBlob();
            item.GuidProfilePageBlob = Guid.NewGuid();


            item.BlobPath = Utils.GetString(10);

            item.BlobPathThumbnail = Utils.GetString(10);

            item.BlobType = Utils.GetInt();


            item.BlobSection = Utils.GetInt();








		return item;
	}
 [TestMethod]
  public void  ProfilePageBlobCreate()
  {
            this. ProfilePageBlobCreate(null, null);
   }

	[TestMethod]
	 public void ProfilePageBlobCreate( List<ProfilePageBlob> items, ProfilePageBlob item )
        {
            var apiWrapper = GetWrapper<ProfilePageBlob>();
            apiWrapper.EntityKeyName = "ProfilePageBlob";
            apiWrapper.EntitySetName  = "ProfilePageBlobs";
			if (item == null && items == null)
            {
				apiWrapper.Item = ProfilePageBlobSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.GuidProfilePageBlob;
				var existent = Services.ApiGetByKey<ProfilePageBlob>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	public ProfilePageStore ProfilePageStoreSample(){
		 ProfilePageStore item = new ProfilePageStore();
            item.GuidProfilePageStore = Guid.NewGuid();


            item.Name = Utils.GetString(10);

            item.Address = Utils.GetString(10);







		return item;
	}
 [TestMethod]
  public void  ProfilePageStoreCreate()
  {
            this. ProfilePageStoreCreate(null, null);
   }

	[TestMethod]
	 public void ProfilePageStoreCreate( List<ProfilePageStore> items, ProfilePageStore item )
        {
            var apiWrapper = GetWrapper<ProfilePageStore>();
            apiWrapper.EntityKeyName = "ProfilePageStore";
            apiWrapper.EntitySetName  = "ProfilePageStores";
			if (item == null && items == null)
            {
				apiWrapper.Item = ProfilePageStoreSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.GuidProfilePageStore;
				var existent = Services.ApiGetByKey<ProfilePageStore>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	public ProfilePageTermContract ProfilePageTermContractSample(){
		 ProfilePageTermContract item = new ProfilePageTermContract();
            item.GuidProfilePageTermContact = Guid.NewGuid();


            item.Description = Utils.GetString(10);







		return item;
	}
 [TestMethod]
  public void  ProfilePageTermContractCreate()
  {
            this. ProfilePageTermContractCreate(null, null);
   }

	[TestMethod]
	 public void ProfilePageTermContractCreate( List<ProfilePageTermContract> items, ProfilePageTermContract item )
        {
            var apiWrapper = GetWrapper<ProfilePageTermContract>();
            apiWrapper.EntityKeyName = "ProfilePageTermContract";
            apiWrapper.EntitySetName  = "ProfilePageTermContracts";
			if (item == null && items == null)
            {
				apiWrapper.Item = ProfilePageTermContractSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.GuidProfilePageTermContact;
				var existent = Services.ApiGetByKey<ProfilePageTermContract>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	public ProjectIncident ProjectIncidentSample(){
		 ProjectIncident item = new ProjectIncident();
            item.GuidProjectIncident = Guid.NewGuid();



            item.Subject = Utils.GetString(10);

            item.PublishDate = Utils.GetDateTime();
			

            item.StatusIncident = Utils.GetInt();








		return item;
	}
 [TestMethod]
  public void  ProjectIncidentCreate()
  {
            this. ProjectIncidentCreate(null, null);
   }

	[TestMethod]
	 public void ProjectIncidentCreate( List<ProjectIncident> items, ProjectIncident item )
        {
            var apiWrapper = GetWrapper<ProjectIncident>();
            apiWrapper.EntityKeyName = "ProjectIncident";
            apiWrapper.EntitySetName  = "ProjectIncidents";
			if (item == null && items == null)
            {
				apiWrapper.Item = ProjectIncidentSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.GuidProjectIncident;
				var existent = Services.ApiGetByKey<ProjectIncident>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	public ProjectContract ProjectContractSample(){
		 ProjectContract item = new ProjectContract();
            item.GuidProjectContract = Guid.NewGuid();


            item.Description = Utils.GetString(10);


            item.Money = Utils.GetDecimal();


            item.StatusProjectContracts = Utils.GetInt();



            item.Payed = Utils.GetBool();


            item.Canceled = Utils.GetBool();







		return item;
	}
 [TestMethod]
  public void  ProjectContractCreate()
  {
            this. ProjectContractCreate(null, null);
   }

	[TestMethod]
	 public void ProjectContractCreate( List<ProjectContract> items, ProjectContract item )
        {
            var apiWrapper = GetWrapper<ProjectContract>();
            apiWrapper.EntityKeyName = "ProjectContract";
            apiWrapper.EntitySetName  = "ProjectContracts";
			if (item == null && items == null)
            {
				apiWrapper.Item = ProjectContractSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.GuidProjectContract;
				var existent = Services.ApiGetByKey<ProjectContract>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	public ProjectProfileRating ProjectProfileRatingSample(){
		 ProjectProfileRating item = new ProjectProfileRating();
            item.GuidProjectProfileRating = Guid.NewGuid();



            item.Comment = Utils.GetString(10);

            item.Calification = Utils.GetInt();








		return item;
	}
 [TestMethod]
  public void  ProjectProfileRatingCreate()
  {
            this. ProjectProfileRatingCreate(null, null);
   }

	[TestMethod]
	 public void ProjectProfileRatingCreate( List<ProjectProfileRating> items, ProjectProfileRating item )
        {
            var apiWrapper = GetWrapper<ProjectProfileRating>();
            apiWrapper.EntityKeyName = "ProjectProfileRating";
            apiWrapper.EntitySetName  = "ProjectProfileRatings";
			if (item == null && items == null)
            {
				apiWrapper.Item = ProjectProfileRatingSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.GuidProjectProfileRating;
				var existent = Services.ApiGetByKey<ProjectProfileRating>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	public ProjectsQuestion ProjectsQuestionSample(){
		 ProjectsQuestion item = new ProjectsQuestion();
            item.GuidProjectQuestion = Guid.NewGuid();


            item.PublishDate = Utils.GetDateTime();
			

            item.StatusQuestion = Utils.GetInt();








		return item;
	}
 [TestMethod]
  public void  ProjectsQuestionCreate()
  {
            this. ProjectsQuestionCreate(null, null);
   }

	[TestMethod]
	 public void ProjectsQuestionCreate( List<ProjectsQuestion> items, ProjectsQuestion item )
        {
            var apiWrapper = GetWrapper<ProjectsQuestion>();
            apiWrapper.EntityKeyName = "ProjectsQuestion";
            apiWrapper.EntitySetName  = "ProjectsQuestions";
			if (item == null && items == null)
            {
				apiWrapper.Item = ProjectsQuestionSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.GuidProjectQuestion;
				var existent = Services.ApiGetByKey<ProjectsQuestion>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	public ProfileEvent ProfileEventSample(){
		 ProfileEvent item = new ProfileEvent();
            item.GuidProfileEvent = Guid.NewGuid();


            item.EventDate = Utils.GetDateTime();
			

            item.Description = Utils.GetString(10);








		return item;
	}
 [TestMethod]
  public void  ProfileEventCreate()
  {
            this. ProfileEventCreate(null, null);
   }

	[TestMethod]
	 public void ProfileEventCreate( List<ProfileEvent> items, ProfileEvent item )
        {
            var apiWrapper = GetWrapper<ProfileEvent>();
            apiWrapper.EntityKeyName = "ProfileEvent";
            apiWrapper.EntitySetName  = "ProfileEvents";
			if (item == null && items == null)
            {
				apiWrapper.Item = ProfileEventSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.GuidProfileEvent;
				var existent = Services.ApiGetByKey<ProfileEvent>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	public ProjectIncidentDetail ProjectIncidentDetailSample(){
		 ProjectIncidentDetail item = new ProjectIncidentDetail();
            item.GuidProjectIncidentDetail = Guid.NewGuid();



            item.Description = Utils.GetString(10);

            item.PublishDate = Utils.GetDateTime();
			







		return item;
	}
 [TestMethod]
  public void  ProjectIncidentDetailCreate()
  {
            this. ProjectIncidentDetailCreate(null, null);
   }

	[TestMethod]
	 public void ProjectIncidentDetailCreate( List<ProjectIncidentDetail> items, ProjectIncidentDetail item )
        {
            var apiWrapper = GetWrapper<ProjectIncidentDetail>();
            apiWrapper.EntityKeyName = "ProjectIncidentDetail";
            apiWrapper.EntitySetName  = "ProjectIncidentDetails";
			if (item == null && items == null)
            {
				apiWrapper.Item = ProjectIncidentDetailSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.GuidProjectIncidentDetail;
				var existent = Services.ApiGetByKey<ProjectIncidentDetail>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	public ProjectContractTerm ProjectContractTermSample(){
		 ProjectContractTerm item = new ProjectContractTerm();
            item.ProjectContractTermId = Guid.NewGuid();


            item.Description = Utils.GetString(10);







		return item;
	}
 [TestMethod]
  public void  ProjectContractTermCreate()
  {
            this. ProjectContractTermCreate(null, null);
   }

	[TestMethod]
	 public void ProjectContractTermCreate( List<ProjectContractTerm> items, ProjectContractTerm item )
        {
            var apiWrapper = GetWrapper<ProjectContractTerm>();
            apiWrapper.EntityKeyName = "ProjectContractTerm";
            apiWrapper.EntitySetName  = "ProjectContractTerms";
			if (item == null && items == null)
            {
				apiWrapper.Item = ProjectContractTermSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.ProjectContractTermId;
				var existent = Services.ApiGetByKey<ProjectContractTerm>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	public InterestedPage InterestedPageSample(){
		 InterestedPage item = new InterestedPage();



            item.Accepted = Utils.GetBool();







            item.GuidInterestedPage = Guid.NewGuid();

		return item;
	}
 [TestMethod]
  public void  InterestedPageCreate()
  {
            this. InterestedPageCreate(null, null);
   }

	[TestMethod]
	 public void InterestedPageCreate( List<InterestedPage> items, InterestedPage item )
        {
            var apiWrapper = GetWrapper<InterestedPage>();
            apiWrapper.EntityKeyName = "InterestedPage";
            apiWrapper.EntitySetName  = "InterestedPages";
			if (item == null && items == null)
            {
				apiWrapper.Item = InterestedPageSample();
			}else{
				apiWrapper.Item = item;
                apiWrapper.Items = items;
			}
            var resultCreate  = Services.ApiCreate(url, apiWrapper);
			Assert.IsTrue(resultCreate == "ok");
			if (apiWrapper.Item != null)
            {
				apiWrapper.ItemKey = apiWrapper.Item.GuidInterestedPage;
				var existent = Services.ApiGetByKey<InterestedPage>(url, apiWrapper);
				Assert.IsNotNull(existent);
			}
            
        }
	  }
}
