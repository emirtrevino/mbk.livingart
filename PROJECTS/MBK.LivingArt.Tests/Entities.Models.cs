﻿ 
 

#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
//using SFSdotNet.Framework.Common.Entities.Metadata;
//using SFSdotNet.Framework.Common.Entities;

//using Repository.Pattern.Ef6;
#endregion
namespace MBK.LivingArt.Client.Models
{


	  [Serializable()]
	  public partial class CommunicationProject{
public Guid? GuidCommunicationProject { get; set; }		

			


public Guid GuidProject { get; set; }		
		
			


public Guid GuidProfile { get; set; }		
		
			


public String Description { get; set; }		
		
			


public DateTime PublishDate { get; set; }		
		
			


public Int32 StatusCommunication { get; set; }		
		
			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			


public Boolean? IsDeleted { get; set; }		
		
			


public Project Project { get; set; }		
		
			


public UserProfile UserProfile { get; set; }		
		
	  }

	  [Serializable()]
	  public partial class MusicPreference{
public Guid? GuidMusicPreference { get; set; }		

			


public String Description { get; set; }		
		
			


public Boolean IsDeleted { get; set; }		
		
			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			
public List<ProfileMusicPreference>  ProfileMusicPreferences { get; set; }		
		
 	  }

	  [Serializable()]
	  public partial class PageType{
public Guid? GuidPageType { get; set; }		

			


public String Name { get; set; }		
		
			


public Int32 ProfileType { get; set; }		
		
			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			


public Boolean? IsDeleted { get; set; }		
		
			
public List<ProfilePage>  ProfilePages { get; set; }		
		
 			
public List<ProjectContract>  ProjectContracts { get; set; }		
		
 	  }

	  [Serializable()]
	  public partial class ProfileBlob{
public Guid? GuidProfileBlob { get; set; }		

			


public Guid GuidProfile { get; set; }		
		
			


public String BlobPath { get; set; }		
		
			


public String BlobPathThumbnail { get; set; }		
		
			


public Int32 BlobType { get; set; }		
		
			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			


public Boolean? IsDeleted { get; set; }		
		
			


public Guid? GuidFile { get; set; }		
		
			


public LAFile LAFile { get; set; }		
		
			


public UserProfile UserProfile { get; set; }		
		
			


public Boolean ExistFile { get; set; }		
		
			


public String FileName { get; set; }		
		
	  }

	  [Serializable()]
	  public partial class ProfileMusicPreference{
			


public Guid? GuidProfile { get; set; }		
		
			


public Guid? GuidMusicPreference { get; set; }		
		
public Guid? GuidProfileMusicPreference { get; set; }		

			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			


public Boolean? IsDeleted { get; set; }		
		
			


public MusicPreference MusicPreference { get; set; }		
		
			


public UserProfile UserProfile { get; set; }		
		
	  }

	  [Serializable()]
	  public partial class ProfilePageBlob{
public Guid? GuidProfilePageBlob { get; set; }		

			


public Guid GuidProfilePage { get; set; }		
		
			


public String BlobPath { get; set; }		
		
			


public String BlobPathThumbnail { get; set; }		
		
			


public Int32 BlobType { get; set; }		
		
			


public Int32 BlobSection { get; set; }		
		
			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			


public Boolean? IsDeleted { get; set; }		
		
			


public ProfilePage ProfilePage { get; set; }		
		
	  }

	  [Serializable()]
	  public partial class ProfilePageStore{
public Guid? GuidProfilePageStore { get; set; }		

			


public Guid GuidProfilePage { get; set; }		
		
			


public String Name { get; set; }		
		
			


public String Address { get; set; }		
		
			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			


public Boolean? IsDeleted { get; set; }		
		
			


public ProfilePage ProfilePage { get; set; }		
		
	  }

	  [Serializable()]
	  public partial class ProfilePageTermContract{
public Guid? GuidProfilePageTermContact { get; set; }		

			


public Guid GuidProfilePage { get; set; }		
		
			


public String Description { get; set; }		
		
			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			


public Boolean? IsDeleted { get; set; }		
		
			


public ProfilePage ProfilePage { get; set; }		
		
	  }

	  [Serializable()]
	  public partial class Project{
public Guid? GuidProject { get; set; }		

			


public Guid GuidProfile { get; set; }		
		
			


public String Description { get; set; }		
		
			


public DateTime PublishDate { get; set; }		
		
			


public DateTime ProjectDate { get; set; }		
		
			


public Boolean IsExternal { get; set; }		
		
			


public Decimal Surface { get; set; }		
		
			


public Decimal Money { get; set; }		
		
			


public Int32 StatusProject { get; set; }		
		
			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			


public Boolean? IsDeleted { get; set; }		
		
			
public List<CommunicationProject>  CommunicationProjects { get; set; }		
		
 			
public List<ProjectIncident>  ProjectIncidents { get; set; }		
		
 			
public List<ProjectsQuestion>  ProjectsQuestions { get; set; }		
		
 			


public UserProfile UserProfile { get; set; }		
		
			
public List<ProjectContract>  ProjectContracts { get; set; }		
		
 	  }

	  [Serializable()]
	  public partial class ProjectIncident{
public Guid? GuidProjectIncident { get; set; }		

			


public Guid GuidProject { get; set; }		
		
			


public Guid GuidProfile { get; set; }		
		
			


public String Subject { get; set; }		
		
			


public DateTime PublishDate { get; set; }		
		
			


public Int32 StatusIncident { get; set; }		
		
			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			


public Boolean? IsDeleted { get; set; }		
		
			


public Project Project { get; set; }		
		
			
public List<ProjectIncidentDetail>  ProjectIncidentDetails { get; set; }		
		
 	  }

	  [Serializable()]
	  public partial class ProjectProfileRating{
public Guid? GuidProjectProfileRating { get; set; }		

			


public Guid GuidProfile { get; set; }		
		
			


public Guid GuidProjectContract { get; set; }		
		
			


public String Comment { get; set; }		
		
			


public Int32 Calification { get; set; }		
		
			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			


public Boolean? IsDeleted { get; set; }		
		
			


public ProjectContract ProjectContract { get; set; }		
		
	  }

	  [Serializable()]
	  public partial class ProjectsQuestion{
public Guid? GuidProjectQuestion { get; set; }		

			


public Guid GuidProject { get; set; }		
		
			


public Byte[] Description { get; set; }		
		
			


public DateTime PublishDate { get; set; }		
		
			


public Int32 StatusQuestion { get; set; }		
		
			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			


public Boolean? IsDeleted { get; set; }		
		
			


public Project Project { get; set; }		
		
	  }

	  [Serializable()]
	  public partial class ProfilePage{
public Guid? GuidProfilePage { get; set; }		

			


public Guid GuidProfile { get; set; }		
		
			


public Guid GuidPageType { get; set; }		
		
			


public String Name { get; set; }		
		
			


public String Description { get; set; }		
		
			


public String Address { get; set; }		
		
			


public String PhoneNumber { get; set; }		
		
			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			


public Boolean? IsDeleted { get; set; }		
		
			


public String ContentHTML { get; set; }		
		
			


public PageType PageType { get; set; }		
		
			
public List<ProfilePageBlob>  ProfilePageBlobs { get; set; }		
		
 			
public List<ProfilePageStore>  ProfilePageStores { get; set; }		
		
 			
public List<ProfilePageTermContract>  ProfilePageTermContracts { get; set; }		
		
 			


public UserProfile UserProfile { get; set; }		
		
			
public List<InterestedPage>  InterestedPages { get; set; }		
		
 	  }

	  [Serializable()]
	  public partial class ProfileEvent{
public Guid? GuidProfileEvent { get; set; }		

			


public Guid GuidProfile { get; set; }		
		
			


public DateTime EventDate { get; set; }		
		
			


public String Description { get; set; }		
		
			


public Guid GuidProject { get; set; }		
		
			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			


public Boolean? IsDeleted { get; set; }		
		
			


public UserProfile UserProfile { get; set; }		
		
	  }

	  [Serializable()]
	  public partial class ProjectIncidentDetail{
public Guid? GuidProjectIncidentDetail { get; set; }		

			


public Guid GuidProjectIncident { get; set; }		
		
			


public Guid GuidProfile { get; set; }		
		
			


public String Description { get; set; }		
		
			


public DateTime PublishDate { get; set; }		
		
			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			


public Boolean? IsDeleted { get; set; }		
		
			


public ProjectIncident ProjectIncident { get; set; }		
		
	  }

	  [Serializable()]
	  public partial class ProjectContractTerm{
public Guid? ProjectContractTermId { get; set; }		

			


public Guid GuidProjectContract { get; set; }		
		
			


public String Description { get; set; }		
		
			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			


public Boolean? IsDeleted { get; set; }		
		
			


public ProjectContract ProjectContract { get; set; }		
		
	  }

	  [Serializable()]
	  public partial class LAFile{
public Guid? GuidFile { get; set; }		

			


public String FileName { get; set; }		
		
			


public Int64? FileSize { get; set; }		
		
			


public String FileType { get; set; }		
		
			


public Byte[] FileData { get; set; }		
		
			


public String FileStorage { get; set; }		
		
			


public String ContainerStorage { get; set; }		
		
			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			


public Boolean? IsDeleted { get; set; }		
		
			
public List<ProfileBlob>  ProfileBlobs { get; set; }		
		
 	  }

	  [Serializable()]
	  public partial class UserProfile{
public Guid? GuidUser { get; set; }		

			


public String Name { get; set; }		
		
			


public String Alias { get; set; }		
		
			


public String Biography { get; set; }		
		
			


public DateTime? Birthdate { get; set; }		
		
			


public Int32 ProfileType { get; set; }		
		
			


public Int32 ProfileStatus { get; set; }		
		
			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			


public Boolean? IsDeleted { get; set; }		
		
			


public Guid? IntegrationID { get; set; }		
		
			
public List<CommunicationProject>  CommunicationProjects { get; set; }		
		
 			
public List<ProfileBlob>  ProfileBlobs { get; set; }		
		
 			
public List<ProfileEvent>  ProfileEvents { get; set; }		
		
 			
public List<ProfileMusicPreference>  ProfileMusicPreferences { get; set; }		
		
 			
public List<ProfilePage>  ProfilePages { get; set; }		
		
 			
public List<Project>  Projects { get; set; }		
		
 			


public Int32 CountMusicPreferencesComputed { get; set; }		
		
	  }

	  [Serializable()]
	  public partial class InterestedPage{
			


public Guid GuidProjectContract { get; set; }		
		
			


public Guid GuidProfilePage { get; set; }		
		
			


public Boolean Accepted { get; set; }		
		
			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			


public Boolean? IsDeleted { get; set; }		
		
public Guid? GuidInterestedPage { get; set; }		

			


public ProfilePage ProfilePage { get; set; }		
		
			


public ProjectContract ProjectContract { get; set; }		
		
	  }

	  [Serializable()]
	  public partial class ProjectContract{
public Guid? GuidProjectContract { get; set; }		

			


public Guid GuidProject { get; set; }		
		
			


public String Description { get; set; }		
		
			


public Guid GuidPageType { get; set; }		
		
			


public Decimal Money { get; set; }		
		
			


public Int32 StatusProjectContracts { get; set; }		
		
			


public Boolean Payed { get; set; }		
		
			


public Boolean Canceled { get; set; }		
		
			


public DateTime? CreatedDate { get; set; }		
		
			


public DateTime? UpdatedDate { get; set; }		
		
			


public Guid? CreatedBy { get; set; }		
		
			


public Guid? UpdatedBy { get; set; }		
		
			


public Int32? Bytes { get; set; }		
		
			


public Boolean? IsDeleted { get; set; }		
		
			
public List<InterestedPage>  InterestedPages { get; set; }		
		
 			


public PageType PageType { get; set; }		
		
			


public Project Project { get; set; }		
		
			
public List<ProjectContractTerm>  ProjectContractTerms { get; set; }		
		
 			
public List<ProjectProfileRating>  ProjectProfileRatings { get; set; }		
		
 	  }
	
}
