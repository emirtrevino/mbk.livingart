﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MBK.LivingArt.Client.Models;
using System.Collections.Generic;

namespace MBK.LivingArt.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void RegisterUserOnlyFullName()
        { 


            string urlApp = System.Configuration.ConfigurationManager.AppSettings["urlApp"];
            string appKey = "MBKLivingArt"; // ID de aplicación
            //string entitySetName = "bonCustomers";
            UserDataRegister data = new MBK.LivingArt.Client.Models.UserDataRegister();
            #region Register user with App
            data = new MBK.LivingArt.Client.Models.UserDataRegister();
            data.Email = Utils.GetString(10) + "@mail.com";
            data.Password = Utils.GetString(10);
            data.FirstName = "Name" + Utils.GetString(6);
            data.FirstName = "Last" + Utils.GetString(6);
            data.AppKey = appKey; // tiene autoservicio, relacionado con una empresa + dominio
            var registerResultWithApp = Services.CallApi(urlApp + "/Api/Register", "POST", data);

            Result<UserDataResponse> resultRegisterWithApp = Newtonsoft.Json.JsonConvert.DeserializeObject<Result<UserDataResponse>>(registerResultWithApp);
            Assert.AreEqual(resultRegisterWithApp.status, "success");
            Assert.IsNotNull(resultRegisterWithApp.data.IdCompany);

            #endregion

            #region Login
            UserDataLogin dataLogin = new MBK.LivingArt.Client.Models.UserDataLogin();
            dataLogin = new MBK.LivingArt.Client.Models.UserDataLogin();
            dataLogin.Username = data.Email;
            dataLogin.Password = data.Password;
            var loginResult = Services.CallApi(urlApp + "/Api/Login", "POST", dataLogin);

            Result<UserDataWithToken>  resultLogin = Newtonsoft.Json.JsonConvert.DeserializeObject<Result<UserDataWithToken>>(loginResult);
            Assert.AreEqual(resultLogin.status, "success");
            Assert.IsNotNull(resultLogin.data.Token);
            #endregion

            #region Get Items of ProfileEvents

            // Se espera que ocurra un error de token inválido porque la contraseña ha sido cambiada

            ApiWrapper dataRequest = new ApiWrapper();

            var dataRequestTyped = new ApiWrapper<ProfileEvent>();
            dataRequestTyped.Items = new List<ProfileEvent>();

            dataRequestTyped.Items.Add(new ProfileEvent() { EventDate = DateTime.Now, Description = Utils.GetString(10),  GuidProject = Guid.Empty,  GuidProfile = Guid.Parse(resultRegisterWithApp.data.IdUser) });
            dataRequestTyped.Items.Add(new ProfileEvent() { EventDate = DateTime.Now, Description = Utils.GetString(10), GuidProject = Guid.Empty, GuidProfile = Guid.Parse(resultRegisterWithApp.data.IdUser) });
            // dataRequest.QueryFilter = "it.UserProfile.GuidUser ";
            dataRequestTyped.Token = resultLogin.data.Token;
            dataRequestTyped.CompanyId = resultRegisterWithApp.data.IdCompany;
            var result = Services.CallApi(urlApp + "/" + appKey + "/ProfileEvents/Create", "POST", (object)dataRequestTyped);

            // var resultGetItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProfileEvent>>(getItems);

            // Assert.AreEqual(resultGetItems.status, "success");

            dataRequest = new ApiWrapper();
            dataRequest.QueryFilter = "it.UserProfile.GuidUser = Guid(\"" + resultRegisterWithApp.data.IdUser + "\")";
            dataRequest.Token = resultLogin.data.Token;
            dataRequest.CompanyId = resultRegisterWithApp.data.IdCompany;
            var getItems = Services.CallApi(urlApp + "/" + appKey + "/ProfileEvents/Get", "POST", dataRequest);

            var resultGetItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ProfileEvent>>(getItems);



            var dataRequest2 = new ApiWrapper();
            
            dataRequestTyped.MethodName = "MyCustomMethodA";
               // dataRequest.QueryFilter = "it.UserProfile.GuidUser ";
            dataRequestTyped.Token = resultLogin.data.Token;
            dataRequestTyped.CompanyId = resultRegisterWithApp.data.IdCompany;
            result = Services.CallApi(urlApp + "/" + appKey + "/ProfileEvents/CustomMethod", "POST", (object)dataRequestTyped);

            // Assert.AreEqual(resultGetItems.status, "success");


            #endregion



        }
    }
}
